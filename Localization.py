#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  Localization.py
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.10.11
#
#  Localization: Adjust eight sources independently. One contains
#  a signal. The others are noise.  Find which source is emitting
#  the signal.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from   __future__ import print_function, absolute_import
from   six.moves  import input           # use raw_input when I say input

#import ipdb  # iPython debugger (relative of pdb)

from   os         import makedirs
from   time       import strftime, time, sleep
import sys
import os.path
import socket
import json

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca

from PySide.QtCore import *
from PySide.QtGui  import *

from common import provideIP    # Manual connect Paca dialog
from common.functions import *  # Connect to Paca, rescaling, etc.
from common.control   import *  # VCS control widget class

from UI import LocalizationUI   # Virtual Control Surface

from Prolog      import *       # An introduction...
from SessionInfo import *
from Rater       import *       # A ratings dialog window

from paca_listener import PacaListener

import random

__appname__    = "Sound Advice"
__module__     = "Localization"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2015, Kevin Cole (2016.10.11)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Button colors

GREY   = "background: #e0e0e0;"  # Neutral (default state)
ORANGE = "background: #ffcc33;"  # Waiting to be clicked
GREEN  = "background: #33ff66;"  # Clicked and active
RED    = "background: #e00000;"  # Speaker max'ed or min'ed out


class UI(LocalizationUI.UI):
    """Main Localization Exploration User Interface (UI)"""

    def __init__(self, initValues, parent=None):
        """Construct the Localization window and fill with widgets"""

        super(UI, self).__init__(parent)  # Old way. Learn new way.

        self.osc            = initValues["osc"]
        self.subjectInfo    = initValues["subjectInfo"]
        self.kymaSound      = initValues["kymaSound"]
        self.programButtons = initValues["programButtons"]
        self.presets        = initValues["presets"]
        self.ratings        = initValues["ratings"]
        self.instructions   = initValues["instructions"]
        self.configuration  = initValues["configuration"]
        self.filename       = initValues["filename"]
        self.width          = initValues["width"]
        self.height         = initValues["height"]
        self.center         = initValues["center"]

        self.speakers       = [0] * 8
        self.resets         = [0] * 8
        self.selected       = [0] * 8

        self.signalState    = [0] * 8
        self.signalSample   = [0] * 8
        self.signalLevel    = [0] * 8
        self.signalPaused   = [0] * 8

        self.noiseState     = [0] * 8
        self.noiseSample    = [0] * 8
        self.noiseLevel     = [0] * 8
        self.noisePaused    = [0] * 8

        self.lines = len(self.presets)
        self.line  = 0

        self.trials    = len(self.presets)
        self.trial     = -1     # Sample pair not yet chosen
        self.marktime  = -1.0   # Time not started
        self.triggered = False  # Not yet auto-started
        self.playing   = 0      # Sounds not playing
        self.paused    = 0      # Sounds not paused
        self.guessed   = False  # No guess has been made

        # Button pressed counts

        self.ald_pressed   = 0  # One press for all ALD buttons?
        self.start_pressed = 0  # Odd = Start/Restart pressed. Even = Stop pressed
        self.pause_pressed = 0  # Odd = Pause pressed.         Even = Resume pressed
        self.rate_pressed  = 0  # Number of times current trial rated
        self.save_pressed  = 0  # Should ALWAYS match rate_pressed
        self.next_pressed  = 0  # Not really necessary

        self.signalsPaused = 0
        self.noisesPaused  = 0
        self.targets = []
        self.rated = None
        self.program = None

        self.results = open(self.filename, "a")

        gradient  = "QLinearGradient("
        gradient += "  x1: 0, y1: 0, x2: 0, y2: 1,"
#       gradient += "  stop: 0    #88d,"
#       gradient += "  stop: 0.1  #99e,"
#       gradient += "  stop: 0.49 #77c,"
#       gradient += "  stop: 0.5  #66b,"
#       gradient += "  stop: 1    #77c"
        gradient += "  stop: 0    #dd8,"
        gradient += "  stop: 0.1  #ee9,"
        gradient += "  stop: 0.49 #cc7,"
        gradient += "  stop: 0.5  #bb6,"
        gradient += "  stop: 1    #cc7"
        gradient += ")"

        self.style  = "color: {0};"
        self.style += "background-color: {0};".format(gradient)
        self.style += "border-width: 1px;"
        self.style += "border-color: #339;"
        self.style += "border-style: solid;"
        self.style += "border-radius: 10;"
        self.style += "font-size: 24px;"
        self.style += "min-width:20px;"
        self.style += "min-height:20px;"
        self.style += "max-width:20px;"
        self.style += "max-height:20px;"
        self.style += "padding:5px 3px 5px 3px;"

        # Set up slots (actions) for signals from widgets

        self.setupUi(self, initValues)

        self.controlMenu.deviceSetting.device.buttonClicked[int].connect(self.getSetting)

        self.controlPanel.speaker1Select.clicked.connect(lambda: self.select(self.controlPanel.speaker1Select))
        self.controlPanel.speaker1Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker1Louder))
        self.controlPanel.speaker1Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker1Softer))

        self.controlPanel.speaker2Select.clicked.connect(lambda: self.select(self.controlPanel.speaker2Select))
        self.controlPanel.speaker2Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker2Louder))
        self.controlPanel.speaker2Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker2Softer))

        self.controlPanel.speaker3Select.clicked.connect(lambda: self.select(self.controlPanel.speaker3Select))
        self.controlPanel.speaker3Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker3Louder))
        self.controlPanel.speaker3Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker3Softer))

        self.controlPanel.speaker4Select.clicked.connect(lambda: self.select(self.controlPanel.speaker4Select))
        self.controlPanel.speaker4Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker4Louder))
        self.controlPanel.speaker4Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker4Softer))

        self.controlPanel.speaker5Select.clicked.connect(lambda: self.select(self.controlPanel.speaker5Select))
        self.controlPanel.speaker5Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker5Louder))
        self.controlPanel.speaker5Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker5Softer))

        self.controlPanel.speaker6Select.clicked.connect(lambda: self.select(self.controlPanel.speaker6Select))
        self.controlPanel.speaker6Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker6Louder))
        self.controlPanel.speaker6Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker6Softer))

        self.controlPanel.speaker7Select.clicked.connect(lambda: self.select(self.controlPanel.speaker7Select))
        self.controlPanel.speaker7Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker7Louder))
        self.controlPanel.speaker7Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker7Softer))

        self.controlPanel.speaker8Select.clicked.connect(lambda: self.select(self.controlPanel.speaker8Select))
        self.controlPanel.speaker8Louder.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker8Louder))
        self.controlPanel.speaker8Softer.clicked.connect(lambda: self.setSignalLevel(self.controlPanel.speaker8Softer))

        self.controlPanel.signalPause.clicked.connect(self.toggleSignal)
        self.controlPanel.noisePause.clicked.connect(self.toggleNoise)

        self.controls = {}

        self.controlMenu.startButton.clicked.connect(lambda: self.togglePlay(False))
        self.controlMenu.pauseButton.clicked.connect(lambda: self.togglePause(False))
        self.controlMenu.nextButton.clicked.connect(self.nextTrial)

        self.controlMenu.rateButton.clicked.connect(self.rate)

        self.pacaListener = PacaListener(self)
        self.pacaListener.talk.connect(self.handleResponse)
        self.pacaListener.fetched.connect(self.update)
        self.pacaListener.start()

        oscRespondTo = "/osc/respond_to"
        msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
        msg.add_arg(8001)  # Send feedback to port 8001
        msg = msg.build()
        for speaking in range(10):
            self.osc.send(msg)
            sleep(0.01)

        oscNotify = "/osc/notify/vcs/SoundAdvice"
        msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
        msg.add_arg(1)  # Turn notifications ON (0 for OFF)
        msg = msg.build()
        for speaking in range(10):
            self.osc.send(msg)
            sleep(0.01)

    def keyPressEvent(self, event):
        super(UI, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            print("DEBUG: Localization got the ESC")
            now = strftime("%Y-%m-%d %H:%M")
            self.results.write("\n,,ABORTED! [Esc] pressed: {0}\n"
                               .format(now))
            self.results.close()
            self.repeat("replay",          [0])        # Stop playing
            self.repeat("pauseSignal",     [0]   * 8)  # Resume all Signals
            self.repeat("pauseNoise",      [0]   * 8)  # Resume all Noises
            self.repeat("setSignalState",  [0]   * 8)  # Power OFF all Signals
            self.repeat("setNoiseState",   [0]   * 8)  # Power OFF all Noises
            self.repeat("setSignalSample", [0]   * 8)  # All Signals = Sample 1
            self.repeat("setNoiseSample",  [0]   * 8)  # All Noises  = Sample 1
            self.repeat("setSignalLevel",  [0.5] * 8)  # All Signals @ Midrange
            self.repeat("setNoiseLevel",   [0.5] * 8)  # All Noises  @ Midrange
            QApplication.closeAllWindows()

    def handleResponse(self, response):
        pass

    def update(self):
        """Update counts of signal & noise samples loaded into each speaker"""

        for speaker in range(8):
            self.speakers[speaker] = {}

            signal  = "signal sample {0}".format(speaker + 1)
            signals = len(widgetry[eventery[signal][0]]["samples"])
            self.speakers[speaker]["signals"] = signals

            noise   = "noise sample {0}".format(speaker + 1)
            noises  = len(widgetry[eventery[noise][0]]["samples"])
            self.speakers[speaker]["noises"] = noises
        self.show()
        self.raise_()
        if not self.triggered:
            self.triggered = True
            self.nextTrial()  # Begin

    def repeat(self, command, modifier, *scale):
        """Send redundant OSC messages to Paca"""

        message = self.pacaListener.command[command]
        pairs = len(message.args) / 2
        for arg in range(1, pairs + 1):
            if scale:
                lower, upper = scale
                setting = float(rescale(modifier[arg - 1], lower, upper))
            else:
                setting = float(modifier[arg - 1])
            message.args[(arg * 2) - 1] = ("f", setting)
        message = message.build()
        for speaking in range(10):
            self.osc.send(message)
            sleep(0.01)

    def select(self, button):
        """Select a source to manipulate"""

        state = ("#af0000", "#00af00")
        info  = ("selected", "deselected")
        OFF, ON = 0, 1

        self.controlPanel.signalPause.setEnabled(True)
        self.controlPanel.noisePause.setEnabled(True)

        self.guessed = True
        selected = int(button.objectName()[1])

        self.log(detail="Source {0} {1} "
                        .format(selected, info[self.selected[selected - 1]]))

        if self.selected[selected - 1] == ON:
            self.selected[selected - 1] = OFF
            restyle = self.style.format(state[OFF])
            self.controlPanel.speakers[selected - 1]["select"].setStyleSheet(restyle)
            self.signalLevel[selected - 1] = self.resets[selected - 1]
            self.repeat("setSignalLevel", self.signalLevel, -12, 12)
            self.controlPanel.speakers[selected - 1]["louder"].setStyleSheet(GREY)
            self.controlPanel.speakers[selected - 1]["softer"].setStyleSheet(GREY)
            self.controlPanel.setSpeakerEnabled(selected, False)
            for speaker in range(1, 9):  # Make all speakers selectable
                self.controlPanel.setSpeakerSelectable(speaker, True)
        else:
            self.selected[selected - 1] = ON
            restyle = self.style.format(state[ON])
            self.controlPanel.speakers[selected - 1]["select"].setStyleSheet(restyle)
            self.controlPanel.setSpeakerEnabled(selected, True)
            for speaker in range(1, 9):
                if speaker != selected:  # Disable all other speakers
                    self.controlPanel.setSpeakerEnabled(speaker, False)

    def deselect(self):
        """Deselect all sources"""

        state = ["#af0000"]
        info  = ["deselected"]
        OFF   = 0

        self.controlPanel.signalPause.setEnabled(False)
        self.controlPanel.noisePause.setEnabled(False)

        for speaker in range(1, 9):
            self.selected[speaker - 1] = OFF
            restyle = self.style.format(state[OFF])
            self.controlPanel.speakers[speaker - 1]["select"].setStyleSheet(restyle)
            self.signalLevel[speaker - 1] = self.resets[speaker - 1]
            self.repeat("setSignalLevel", self.signalLevel, -12, 12)
            self.controlPanel.speakers[speaker - 1]["louder"].setStyleSheet(GREY)
            self.controlPanel.speakers[speaker - 1]["softer"].setStyleSheet(GREY)
            self.controlPanel.setSpeakerEnabled(speaker, False)
            self.controlPanel.setSpeakerSelectable(speaker, True)

    def setSignalLevel(self, button, dB=None):
        """Set level for individual speaker after validating level"""

        if isinstance(button, QPushButton):
            targetSpeaker = int(button.objectName()[1])
            for speaker in range(1, 9):
                if speaker != targetSpeaker:  # Reset all other speakers
                    self.signalLevel[speaker - 1] = self.resets[speaker - 1]
                    self.controlPanel.speakers[speaker - 1]["louder"].setStyleSheet(GREY)
                    self.controlPanel.speakers[speaker - 1]["softer"].setStyleSheet(GREY)
            direction = button.objectName()[2:]    # "Louder" or "Softer"
            if   direction == "Louder":
                if self.signalLevel[targetSpeaker - 1] < 12:
                    self.signalLevel[targetSpeaker - 1] += 3  # dB
                    spawner = button.parent()
                    sibling = spawner.findChild(QPushButton,
                                                "s{0}Softer"
                                                .format(targetSpeaker))
                    sibling.setStyleSheet(GREY)
                    self.log_levelChange(targetSpeaker)
                else:
                    button.setStyleSheet(RED)
                    explain = "<p>This loudspeaker cannot become louder</p>"
                    self.warning("Maximum reached", explain)
            elif direction == "Softer":
                if self.signalLevel[targetSpeaker - 1] > -12:
                    self.signalLevel[targetSpeaker - 1] -= 3  # dB
                    spawner = button.parent()
                    sibling = spawner.findChild(QPushButton,
                                                "s{0}Louder"
                                                .format(targetSpeaker))
                    sibling.setStyleSheet(GREY)
                    self.log_levelChange(targetSpeaker)
                else:
                    button.setStyleSheet(RED)
                    explain = "<p>This loudspeaker cannot become softer</p>"
                    self.warning("Minimum reached", explain)
        elif isinstance(button, int):
            targetSpeaker = button
            self.signalLevel[targetSpeaker - 1] = dB
        self.repeat("setSignalLevel", self.signalLevel, -12, 12)

    def log_levelChange(self, speaker):
        """Record the dB value of the changed speaker (and time)"""

        timestamp    = round((time() - self.marktime), 3)
        row  = "{0},".format(self.trial + 1)
        row += "{0},".format(timestamp)
        row += "{0},".format("Changed speaker level")
        row += "," * (speaker - 1) + str(self.signalLevel[speaker - 1]) + "\n"
        self.results.write(row)

    def warning(self, title, message):
        """Pop up a warning dialog box"""

        box = QMessageBox()
        box.setIcon(QMessageBox.Warning)
        box.setWindowTitle(title)
        box.setText("<strong>{0}</strong>".format(title))
        box.setInformativeText(message.replace(" ", "&nbsp;"))
        centerX, centerY = self.controlPanel.centerX, self.controlPanel.centerY
        left,  top    = centerX - 150, centerY + 150
        width, height = 300, 150
        box.setGeometry(QRect(left, top, width, height))  # center box in pane
        box.exec_()

    def setTask(self):
        """Set the instructions to the appropriate task"""

        if self.rate_pressed < len(self.instructions[self.trial]["TASK"]) and \
          self.instructions[self.trial]["TASK"][self.rate_pressed] is not None:
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["TASK"][self.rate_pressed])
            self.controlMenu.rateButton.setEnabled(True)
        elif self.trial == (self.trials - 1):
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["QUIT"])
            self.controlMenu.startButton.setEnabled(False)
            self.controlMenu.startButton.setText("Start")
            self.controlMenu.nextButton.setEnabled(True)
        else:
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["NEXT"])
            self.controlMenu.startButton.setEnabled(False)
            self.controlMenu.startButton.setText("Start")
            self.controlMenu.nextButton.setEnabled(True)

    def togglePlay(self, autoset):
        """Toggle between [Restart] (while stopped) and [Stop] (while playing)"""

        toggle = ("Restart", "Stop")
        if not self.started:
            self.controlMenu.rateButton.setEnabled(True)
            self.marktime = time()
            self.started = True
            self.setTask()
        if not autoset:
            self.log(detail=toggle[self.playing])  # User clicked button
        self.playing = (self.playing + 1) % 2  # Stopped -> Playing -> Stopped -> ...
        self.repeat("replay", [self.playing])
        self.controlMenu.startButton.setText(toggle[self.playing])
        if self.playing:
            selected = False
            for speaker in range(1, 9):
                if self.selected[speaker - 1]:
                    selected = True
                    self.controlPanel.setSpeakerEnabled(speaker, True)
            if not selected:
                for speaker in range(1, 9):
                    self.controlPanel.setSpeakerSelectable(speaker, True)
            self.controlMenu.pauseButton.setEnabled(True)
        else:
            self.controlPanel.setEnabled(False)
            self.controlMenu.pauseButton.setEnabled(False)

        # Button press always turns off pausing
        self.paused = 1         # Force state to resumed...
        self.togglePause(True)  # ...then toggle it

    def togglePause(self, autoset):
        """Toggle between [Pause] (while playing) and [Resume] (while paused)"""

        toggle = ("Pause", "Resume")
        if not autoset:
            self.log(detail=toggle[self.paused])  # User clicked button
        self.paused = (self.paused + 1) % 2  # Resumed -> Paused -> Resumed -> ...
        self.repeat("pauseSignal", [self.paused] * 8)
        self.repeat("pauseNoise",  [self.paused] * 8)
        if (self.paused) or (not self.guessed) or (not self.playing):
            self.controlPanel.signalPause.setEnabled(False)
            self.controlPanel.noisePause.setEnabled(False)
        else:
            self.controlPanel.signalPause.setEnabled(True)
            self.controlPanel.noisePause.setEnabled(True)
        # Button push always turns off pausing
        self.signalsPaused = 0
        self.signalPaused  = [self.signalsPaused] * 8
        self.noisesPaused  = 0
        self.noisePaused   = [self.noisesPaused]  * 8
        self.controlPanel.signalPause.setText("Pause\nSignal")
        self.controlPanel.noisePause.setText("Pause\nNoise")
        self.controlMenu.pauseButton.setText(toggle[self.paused])

    def toggleSignal(self):
        """Toggle the muting of the speaker(s) playing signal"""

        toggle = ("Pause", "Resume")
        self.log(detail="{0} Signal".format(toggle[self.signalsPaused]))
        self.signalsPaused = (self.signalsPaused + 1) % 2  # Resumed -> Paused -> Resumed
        buttonText = "{0}\nSignal".format(toggle[self.signalsPaused])
        self.controlPanel.signalPause.setText(buttonText)
        self.signalPaused = [self.signalsPaused] * 8
        self.repeat("pauseSignal", self.signalPaused)

    def toggleNoise(self):
        """Toggle the muting of the speaker(s) playing noise"""

        toggle = ("Pause", "Resume")
        self.log(detail="{0} Noise".format(toggle[self.noisesPaused]))
        self.noisesPaused = (self.noisesPaused + 1) % 2  # Resumed -> Paused -> Resumed
        buttonText = "{0}\nNoise".format(toggle[self.noisesPaused])
        self.controlPanel.noisePause.setText(buttonText)
        self.noisePaused = [self.noisesPaused] * 8
        self.repeat("pauseNoise", self.noisePaused)

    def nextTrial(self):
        """Begin a new trial"""

        self.trial += 1
        if self.trial > 0:         # If there was a previous trial...
            self.log(detail="Trial {0} ended with".format(self.trial),
                     offset=0)     # ...save previous trial's final values
        if self.trial in range(self.trials):
            self.preset()          # Set initial levels
        if self.trial == (self.trials - 1):
            self.controlMenu.nextButton.setText("Quit")
        elif self.trial > (self.trials - 1):
            now = strftime("%Y-%m-%d %H:%M")
            self.results.write("\n,,Completed exercise: {0}\n".format(now))
            self.results.close()
            self.repeat("replay", [0])  # Stopped
            logging.info("Quitting session\n")

            oscNotify = "/osc/notify/vcs/SoundAdvice"
            msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
            msg.add_arg(0)  # Turn notifications OFF (1 for ON)
            msg = msg.build()
            for speaking in range(10):
                self.osc.send(msg)
                sleep(0.01)

            oscRespondTo = "/osc/respond_to"
            msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
            msg.add_arg(0)  # Send feedback to port /dev/null
            msg = msg.build()
            for speaking in range(10):
                self.osc.send(msg)
                sleep(0.01)

            self.pacaListener.server.shutdown()
            self.pacaListener.quit()
            QApplication.quit()

        state = ("#af0000", "#00af00")  # red, green
        OFF, ON = 0, 1

        volume = "s{0}{1}"              # Volume buttons
        for speaker in range(1, 9):
            for direction in ("Louder", "Softer"):
                fader = self.controlPanel.findChild(QPushButton,
                                                    volume.format(speaker,
                                                                  direction))
                fader.setStyleSheet(GREY)

            self.selected[speaker - 1] = OFF
            restyle = self.style.format(state[OFF])
            self.controlPanel.speakers[speaker - 1]["select"].setStyleSheet(restyle)

        self.controlPanel.signalPause.setStyleSheet(GREY)
        self.controlPanel.noisePause.setStyleSheet(GREY)

        if self.playing:           # If playing...
            self.togglePlay(True)  # ...stop the sounds
        self.started = False       # Trial not started yet
        self.guessed = False       # No guesses made yet
        self.rated   = 0           # This trial has never been rated
        self.rate_pressed = 0      # Rating button never pressed
        self.program = 0           # No device program selected

        self.controlMenu.deviceSetting.setEnabled(True)
        self.controlPanel.setEnabled(False)
        self.controlMenu.setEnabled(False)
        self.controlMenu.startButton.setText("Start")

    def preset(self):
        """Set all speakers to initial levels from data file"""

        initialized = False

        logging.info("Trial: {0}".format(self.trial + 1))
        settings = self.presets[self.trial]

        self.controlMenu.deviceSetting.uncheckAll()
        for setting in range(self.programButtons):
            self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREY)

        self.program = settings["program"] if settings["program"] != 0 else -99

        if self.program == 99:
            self.program = random.randrange(1, self.programButtons + 1)

        if self.program != -99:  # Experimenter's choice
            self.controlMenu.deviceSetting.setEnabled(False)
            self.controlMenu.deviceSetting.ready[self.program - 1].setEnabled(True)
            self.controlMenu.deviceSetting.ready[self.program - 1].setStyleSheet(ORANGE)
            self.controlMenu.deviceSetting.ready[self.program - 1].setFocus()
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["SET ALD"]
                                           .format(program=self.program))
            self.results.write("{0},,Computed device program preset: {1}\n"
                               .format(self.trial + 1, self.program))
        else:                    # User's choice
            self.controlMenu.deviceSetting.setEnabled(True)
            for setting in range(self.programButtons):
                self.controlMenu.deviceSetting.ready[setting].setStyleSheet(ORANGE)
                self.controlMenu.deviceSetting.ready[setting].setFocus()
                self.controlMenu.deviceSetting.ready[setting].clearFocus()
            self.results.write("{0},,Computed device program preset: {1}\n"
                               .format(self.trial + 1, "User choice"))

        for speaker in range(8):
            self.signalState[speaker]  = settings["signal"][speaker]["state"]
            self.signalSample[speaker] = settings["signal"][speaker]["sample"]
            self.signalSample[speaker] = rescale(self.signalSample[speaker],
                                                  1, self.speakers[speaker]["signals"])
            self.signalLevel[speaker]  = settings["signal"][speaker]["level"]
            self.signalPaused[speaker]  = 0
            self.noiseState[speaker]   = settings["noise"][speaker]["state"]
            self.noiseSample[speaker]  = settings["noise"][speaker]["sample"]
            self.noiseSample[speaker]  = rescale(self.noiseSample[speaker],
                                                  1, self.speakers[speaker]["noises"])
            self.noiseLevel[speaker]   = settings["noise"][speaker]["level"]
            self.noisePaused[speaker]   = 0

        self.results.write("\n{0},,Starting trial {0}\n"
                           .format(self.trial + 1))

        self.results.write("{0},,\"Preset comment: {1}\"\n"
                           .format(self.trial + 1,
                                   settings["description"]))

        self.results.write("{0},,Device program preset: {1}\n"
                           .format(self.trial + 1, self.program))

        row = "{0},,Signal locations:".format(self.trial + 1)
        self.targets = []
        for target in settings["targets"]:    # Where signals are
            self.targets.append(target - 1)
            row += " {0}".format(target)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial signal samples:,".format(self.trial + 1)
        for speaker in range(8):
            signal = "signal sample {0}".format(speaker + 1)
            try:
                label = int(descale(self.signalSample[speaker],
                                    0, self.speakers[speaker]["signals"] - 1))
                s = widgetry[eventery[signal][0]]["samples"][label]
            except KeyError:
                s = "Error: Unknown"
            row += "{0}, ".format(s)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise samples:,".format(self.trial + 1)
        for speaker in range(8):
            noise = "noise sample {0}".format(speaker + 1)
            try:
                label = int(descale(self.signalSample[speaker],
                                    0, self.speakers[speaker]["noises"] - 1))
                n = widgetry[eventery[noise][0]]["samples"][label]
            except KeyError:
                n = "Error: Unknown"
            row += "{0}, ".format(n)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial signal levels:,".format(self.trial + 1)
        for level in self.signalLevel:
            row += "{0},".format(level)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise levels:,".format(self.trial + 1)
        for level in self.noiseLevel:
            row += "{0},".format(level)
        self.results.write("{0}\n".format(row))

        row = "{0},,Computed signal levels:,".format(self.trial + 1)
        for speaker in range(8):
            if self.signalLevel[speaker] == 99:
                self.signalLevel[speaker] = random.randrange(-12, 13, 3)
            if self.signalLevel[speaker] == -99:
                self.signalState[speaker] = 0
            row += "{0},".format(self.signalLevel[speaker])
        self.results.write("{0}\n".format(row))

        row = "{0},,Computed noise levels:,".format(self.trial + 1)
        for speaker in range(8):
            if self.noiseLevel[speaker] == 99:
                self.noiseLevel[speaker] = random.randrange(-12, 13, 3)
            if self.noiseLevel[speaker] == -99:
                self.noiseState[speaker] = 0
            row += "{0},".format(self.noiseLevel[speaker])
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial signal states:,".format(self.trial + 1)
        for state in self.signalState:
            row += "{0},".format(state)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise states:,".format(self.trial + 1)
        for state in self.noiseState:
            row += "{0},".format(state)
        self.results.write("{0}\n".format(row))

        # Change levels only, not GUI
        self.repeat("setSignalState",  self.signalState)
        self.repeat("setNoiseState",   self.noiseState)
        self.repeat("setSignalSample", self.signalSample)  # Prescaled!
        self.repeat("setNoiseSample",  self.noiseSample)   # Prescaled!
        self.repeat("pauseSignal",     self.signalPaused)
        self.repeat("pauseNoise",      self.noisePaused)
        self.repeat("setSignalLevel",  self.signalLevel, -12, 12)
        self.repeat("setNoiseLevel",   self.noiseLevel,  -12, 12)

        self.controlPanel.update()  # Repaint the control panel

        self.prolog = Prolog(self.instructions[self.trial]["PROLOG"],
                             width=self.width,
                             height=self.height,
                             center=self.center)
        self.prolog.exec_()

        initialized = True

    def getSetting(self):
        """Device program chosen. Enable [Restart], disable device program"""

        for setting in range(self.programButtons):
            self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREY)
            if self.controlMenu.deviceSetting.ready[setting].isChecked():
                self.program = setting + 1
                self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREEN)
        self.results.write("{0},,Device program selected: {1}\n"
                           .format(self.trial + 1, self.program))
        self.controlMenu.deviceSetting.setEnabled(False)

        self.controlMenu.startButton.setEnabled(True)
        self.controlMenu.nextButton.setEnabled(False)
        self.controlMenu.startButton.setFocus()

    def rate(self):
        """Record ratings in results, mute speakers and reset ratings panel"""

        self.rate_pressed += 1
        self.controlMenu.rateButton.setEnabled(False)
        dialog = QDialog()
        rater = RaterDialog(dialog, self)
        dialog.exec_()
        self.setTask()

    def log(self, detail="", ratings=None, offset=1):
        """Record user action and states in results"""

        timestamp    = round((time() - self.marktime), 3)
        row  = "{0},".format(self.trial + offset)
        row += "{0},".format(timestamp)
        row += "{0},".format(detail)
        for speaker in self.signalLevel:
            row += "{0},".format(speaker)
        row += ",,"                         # Signal, Noise
        if ratings:
            for rating in ratings:
                print("({0}) {1}".format(type(rating), rating))
                if isinstance(rating, int):
                    row += "{0},".format(rating)
                elif isinstance(rating, unicode) or isinstance(rating, list):
                    row += "\"{0}\",".format(rating)
        row += "\n"
        self.results.write(row)


class ProvideIP(QDialog, provideIP.Ui_provideIP):
    """Dialog for manual IP address entry"""

    def __init__(self, parent=None):
        """Provide a dialog window with four octets to fill"""

        super(ProvideIP, self).__init__(parent)  # Old way. Learn new way.
        self.setupUi(self)

        self.connectButton.clicked.connect(self.accept)


def main():
    """Instantiate the window, show it and start the app."""

    logging.info("[Localization]")

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    center = app.desktop().rect().center()
    # print QStyleFactory.keys() on Linux yields:
    # [u'Windows', u'Motif', u'CDE', u'Plastique', u'GTK+', u'Cleanlooks']

#   app.setStyle("Windows")     # Ugly
#   app.setStyle("Motif")       # Ugly
#   app.setStyle("CDE")         # Ugly
#   app.setStyle("Plastique")   # Better
#   app.setStyle("GTK+")        # Native on Linux. Pretty good
#   app.setStyle("Cleanlooks")  # Not bad! Text alignment better than native

    start = time()
    splash = QSplashScreen(QPixmap("images/splash_screen.svg"))
    splash.show()
    while time() - start < 3:  # Was 10 with full credits
        sleep(0.001)
        app.processEvents()

    si = SessionInfo("Localization", UI,
                     width=width, height=height, center=center,
                     feet=8, inches=8)
    splash.finish(si)
    si.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
