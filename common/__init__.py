#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Common routines
#
#  Copyright 2015 Kevin Cole <kevin.cole@novawebcoop.org> 2015.12.02
#
#  Signal to Noise: Adjust signal level (from a single source), and
#  adjust background noise level (coming from multiple sources)
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input           # use raw_input when I say input
import sys
from os.path import expanduser  # Cross-platform home directory finder

from time import strftime, time

__appname__    = "Sound Advice"
__module__     = "Common functions"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2015, Kevin Cole (2015.11.20)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"
