#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Common routines
#
#  Copyright 2015 Kevin Cole <kevin.cole@novawebcoop.org> 2015.12.02
#
#  Signal to Noise: Adjust signal level (from a single source), and
#  adjust background noise level (coming from multiple sources)
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input  # use raw_input when I say input
import sys
import re                     # Regular Expression functions
import os
import os.path                # Directory, file and path services

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca

from time import strftime, time, sleep

import logging

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Open Sound Control messenger

from PySide.QtCore import *
from PySide.QtGui  import *

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

wd = os.path.expanduser("~/Data/Sound Advice")
if not os.path.exists(wd):
    os.makedirs(wd, 0o755)
osclog = "{0}/osc.log".format(wd)
logging.basicConfig(filename=osclog,
                    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
                    datefmt="%m-%d %H:%M",
                    level=logging.DEBUG)

__appname__    = "Sound Advice"
__module__     = "Common functions"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2015, Kevin Cole (2015.11.20)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

osc = udp_client.UDPClient("127.0.0.1", 8000)  # Aaarg! Token placeholder!


def listify(obj):
    """Recurse thru dictionary splitting multi-line string values into lists"""
    for field in obj:
        if isinstance(obj[field], dict):
            listify(obj[field])
        elif isinstance(obj[field], stringTypes):
            if obj[field][-1] == u"\r":       # If string ends with newline...
                obj[field] = obj[field][:-1]  # ...remove it
            obj[field] = obj[field].split(u"\r")  # Paca uses Mac newline
            if len(obj[field]) == 1:        # If "list" is only one element...
                obj[field] = obj[field][0]  # ...unlist it
    return obj


def fetch(root, include=".*", exclude="^$"):
    """
    Returns a list of fully qualified filenames under 'root'
    matching regex 'include', excluding dirs matching regex 'exclude'
    """
    targets = re.compile(include)               # Files to include
    skip    = re.compile(exclude)               # Dirs  to exclude

    for path, dirs, files in os.walk(root):
        if not skip.search(path):               # If path is not excluded...
            for file in files:                  # ...check the files
                if not targets.search(file):    # ...If non-matching file...
                    continue                    # ......skip it
                yield os.path.join(path, file)  # Like "%s/%s" % (path, file)


def rescale(value, minimum, maximum):
    """Satisfy Kyma's perverse need for everything to be 0.0 to 1.0"""
    try:
        scaled = float(value - minimum) / float(maximum - minimum)
    except ZeroDivisionError:
        scaled = 0
    return scaled


def descale(value, minimum, maximum):
    """Satisfy Kyma's perverse need for everything to be 0.0 to 1.0"""
    scaled = ((maximum - minimum) * value) + minimum  # y = mx + b
    return scaled


def repeat(message):
    for speaking in range(10):
        osc.send(message)
        sleep(0.01)


def setLevel(osc_pipe, speaker, level):
    """Set level (in dB) for the specified loudspeaker"""
    toSpeaker = "/vcs/dBGain{0}/1".format(speaker)
    msg = osc_message_builder.OscMessageBuilder(address=toSpeaker)
    msg.add_arg(rescale(level, -12, 12))
    msg = msg.build()
    osc.send(msg)


def unjson(path):
    """
    Returns the dictionary loaded from a JSON file.
        path   = path to directory containing a set
    """

    path += "/"  # QFileDialog fails to add the trailing slash
    phylum = path.split("/")[5]   # "presets" or "ratings"

    if not os.path.isdir(path):
        raise Exception("{0} is not a directory!".format(path))

    fd = open(path + "00-index.json", "r")
    session = json.load(fd)
    fd.close()
    session[phylum] = []

    filenames = os.listdir(path)
    filenames.sort()
    filenames[:] = (filename for filename in filenames
                    if filename != "00-index.json")

    for filename in filenames:
        if os.path.isfile(path + filename) and \
           filename.endswith(".json"):
            fd = open(path + filename, "r")
            session[phylum].append(json.load(fd))

    return session


def squeeze(stanza):
    """Reformats a stanza, removing whitespace"""
    stanza = stanza.toPlainText()                 # No HTML, etc.
    stanza = stanza.replace("\t", " ")            # Convert [TAB]s to spaces
    stanza = re.sub(r"( ) +",    "\\1",  stanza)  # Remove excess whitespace
    stanza = re.sub(r" *(\n) *", "\\1",  stanza)  # Remove spaces around [LF]s
    stanza = re.sub(r"\n{3,}",   "\n\n", stanza)  # Maximum number of \n = 2
    while stanza and stanza[-1] == "\n":
        stanza = stanza[:-1]                     # Remove trailing [LF]s
    stanza = re.sub(r"^\s*$", "null", stanza, re.MULTILINE)
    if stanza == "null":                         # If all whitespace...
        return None                              # ...return None (null)
    else:
        return stanza.encode("ascii")            # No UTF-8, Unicode, etc.


handled = False  # Each time we send, set handled to False

naries = {}    # All JSON data for all widgets
widgetry = {}  # concreteEventID -> label map
eventery = {}  # label -> concreteEventID map
