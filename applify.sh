#!/bin/bash
# Written by Kevin Cole <kevin.cole@novawebcoop.org> 2015.10.23
# Generate a Mac OS X "app" from Python code.
#
rm -rf build dist
py2applet --make-setup $1 --iconfile icon.icns
python setup.py py2app -A
