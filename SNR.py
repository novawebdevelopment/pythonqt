#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  SNR.py
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.10.09
#
#  Signal to Noise Ratio: Adjust signal level (from a single source),
#  and adjust background noise level (coming from multiple sources)
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from   __future__ import print_function, absolute_import
from   six.moves  import input           # use raw_input when I say input

#import ipdb  # iPython debugger (relative of pdb)

from   os         import makedirs
from   time       import strftime, time, sleep
import sys
import os.path
import socket
import json

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca

from PySide.QtCore import *
from PySide.QtGui  import *

from common import provideIP    # Manual connect Paca dialog
from common.functions import *  # Connect to Paca, rescaling, etc.
from common.control   import *  # VCS control widget class

from UI import SNRUI            # Virtual Control Surface

from Prolog      import *       # An introduction...
from SessionInfo import *
from Rater       import *       # A ratings dialog window

from paca_listener import PacaListener

import random

__appname__    = "Sound Advice"
__module__     = "Signal to Noise Ratio"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole (2016.10.09)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Button colors

GREY   = "background: #e0e0e0;"  # Neutral (default state)
ORANGE = "background: #ffcc33;"  # Waiting to be clicked
GREEN  = "background: #33ff66;"  # Clicked and active


class UI(SNRUI.UI):
    """Main Signal to Noise Ratio Exploration User Interface (UI)"""

    def __init__(self, initValues, parent=None):
        """Construct the Signal to Noise Ratio window and fill with widgets"""

        super(UI, self).__init__(parent)  # Old way. Learn new way.

        self.osc            = initValues["osc"]
        self.subjectInfo    = initValues["subjectInfo"]
        self.kymaSound      = initValues["kymaSound"]
        self.programButtons = initValues["programButtons"]
        self.presets        = initValues["presets"]
        self.ratings        = initValues["ratings"]
        self.instructions   = initValues["instructions"]
        self.configuration  = initValues["configuration"]
        self.filename       = initValues["filename"]
        self.width          = initValues["width"]
        self.height         = initValues["height"]
        self.center         = initValues["center"]

        self.speakers       = [0] * 8

        self.signalState    = [0] * 8
        self.signalSample   = [0] * 8
        self.signalLevel    = [0] * 8
        self.signalPaused   = [0] * 8

        self.noiseState     = [0] * 8
        self.noiseSample    = [0] * 8
        self.noiseLevel     = [0] * 8
        self.noisePaused    = [0] * 8

        self.lines = len(self.presets)
        self.line  = 0

        self.trials    = len(self.presets)
        self.trial     = -1     # Sample pair not yet chosen
        self.marktime  = -1.0   # Time not started
        self.triggered = False  # Not yet auto-started
        self.playing   = 0
        self.paused    = 0

        # Button pressed counts

        self.ald_pressed   = 0  # One press for all ALD buttons?
        self.start_pressed = 0  # Odd = Start/Restart pressed. Even = Stop pressed
        self.pause_pressed = 0  # Odd = Pause pressed.         Even = Resume pressed
        self.rate_pressed  = 0  # Number of times current trial rated
        self.save_pressed  = 0  # Should ALWAYS match rate_pressed
        self.next_pressed  = 0  # Not really necessary

        self.targets = []
        self.rated = None
        self.program = None

        self.results = open(self.filename, "a")

        # Set up slots (actions) for signals from widgets

        self.setupUi(self, initValues)

        self.controlMenu.deviceSetting.device.buttonClicked[int].connect(self.getSetting)

        self.controlPanel.signalLevel.valueChanged.connect(self.setSignalLevel)
        self.controlPanel.noiseLevel.valueChanged.connect(self.setNoiseLevel)
        self.controls = {}

        self.controlMenu.startButton.clicked.connect(lambda: self.togglePlay(False))
        self.controlMenu.pauseButton.clicked.connect(lambda: self.togglePause(False))
        self.controlMenu.nextButton.clicked.connect(self.nextTrial)

        self.controlMenu.rateButton.clicked.connect(self.rate)

        self.pacaListener = PacaListener(self)
        self.pacaListener.talk.connect(self.handleResponse)
        self.pacaListener.fetched.connect(self.update)
        self.pacaListener.start()

        oscRespondTo = "/osc/respond_to"
        msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
        msg.add_arg(8001)  # Send feedback to port 8001
        msg = msg.build()
        for speaking in range(10):
            self.osc.send(msg)
            sleep(0.01)

        oscNotify = "/osc/notify/vcs/SoundAdvice"
        msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
        msg.add_arg(1)  # Turn notifications ON (0 for OFF)
        msg = msg.build()
        for speaking in range(10):
            self.osc.send(msg)
            sleep(0.01)

    def keyPressEvent(self, event):
        super(UI, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            print("DEBUG: SNR got the ESC")
            now = strftime("%Y-%m-%d %H:%M")
            self.results.write("\n,,ABORTED! [Esc] pressed: {0}\n"
                               .format(now))
            self.results.close()
            self.repeat("replay",          [0])        # Stop playing
            self.repeat("pauseSignal",     [0]   * 8)  # Resume all Signals
            self.repeat("pauseNoise",      [0]   * 8)  # Resume all Noises
            self.repeat("setSignalState",  [0]   * 8)  # Power OFF all Signals
            self.repeat("setNoiseState",   [0]   * 8)  # Power OFF all Noises
            self.repeat("setSignalSample", [0]   * 8)  # All Signals = Sample 1
            self.repeat("setNoiseSample",  [0]   * 8)  # All Noises  = Sample 1
            self.repeat("setSignalLevel",  [0.5] * 8)  # All Signals @ Midrange
            self.repeat("setNoiseLevel",   [0.5] * 8)  # All Noises  @ Midrange
            QApplication.closeAllWindows()

    def handleResponse(self, response):
        pass

    def update(self):
        """Update counts of signal & noise samples loaded into each speaker"""

        for speaker in range(8):
            self.speakers[speaker] = {}

            signal  = "signal sample {0}".format(speaker + 1)
            signals = len(widgetry[eventery[signal][0]]["samples"])
            self.speakers[speaker]["signals"] = signals

            noise   = "noise sample {0}".format(speaker + 1)
            noises  = len(widgetry[eventery[noise][0]]["samples"])
            self.speakers[speaker]["noises"] = noises
        self.show()
        self.raise_()
        if not self.triggered:
            self.triggered = True
            self.nextTrial()  # Begin

    def repeat(self, command, modifier, *scale):
        """Send redundant OSC messages to Paca"""

        message = self.pacaListener.command[command]
        pairs = len(message.args) / 2
        for arg in range(1, pairs + 1):
            if scale:
                lower, upper = scale
                setting = float(rescale(modifier[arg - 1], lower, upper))
            else:
                setting = float(modifier[arg - 1])
            message.args[(arg * 2) - 1] = ("f", setting)
        message = message.build()
        for speaking in range(10):
            self.osc.send(message)
            sleep(0.01)

    def setSignalState(self, speaker, state):
        """Mute or unmute signal in an individual speaker"""

        self.signalState[speaker - 1] = state
        self.repeat("setSignalState", self.signalState)

    def setNoiseState(self, speaker, state):
        """Mute or unmute noise in an individual speaker"""

        self.noiseState[speaker - 1] = state
        self.repeat("setNoiseState", self.noiseState)

    def setSignalLevel(self, level):
        """Set signal level (one speaker) based on user's slider's position"""

        self.log(detail="Set SIGNAL level")
        level *= 3  # Scale back up to between -12 and 12
        for target in self.targets:
            self.signalLevel[target] = level
        self.repeat("setSignalLevel", self.signalLevel, -12, 12)

    def setNoiseLevel(self, level):
        """Set noise level (all speakers) based on user's dial's position"""

        self.log(detail="Set NOISE level")
        level *= 3  # Scale back up to between -12 and 12
        self.noiseLevel = [level] * 8
        self.repeat("setNoiseLevel", self.noiseLevel, -12, 12)

    def setSignalSample(self, speaker, sample):
        """Load a Signal sample for an individual speaker"""

        self.signalSample[speaker - 1] = sample
        self.repeat("setSignalSample", self.signalSample)  # Prescaled!

    def setNoiseSample(self, speaker, sample):
        """Load a Noise sample for an individual speaker"""

        self.noiseSample[speaker - 1] = sample
        self.repeat("setNoiseSample", self.noiseSample)    # Prescaled!

    def setTask(self):
        """Set the instructions to the appropriate task"""

        if self.rate_pressed < len(self.instructions[self.trial]["TASK"])  and \
            self.instructions[self.trial]["TASK"][self.rate_pressed] is not None:
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["TASK"][self.rate_pressed])
            self.controlMenu.rateButton.setEnabled(True)
        elif self.trial == (self.trials - 1):
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["QUIT"])
            self.controlMenu.startButton.setEnabled(False)
            self.controlMenu.startButton.setText("Start")
            self.controlMenu.nextButton.setEnabled(True)
        else:
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["NEXT"])
            self.controlMenu.startButton.setEnabled(False)
            self.controlMenu.startButton.setText("Start")
            self.controlMenu.nextButton.setEnabled(True)

    def togglePlay(self, autoset):
        """Toggle between [Restart] (while stopped) and [Stop] (while playing)"""

        toggle = ("Restart", "Stop")
        if not self.started:
            self.controlMenu.rateButton.setEnabled(True)
            self.marktime = time()
            self.started = True
            self.setTask()
        if not autoset:
            self.log(detail=toggle[self.playing])  # User clicked button
        self.playing = (self.playing + 1) % 2  # Stopped -> Playing -> Stopped -> ...
        self.repeat("replay", [self.playing])
        self.controlMenu.startButton.setText(toggle[self.playing])
        if self.playing:
            self.controlPanel.setEnabled(True)
            self.controlMenu.pauseButton.setEnabled(True)
        else:
            self.controlPanel.setEnabled(False)
            self.controlMenu.pauseButton.setEnabled(False)

        # Button press always turns off pausing
        self.paused = 1         # Force state to resumed...
        self.togglePause(True)  # ...then toggle it

    def togglePause(self, autoset):
        """Toggle between [Pause] (while playing) and [Resume] (while paused)"""

        toggle = ("Pause", "Resume")
        if not autoset:
            self.log(detail=toggle[self.paused])  # User clicked button
        self.paused = (self.paused + 1) % 2  # Resumed -> Paused -> Resumed -> ...
        self.signalPaused = [self.paused] * 8
        self.noisePaused  = [self.paused] * 8
        self.repeat("pauseSignal", self.signalPaused)
        self.repeat("pauseNoise",  self.noisePaused)
        self.controlMenu.pauseButton.setText(toggle[self.paused])

    def nextTrial(self):
        """Begin a new trial"""

        self.trial += 1
        if self.trial > 0:  # If there was a previous trial...
            self.log(detail="Trial {0} ended with".format(self.trial),
                     offset=0)     # ...save previous trial's final values
        self.started = False       # Trial not started yet
        self.rated   = 0           # This trial has never been rated
        self.rate_pressed = 0      # Rating button never pressed
        self.program = 0           # No device program selected
        if self.playing:           # If playing...
            self.togglePlay(True)  # ...stop the sounds
        self.controlMenu.deviceSetting.setEnabled(True)
        self.controlPanel.setEnabled(False)
        self.controlMenu.setEnabled(False)
        self.controlMenu.setEnabled(False)
        self.controlMenu.startButton.setText("Start")

        if self.trial in range(self.trials):
            self.preset()        # Set initial levels
        if self.trial == (self.trials - 1):
            self.controlMenu.nextButton.setText("Quit")
        elif self.trial > (self.trials - 1):
            now = strftime("%Y-%m-%d %H:%M")
            self.results.write("\n,,Completed exercise: {0}\n".format(now))
            self.results.close()
            self.repeat("replay", [0])  # Stopped
            logging.info("Quitting session\n")

            oscNotify = "/osc/notify/vcs/SoundAdvice"
            msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
            msg.add_arg(0)  # Turn notifications OFF (1 for ON)
            msg = msg.build()
            for speaking in range(10):
                self.osc.send(msg)
                sleep(0.01)

            oscRespondTo = "/osc/respond_to"
            msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
            msg.add_arg(0)  # Send feedback to port /dev/null
            msg = msg.build()
            for speaking in range(10):
                self.osc.send(msg)
                sleep(0.01)

            self.pacaListener.server.shutdown()
            self.pacaListener.quit()
            QApplication.quit()

    def preset(self):
        """Set all speakers to initial levels from data file"""

        initialized = False

        for setting in range(self.programButtons):
            self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREY)

        self.results.write("\n{0},,Starting trial {0}\n"
                           .format(self.trial + 1))
        logging.info("Trial: {0}".format(self.trial + 1))
        settings = self.presets[self.trial]

        self.results.write("{0},,\"Preset comment: {1}\"\n"
                           .format(self.trial + 1,
                                   settings["description"]))

        self.program = settings["program"] if settings["program"] != 0 else -99
        self.results.write("{0},,Device program preset: {1}\n"
                           .format(self.trial + 1, self.program))

        if self.program == 99:
            self.program = random.randrange(1, self.programButtons + 1)

            self.controlMenu.deviceSetting.uncheckAll()

        if self.program != -99:  # Experimenter's choice
            self.controlMenu.deviceSetting.setEnabled(False)
            self.controlMenu.deviceSetting.ready[self.program - 1].setEnabled(True)
            self.controlMenu.deviceSetting.ready[self.program - 1].setStyleSheet(ORANGE)
            self.controlMenu.deviceSetting.ready[self.program - 1].setFocus()
            self.controlMenu.instruction.label.setText(self.instructions[self.trial]["SET ALD"]
                                           .format(program=self.program))
            self.results.write("{0},,Computed device program preset: {1}\n"
                               .format(self.trial + 1, self.program))
        else:                    # User's choice
            self.controlMenu.deviceSetting.setEnabled(True)
            for setting in range(self.programButtons):
                self.controlMenu.deviceSetting.ready[setting].setStyleSheet(ORANGE)
                self.controlMenu.deviceSetting.ready[setting].setFocus()
                self.controlMenu.deviceSetting.ready[setting].clearFocus()
            self.results.write("{0},,Computed device program preset: {1}\n"
                               .format(self.trial + 1, "User choice"))

        row = "{0},,Signal locations:".format(self.trial + 1)
        self.targets = []
        for target in settings["targets"]:    # Where signals are
            self.targets.append(target - 1)
            row += " {0}".format(target)
        self.results.write("{0}\n".format(row))

        for speaker in range(8):
            self.signalState[speaker]  = settings["signal"][speaker]["state"]
            self.signalSample[speaker] = settings["signal"][speaker]["sample"]
            self.signalSample[speaker] = rescale(self.signalSample[speaker],
                                                 1, self.speakers[speaker]["signals"])
            self.signalLevel[speaker]  = settings["signal"][speaker]["level"]
            self.signalPaused[speaker]  = 0

            self.noiseState[speaker]   = settings["noise"][speaker]["state"]
            self.noiseSample[speaker]  = settings["noise"][speaker]["sample"]
            self.noiseSample[speaker]  = rescale(self.noiseSample[speaker],
                                                 1, self.speakers[speaker]["noises"])
            self.noiseLevel[speaker]   = settings["noise"][speaker]["level"]
            self.noisePaused[speaker]   = 0

        row = "{0},,Initial signal samples:,".format(self.trial + 1)
        for speaker in range(8):
            signal = "signal sample {0}".format(speaker + 1)
            try:
                label = int(descale(self.signalSample[speaker],
                                    0, self.speakers[speaker]["signals"] - 1))
                s = widgetry[eventery[signal][0]]["samples"][label]
            except KeyError:
                s = "Error: Unknown"
            row += "{0}, ".format(s)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise samples:,".format(self.trial + 1)
        for speaker in range(8):
            noise = "noise sample {0}".format(speaker + 1)
            try:
                label = int(descale(self.signalSample[speaker],
                                    0, self.speakers[speaker]["noises"] - 1))
                n = widgetry[eventery[noise][0]]["samples"][label]
            except KeyError:
                n = "Error: Unknown"
            row += "{0}, ".format(n)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial signal levels:,".format(self.trial + 1)
        for level in self.signalLevel:
            row += "{0},".format(level)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise levels:,".format(self.trial + 1)
        for level in self.noiseLevel:
            row += "{0},".format(level)
        self.results.write("{0}\n".format(row))

        row = "{0},,Computed signal levels:,".format(self.trial + 1)
        for speaker in range(8):
            if self.signalLevel[speaker] == 99:
                self.signalLevel[speaker] = random.randrange(-12, 13, 3)
            if self.signalLevel[speaker] == -99:
                self.signalState[speaker] = 0
            row += "{0},".format(self.signalLevel[speaker])
        self.results.write("{0}\n".format(row))

        row = "{0},,Computed noise levels:,".format(self.trial + 1)
        for speaker in range(8):
            if self.noiseLevel[speaker] == 99:
                self.noiseLevel[speaker] = random.randrange(-12, 13, 3)
            if self.noiseLevel[speaker] == -99:
                self.noiseState[speaker] = 0
            row += "{0},".format(self.noiseLevel[speaker])
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial signal states:,".format(self.trial + 1)
        for state in self.signalState:
            row += "{0},".format(state)
        self.results.write("{0}\n".format(row))

        row = "{0},,Initial noise states:,".format(self.trial + 1)
        for state in self.noiseState:
            row += "{0},".format(state)
        self.results.write("{0}\n".format(row))

        # Change levels only, not GUI
        self.repeat("setSignalState",  self.signalState)
        self.repeat("setNoiseState",   self.noiseState)
        self.repeat("setSignalSample", self.signalSample)  # Prescaled!
        self.repeat("setNoiseSample",  self.noiseSample)   # Prescaled!
        self.repeat("setSignalLevel",  self.signalLevel, -12, 12)
        self.repeat("setNoiseLevel",   self.noiseLevel,  -12, 12)
        self.repeat("pauseSignal",     self.signalPaused)
        self.repeat("pauseNoise",      self.noisePaused)

        # Change GUI
        targets = self.targets[0]  # Assume multiple targets have same level
        self.controlPanel.signalLevel.setValue(int(self.signalLevel[targets] / 3))
        self.controlPanel.noiseLevel.setValue(int(self.noiseLevel[0] / 3))
        self.controlPanel.update()  # Repaint the control panel

        self.prolog = Prolog(self.instructions[self.trial]["PROLOG"],
                             width=self.width,
                             height=self.height,
                             center=self.center)
        self.prolog.exec_()

        initialized = True

    def getSetting(self):
        """Device program chosen. Enable [Restart], disable device program"""

        for setting in range(self.programButtons):
            self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREY)
            if self.controlMenu.deviceSetting.ready[setting].isChecked():
                self.program = setting + 1
                self.controlMenu.deviceSetting.ready[setting].setStyleSheet(GREEN)
        self.results.write("{0},,Device program selected: {1}\n"
                           .format(self.trial + 1, self.program))
        self.controlMenu.deviceSetting.setEnabled(False)

        self.controlMenu.startButton.setEnabled(True)
        self.controlMenu.nextButton.setEnabled(False)
        self.controlMenu.startButton.setFocus()

    def rate(self):
        """Record ratings in results, mute speakers and reset ratings panel"""

        self.rate_pressed += 1
        self.controlMenu.rateButton.setEnabled(False)
        dialog = QDialog()
        rater = RaterDialog(dialog, self)
        dialog.exec_()
        self.setTask()

    def log(self, detail="", ratings=None, offset=1):
        """Record user action and states in results"""

        timestamp    = round((time() - self.marktime), 3)
        signalLevel  = self.controlPanel.signalLevel.value() * 3
        noiseLevel   = self.controlPanel.noiseLevel.value()  * 3

        row  = "{0},".format(self.trial + offset)
        row += "{0},".format(timestamp)
        row += "{0},".format(detail)
        row += ",,,,,,,,"                    # Push values past speaker columns
        row += "{0},".format(signalLevel)
        row += "{0},".format(noiseLevel)
        if ratings:
            for rating in ratings:
                print("({0}) {1}".format(type(rating), rating))
                if isinstance(rating, int):
                    row += "{0},".format(rating)
                elif isinstance(rating, unicode) or isinstance(rating, list):
                    row += "\"{0}\",".format(rating)
        row += "\n"
        self.results.write(row)


class ProvideIP(QDialog, provideIP.Ui_provideIP):
    """Dialog for manual IP address entry"""

    def __init__(self, parent=None):
        """Provide a dialog window with four octets to fill"""

        super(ProvideIP, self).__init__(parent)  # Old way. Learn new way.
        self.setupUi(self)

        self.connectButton.clicked.connect(self.accept)


def main():
    """Instantiate the window, show it and start the app."""

    logging.info("[Signal to Noise Ratio]")

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    center = app.desktop().rect().center()

    # print QStyleFactory.keys() on Linux yields:
    # [u'Windows', u'Motif', u'CDE', u'Plastique', u'GTK+', u'Cleanlooks']

#   app.setStyle("Windows")     # Ugly
#   app.setStyle("Motif")       # Ugly
#   app.setStyle("CDE")         # Ugly
#   app.setStyle("Plastique")   # Better
#   app.setStyle("GTK+")        # Native on Linux. Pretty good
#   app.setStyle("Cleanlooks")  # Not bad! Text alignment better than native

    start = time()
    splash = QSplashScreen(QPixmap("images/splash_screen.svg"))
    splash.show()
    while time() - start < 3:  # Was 10 with full credits
        sleep(0.001)
        app.processEvents()

    si = SessionInfo("SNR", UI,
                     width=width, height=height, center=center,
                     feet=17, inches=0)
    splash.finish(si)
    si.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
