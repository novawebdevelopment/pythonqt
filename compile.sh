#!/bin/bash
# Last modified by Kevin Cole <kevin.cole@novawebcoop.org> 2018.06.01
#
# Converts command-line Python programs into apps with clickable icons.
#
# 2017.01.25 KJC - Now using setup.cfg for iconfile, resources, etc.
# 2017.01.25 KJC - Combined all 8 setup.py to a single applify.py
# 2017.01.25 KJC - Would LIKE to eliminate -A (alias) but... Mac OS X sucks.
# 2017.09.25 KJC - Now dependant on VirtualEnvWrapper setup.
# 2018.03.09 KJC - Create a Mac OS X alias with AppleScript
# 2018.06.01 KJC - Switched to Pipenv !!!
#

rm -rf build dist

pipenv run ./applify.py py2app -A

osascript alias.applescript
rm dist/Instructions.app/Contents/resources/icon.icns
ln -s /Users/kjcole/gits/rerc/SoundAdvice/python.icns dist/Instructions.app/Contents/resources/icon.icns
