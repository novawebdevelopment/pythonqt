#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  instructions.py
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.02.18
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Configure Sound Advice instructions"""

from __future__ import print_function, absolute_import

import sys
import os.path
import json
import re                         # Regular Expression functions

from   time import time, sleep
from   copy import deepcopy

from PySide.QtCore import *
from PySide.QtGui  import *

from common.functions import fetch, unjson, squeeze

from UI.selector      import SelectorUI
from UI               import PrologEditUI  # Prolog Editor

import SNREdit
import LocalizationEdit
import OptimizationEdit

__appname__    = "Configure Instructions"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

REMINDER = "Add instructions for Prolog and Task(s)"  # Placeholder
SET_ALD  = "Set your listening device to program {program} then press button "
SET_ALD += "number {program} above.\n\nThen press the START button below to "
SET_ALD += "listen."
NEXT     = "To continue to the next scenario, press the NEXT button."
QUIT     = "To exit the application, press the QUIT button."
UNUSED   = {"description": None,
            "PROLOG":  None,
            "SET ALD": None,
            "TASK":    [None, None, None, None, None],
            "NEXT":    None,
            "QUIT":    None}


class Selector(QDialog, SelectorUI):
    """Select unit, presets, ratings and instructions to edit"""

    def __init__(self, width, height, parent=None):
        """Construct a Dialog window and fill with widgets"""
        super(Selector, self).__init__(parent)
        self.parent = parent
        self.width  = width
        self.height = height
        self.ready  = 0b00000       # Choice completion flags.
        self.prompts = None
        self.files  = {}            # Trial file counts
        self.instructions_paths = None
        self.unit = None
        self.setupUi(self)

        # Accumulate preset descriptions for pull-down menu
        self.presets_paths = [None]
        config = os.path.expanduser("~/.config/sound-advice/presets/")
        presets = fetch(config, r".*index.*\.json", r"/archive(/.+)?$")
        for preset in presets:
            self.presets_paths.append(os.path.dirname(preset))
            json_file = open(preset, "r")
            description = json.load(json_file)["description"]
            self.presets.addItem(description)

        # Accumulate ratings descriptions for pull-down menu
        self.ratings_paths = [None]
        config = os.path.expanduser("~/.config/sound-advice/ratings/")
        ratings = fetch(config, r".*index.*\.json", r"/archive(/.+)?$")
        for rating in ratings:
            self.ratings_paths.append(os.path.dirname(rating))
            json_file = open(rating, "r")
            description = json.load(json_file)["description"]
            self.ratings.addItem(description)

        # Turn off ALL buttons until all conditions met
        self.presets.setEnabled(False)
        self.ratings.setEnabled(False)
        self.instructions.setEnabled(False)
        self.submitter.setEnabled(False)

        # Module choice (SNR, Localization, Optimization) made.
        self.snr.clicked.connect(self.point)
        self.localization.clicked.connect(self.point)
        self.optimization.clicked.connect(self.point)

        # Presets, Ratings or Instructions set chosen
        self.presets.activated.connect(self.chosen)
        self.ratings.activated.connect(self.chosen)
        self.instructions.activated.connect(self.chosen)

        # OK or Cancel chosen
        self.submitter.accepted.connect(self.accept)
        self.submitter.rejected.connect(self.reject)

    def point(self):
        """Point to unit instructions"""

        # Accumulate chosen module's instruct descriptions for pull-down menu
        self.unit = self.sender().objectName()
        self.instructions.clear()
        self.instructions.addItem("Choose instructions...")

        self.instructions_paths = [None]
        config = os.path.expanduser("~/.config/sound-advice/instructions/{0}"
                                    .format(self.unit))
        instructs = fetch(config, r".*index.*\.json", r"/archive(/.+)?$")
        for instruct in instructs:
            self.instructions_paths.append(os.path.dirname(instruct))
            json_file = open(instruct, "r")
            description = json.load(json_file)["description"]
            self.instructions.addItem(description)

        # Enable choices dependent on first selecting a module
        self.presets.setEnabled(True)
        self.ratings.setEnabled(True)
        self.instructions.setEnabled(True)

        self.ready &= 0b00111  # Instructions UNchosen
        self.ready |= 0b00001  # Unit chosen
        self.proceed()

    def chosen(self):
        """Set or clear bits for the selected configuration item"""
        sender = self.sender()
        index = sender.currentIndex()
        if   sender.objectName() == "presets":
            if index > 0: self.ready |= 0b00010       # Set   bit
            else:         self.ready &= 0b11101       # Clear bit
        elif sender.objectName() == "ratings":
            if index > 0: self.ready |= 0b00100       # Set   bit
            else:         self.ready &= 0b11011       # Clear bit
        elif sender.objectName() == "instructions":
            if index > 0: self.ready |= 0b11000       # Set   bits
            else:         self.ready &= 0b00111       # Clear bits
        self.proceed()

    def proceed(self):
        """If all conditions selected, allow moving forward"""
        if self.ready == 0b11111:             # If all bits ready...
            self.submitter.setEnabled(True)   # ...enable  submission
        else:                                 # Otherwise...
            self.submitter.setEnabled(False)  # ...disable submission

    def generate(self, presets_path, ratings_path, presets):
        """Generate instructions from the default template"""
        prefix = os.path.expanduser("~/.config/sound-advice/instructions/")
        template_path = "{0}/{1}/default/00-index.json".format(prefix,
                                                               self.unit)
        json_file = open(template_path, "r")
        self.prompts = json.load(json_file)
        self.prompts["presets"] = presets_path.split("/")[-1]
        self.prompts["ratings"] = ratings_path.split("/")[-1]
        self.prompts["instructions"] = []
        template_path = "{0}/{1}/default/template.json".format(prefix,
                                                               self.unit)
        json_file = open(template_path, "r")
        template = json.load(json_file)       # Instructions for edit
        for trial, preset in enumerate(presets):
            self.prompts["instructions"].append(deepcopy(template))
            self.prompts["instructions"][trial]["description"] = preset["description"]

    def accept(self):
        """Unit, presets, ratings, and instructions chosen. Begin editing"""
        print("Accepted")

        program_buttons = 6

        presets_path = self.presets_paths[self.presets.currentIndex()]
        self.files["presets"] = os.listdir(presets_path)
        self.files["presets"].sort()
        configuration = unjson(presets_path)  # Presets for each trial
        presets    = configuration["presets"]

        ratings_path = self.ratings_paths[self.ratings.currentIndex()]
        configuration = unjson(ratings_path)  # Ratings for entire edit
        ratings = configuration["ratings"]

        instructions_path = self.instructions_paths[self.instructions.currentIndex()]
        self.files["instructions"] = os.listdir(instructions_path)
        self.files["instructions"].sort()
        if instructions_path.endswith("/default"):
            self.generate(presets_path, ratings_path, presets)
        else:
            self.prompts = unjson(instructions_path)
            fill = 6 - len(self.prompts["instructions"])
            self.prompts["instructions"] += [deepcopy(UNUSED) for _ in range(fill)]
            for trial, preset in enumerate(presets):
                self.prompts["instructions"][trial]["description"] = preset["description"]
            self.prompts["unit"] = self.unit
            self.prompts["presets"] = presets_path.split("/")[-1]
            self.prompts["ratings"] = ratings_path.split("/")[-1]
            if self.files["presets"] != self.files["instructions"]:
                diff = len(self.files["presets"]) - len(self.files["instructions"])
                if diff > 0:                                       # Add empties
                    for trial in range(len(self.files["instructions"]) - 1,
                                       len(self.files["presets"])      - 1):
                        self.prompts["instructions"][trial]["PROLOG"]  = REMINDER
                        self.prompts["instructions"][trial]["SET ALD"] = SET_ALD
                        self.prompts["instructions"][trial]["TASK"][0] = REMINDER
                        self.prompts["instructions"][trial]["NEXT"]    = NEXT
                        self.prompts["instructions"][trial]["QUIT"]    = QUIT
                        print(self.prompts["instructions"][trial]["PROLOG"])
                elif diff < 0:                                     # Empty added
                    for trial in range(len(self.files["presets"]) - 1, 6):
                        self.prompts["instructions"][trial] = deepcopy(UNUSED)

        init_values = {"unit":           self.unit,
                       "programButtons": program_buttons,
                       "presetsPath":    presets_path.split("/")[-1],
                       "ratingsPath":    ratings_path.split("/")[-1],
                       "presets":        presets,
                       "ratings":        ratings,
                       "prompts":        self.prompts,
                       "meatRadius":     17 * 12,
                       "width":          self.width,
                       "height":         self.height,
                       "files":          self.files["presets"]}

        main_window = MainWindow(init_values, parent=self)

        super(Selector, self).accept()
        self.setParent(None)

    def reject(self):
        """Let's call the whole thing off. Abort."""
        print("Rejected")
        super(Selector, self).reject()
        sys.exit(1)                      # Fixes stuff. Why?


class Prolog(PrologEditUI.UI):
    """Prolog edit pop-up dialog"""

    def __init__(self, text=None, width=None, height=None, center=None,
                 parent=None):
        """Construct the Signal to Noise Ratio window and fill with widgets"""

        super(Prolog, self).__init__()  # Old way. Learn new way.

        self.text = text
        self.parent = parent

        self.resize(int(width * 0.60), int(height * 0.80))
        self.move(center - self.rect().center())
        self.div.setPlainText(self.text)

        self.saveButton.clicked.connect(self.save)
        self.cancelButton.clicked.connect(self.cancel)

    def save(self):
        """Save prolog to memory"""
        print("SAVED")
        intro = squeeze(self.div)
        if intro is None:
            error  = "This instruction cannot be blank"
            error = "<strong>{0}</strong>".format(error)
            suggestion = "Change ignored."
            box = QMessageBox()
            box.setIcon(QMessageBox.Warning)
            box.setWindowTitle("Invalid empty instruction")
            box.setText(error)
            box.setInformativeText(suggestion)
            box.exec_()
            self.div.setPlainText(self.text)
        else:
            self.parent.current["instructions"][self.parent.trial]["PROLOG"] = intro
            self.accept()

    def cancel(self):
        """Leave prolog in memory unchanged"""
        print("CANCELED")
        self.reject()


class MainWindow():
    """GUI main window"""

    lit  = "QPushButton {"
    lit += "  color:            #ff0000;"
    lit += "  background-color: #ffff00;"
    lit += "}"

    unlit  = "QPushButton {"
    unlit += "  color:            #7f0000;"
    unlit += "  background-color: #ffffcf;"
    unlit += "}"

    required = ("PROLOG", "TASK-1", "NEXT", "QUIT")  # Cannot be empty?

    def __init__(self, init_values, parent=None):
        """Construct the edit window from selected components"""
        self.parent      = parent
        self.choice      = init_values["unit"]
        self.files       = init_values["files"]
        self.presets     = init_values["presets"]
        self.current     = init_values["prompts"]
        self.backup      = deepcopy(self.current)  # for later comparison
        self.trials      = len(self.presets)
        self.trial       = 0                       # Trial 0 of trials
        self.step_number = 0
        self.task        = 0                       # Task within trial
        self.saved       = False                   # Never been saved
        self.folder      = None
        if   self.choice == "SNR":
            self.unit = SNREdit.UI(init_values, parent=self)
        elif self.choice == "Localization":
            self.unit = LocalizationEdit.UI(init_values, parent=self)
        elif self.choice == "Optimization":
            self.unit = OptimizationEdit.UI(init_values, parent=self)

        # Create multiple lists of trial step names, one list per trial
        self.steps = []
        for trial in range(self.trials):
            self.unit.instruction.hint.addItem("{0} of {1}: {2}"
                                               .format(trial + 1,
                                                       self.trials,
                                                       self.presets[trial]["description"]))
            steps = ["PROLOG"]
            for i, task in enumerate(self.current["instructions"][trial]["TASK"]):
                steps.append("TASK-{0}".format(i + 1))  # ["TASK"][{0}] later
            steps.append("NEXT")
            steps.append("QUIT")
            self.steps.append(steps)

        # Populate the trial steps pull-down with steps from the first trial
        # and point to the first task step within the first trial.
        for i, step in enumerate(self.steps[self.trial]):
            self.unit.instruction.step.addItem(step)
            if step == "TASK-1":
                self.step_number = i

        self.load(self.steps[self.trial][self.step_number])
        self.unit.instruction.step.setCurrentIndex(self.step_number)

        self.edited = False

        self.unit.instruction.hint.activated.connect(self.trial_edit)
        self.unit.instruction.step.activated.connect(self.step_edit)
        self.unit.instruction.save.clicked.connect(self.save)
        self.unit.instruction.exit.clicked.connect(self.exit)

    def save_state(self):
        """Save in memory the current state of the instructions"""
        step = squeeze(self.unit.instruction.text)

        if step is None and \
           self.steps[self.trial][self.step_number] in MainWindow.required:
            error  = "This instruction cannot be blank"
            error = "<strong>{0}</strong>".format(error)
            suggestion = "Change ignored."
            box = QMessageBox()
            box.setIcon(QMessageBox.Warning)
            box.setWindowTitle("Invalid empty instruction")
            box.setText(error)
            box.setInformativeText(suggestion)
            box.exec_()
            if self.steps[self.trial][self.step_number].startswith("TASK"):
                task = int(self.steps[self.trial][self.step_number][-1])
                step = self.current["instructions"][self.trial]["TASK"][task - 1]
                self.unit.instruction.text.setPlainText(step)
            else:
                step_name = self.steps[self.trial][self.step_number]
                step = self.current["instructions"][self.trial][step_name]
                self.unit.instruction.text.setPlainText(step)
            self.unit.instruction.step.setCurrentIndex(self.step_number)
            self.unit.instruction.hint.setCurrentIndex(self.trial)
            return 1                                                    # Error

        if self.steps[self.trial][self.step_number].startswith("TASK"):
            task = int(self.steps[self.trial][self.step_number][-1])
            self.current["instructions"][self.trial]["TASK"][task - 1] = step
        else:
            step_name = self.steps[self.trial][self.step_number]
            self.current["instructions"][self.trial][step_name] = step
        if self.current != self.backup:
            self.edited = True
            self.unit.instruction.save.setText("* SAVE *")
            self.unit.instruction.save.setStyleSheet(MainWindow.lit)
        return 0                                                      # Success

    def load(self, element):
        """Load the right instruction text from the ratsnest"""
        text = ""
        if element.startswith("TASK"):
            task = int(self.steps[self.trial][self.step_number][-1])
            text = self.current["instructions"][self.trial]["TASK"][task - 1]
        else:
            text = self.current["instructions"][self.trial][element]
        self.unit.instruction.text.setPlainText(text)

    def step_edit(self):
        """Set system to a different step for the current trial"""
        if self.save_state():  # If there was an error in saving the state...
            return 1           # ...abort and propagate error upwards
        if self.unit.instruction.step.currentIndex() == 0:
            screen_resolution = QApplication.desktop().screenGeometry()
            width, height = screen_resolution.width(), screen_resolution.height()
            center = QApplication.desktop().rect().center()

            prolog = Prolog(text=self.current["instructions"][self.trial]["PROLOG"],
                            width=width,
                            height=height,
                            center=center,
                            parent=self)
            prolog.exec_()
            if self.current != self.backup:
                self.edited = True
                self.unit.instruction.save.setText("* SAVE *")
                self.unit.instruction.save.setStyleSheet(MainWindow.lit)
        else:
            self.step_number = self.unit.instruction.step.currentIndex()
            self.load(self.steps[self.trial][self.step_number])
        return 0

    def trial_edit(self):
        """Set system to a different trial for the current step"""
        if self.save_state():  # If there was an error saving the state...
            return 1           # ...abort and propagate error upwards
        self.trial = self.unit.instruction.hint.currentIndex()

        self.unit.instruction.step.clear()                 # Out w/ the old...

        for i, step in enumerate(self.steps[self.trial]):
            self.unit.instruction.step.addItem(step)       # In  w/ the new...
            if step == "TASK-1":
                self.step_number = i

        self.load(self.steps[self.trial][self.step_number])
        self.unit.instruction.step.setCurrentIndex(self.step_number)
        return 0

    def save(self):
        """Save all instructions for all trials"""

        # Process the step currently being edited
        step = squeeze(self.unit.instruction.text)

        if self.steps[self.trial][self.step_number].startswith("TASK"):
            task = int(self.steps[self.trial][self.step_number][-1])
            self.current["instructions"][self.trial]["TASK"][task - 1] = step
        else:
            step_name = self.steps[self.trial][self.step_number]
            self.current["instructions"][self.trial][step_name] = step

        # Don't save if there are still unedited boilerplate instructions
        for number, steps in enumerate(self.current["instructions"]):
            if (steps["description"] is not None) and \
               (REMINDER in (steps["PROLOG"], steps["TASK"][0])):
                warn = "Trial {0}: PROLOG and / or TASK-1 need editing"
                warn = warn.format(number + 1)
                warn = "<strong>{0}</strong>".format(warn)
                suggestion  = "<p>One of the newly added  trial instruction "
                suggestion += "sets (trial {0}) is missing instructions for "
                suggestion += "either PROLOG or TASK-1. Please check both.</p>"
                suggestion = suggestion.format(number + 1)
                box = QMessageBox()
                box.setIcon(QMessageBox.Warning)
                box.setWindowTitle("PROLOG or TASK-1 instructions missing")
                box.setText(warn)
                box.setInformativeText(suggestion)
                box.setStandardButtons(QMessageBox.Ok)
                decision = box.exec_()
                return

        pretty_print = {"sort_keys":  True,
                        "indent":     4,
                        "separators": (",", ": ")}

        conf_dir = os.path.expanduser("~/.config/sound-advice/instructions/{0}"
                                      .format(self.choice))
        pattern = re.compile(conf_dir + "/.")
        while self.folder is None or not pattern.match(self.folder):
            self.folder = QFileDialog.getExistingDirectory(caption="Save as...",
                                                           dir=conf_dir)
            if self.folder == "":      # If [Cancel] pressed...
                return                 # Skip it all
            if not pattern.match(self.folder):
                error  = "You cannot save to this folder."
                error = "<strong>{0}</strong>".format(error)
                suggestion = "Select an existing folder from the list or create a new one."
                box = QMessageBox()
                box.setIcon(QMessageBox.Warning)
                box.setWindowTitle("Invalid folder")
                box.setText(error)
                box.setInformativeText(suggestion)
                box.exec_()

        # Remove old instructions and master description on first save
        if not self.saved:
            if os.listdir(self.folder):    # If directory not empty 1st time...
                warn  = "Overwrite existing instructions?"
                warn = "<strong>{0}</strong>".format(warn)
                suggestion  = "<p>You are about to overwrite an existing "
                suggestion += "set of instructions.</p>"
                suggestion += "<p>Click [OK] to continue or [Cancel] to "
                suggestion += "continue editing. (Then click [Save] again "
                suggestion += "to save with a new name.)</p>"
                box = QMessageBox()
                box.setIcon(QMessageBox.Warning)
                box.setWindowTitle("Overwriting existing instructions")
                box.setText(warn)
                box.setInformativeText(suggestion)
                box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                box.setDefaultButton(QMessageBox.Cancel)
                decision = box.exec_()
                if decision == QMessageBox.Cancel:
                    self.folder = None
                    return
                else:
                    for fid in os.listdir(self.folder):
                        os.remove("{0}/{1}".format(self.folder, fid))
            if "description" in self.current:
                del self.current["description"]

        # Ask for a master description and save master index
        if "description" not in self.current:  # If no description yet...
            description, okay = "", False
            popup = QInputDialog()
            popup.setWindowTitle("Enter a new instruction set description")
            popup.setInputMode(QInputDialog.TextInput)
            popup.setLabelText("Description:")
            popup.resize(1102, 150)
            while (not okay) or (not description.strip()):
                okay = popup.exec_()
                description = popup.textValue()
            self.current["description"] = description
        index = deepcopy(self.current)
        del index["instructions"]
        fileno = 0
        filepath = "{0}/{1}".format(self.folder, self.files[fileno])
        fileout = open(filepath, "w")
        fileout.write(json.dumps(index, **pretty_print))
        fileout.close()

        # Save instructions for each trial
        for steps in self.current["instructions"]:
            if steps["description"] is not None:
                fileno += 1
                filepath = "{0}/{1}".format(self.folder, self.files[fileno])
                fileout = open(filepath, "w")
                fileout.write(json.dumps(steps, **pretty_print))
                fileout.close()

        self.saved = True
        self.backup = deepcopy(self.current)
        self.edited = False
        self.unit.instruction.save.setText("Save")
        self.unit.instruction.save.setStyleSheet(MainWindow.unlit)

    def exit(self):
        """Save and exit or just quit"""
        if self.save_state():  # If there was an error in saving the state...
            return 1           # ...abort and propagate error upwards
        if self.edited:
            box = QMessageBox()
            box.setText("Instructions have unsaved changes")
            box.setInformativeText("Do you wish to exit without saving?")
            box.setStandardButtons(QMessageBox.Save    |
                                   QMessageBox.Discard |
                                   QMessageBox.Cancel)
            box.setDefaultButton(QMessageBox.Save)
            response = box.exec_()
            if   response == QMessageBox.Save:
                self.save()
                sys.exit(0)
            elif response == QMessageBox.Discard:
                sys.exit(0)
            elif response == QMessageBox.Cancel:
                return 0
        else:
            sys.exit(0)
        return 0           # Never reached. Only satisfies PyLint


def main():
    """The main routine"""

    # Instantiate a form, show it and start the app.
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()

    start = time()
    splash = QSplashScreen(QPixmap("images/splash_screen.svg"))
    splash.show()
    while time() - start < 3:  # Was 10 with full credits
        sleep(0.001)
        app.processEvents()

    selector = Selector(width, height)
    splash.finish(selector)
    selector.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
