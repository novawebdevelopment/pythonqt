# Written by Kevin Cole <kevin.cole@novawebdevelopment.org> 2018.03.09 (kjc)
#
# Creates alias from applications in a py2ap dist folder to the desktop.
#

#do shell script "rm -fv $HOME/Desktop/SNR"
#do shell script "rm -fv $HOME/Desktop/Localization"
#do shell script "rm -fv $HOME/Desktop/Optimization"
#do shell script "rm -fv $HOME/Desktop/Configure\\ Instructions"
#do shell script "rm -fv $HOME/Desktop/Testing"

tell application "Finder"

  delete "/Users/kjcole/Desktop/SNR" as POSIX file
  set myapp to POSIX file "/Users/kjcole/gits/rerc/SoundAdvice/dist/SNR.app" as alias
  make new alias to myapp at desktop
  set name of result to "SNR"

  delete "/Users/kjcole/Desktop/Localization" as POSIX file
  set myapp to POSIX file "/Users/kjcole/gits/rerc/SoundAdvice/dist/Localization.app" as alias
  make new alias to myapp at desktop
  set name of result to "Localization"

  delete "/Users/kjcole/Desktop/Optimization" as POSIX file
  set myapp to POSIX file "/Users/kjcole/gits/rerc/SoundAdvice/dist/Optimization.app" as alias
  make new alias to myapp at desktop
  set name of result to "Optimization"

  delete "/Users/kjcole/Desktop/Configure Instructions" as POSIX file
  set myapp to POSIX file "/Users/kjcole/gits/rerc/SoundAdvice/dist/instructions.app" as alias
  make new alias to myapp at desktop
  set name of result to "Configure Instructions"

  open information window of application file "Instructions.app" of folder "dist" of folder "SoundAdvice" of folder "rerc" of folder "gits" of folder "kjcole" of folder "Users" of startup disk
  open information window of document file "python.icns" of folder "SoundAdvice" of folder "rerc" of folder "gits" of folder "kjcole" of folder "Users" of startup disk
  close information window 2
  close information window 1
end tell
