#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  Optimization.py
#
#  Copyright 2015 Kevin Cole <kevin.cole@novawebcoop.org> 2015.12.02
#
#  Optimization: Find the signal(s) by moving an avatar through
#  a top-down view of virtual 2D space.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from   __future__ import print_function, absolute_import

from   time       import strftime, time, sleep
import sys

from PySide.QtCore import *
from PySide.QtGui  import *

from UI               import OptimizationEditUI   # Virtual Control Surface
from common.functions import *

__appname__    = "Sound Advice"
__module__     = "Optimization"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2015, Kevin Cole (2015.11.20)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Button colors

GREY   = "background: #e0e0e0;"  # Neutral (default state)
ORANGE = "background: #ffcc33;"  # Waiting to be clicked
GREEN  = "background: #33ff66;"  # Clicked and active
RED    = "background: #e00000;"  # Speaker max'ed or min'ed out


class UI(OptimizationEditUI.UI):
    """Main Optimization Exploration User Interface (UI)"""

    def __init__(self, initValues, parent=None):
        """Construct the Optimization window and fill with widgets"""
        super(UI, self).__init__()  # Old way. Learn new way.

        self.parent         = parent
        self.programButtons = initValues["programButtons"]
        self.meatRadius     = initValues["meatRadius"]
        self.presets        = initValues["presets"]
        self.ratings        = initValues["ratings"]

        self.trials = len(self.presets)

        self.setupUi(self, initValues)

    def keyPressEvent(self, event):
        super(UI, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            QApplication.closeAllWindows()


def main():
    """Instantiate the window, show it and start the app."""

    import json
    import os.path

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()

    wd = os.path.expanduser("~/.config/sound-advice/presets/default")
    configuration = unjson(wd)  # Presets for each trial
    presets = configuration["presets"]

    wd = os.path.expanduser("~/.config/sound-advice/ratings/default")
    configuration = unjson(wd)  # Ratings for entire session
    ratings = configuration["ratings"]

    wd = os.path.expanduser("~/.config/sound-advice/instructions/Optimization/default.json")
    fd = open(wd, "r")
    instructions = json.load(fd)              # Instructions for session

    initValues = {"programButtons": 6,
                  "presets":        presets,
                  "ratings":        ratings,
                  "meatRadius":     17 * 12,
                  "width":          width,
                  "height":         height,
                  "instructions":   instructions}

    ui = UI(initValues)
    ui.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
