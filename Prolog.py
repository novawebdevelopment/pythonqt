#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  Copyright 2018 Kevin Cole <kevin.cole@novawebcoop.org> 2018.06.05
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
# Solution to button focus issue provided by:
#
#     https://stackoverflow.com/a/45568125/447830
#

from   __future__ import print_function, absolute_import
from   six.moves  import input           # use raw_input when I say input

#import ipdb  # iPython debugger (relative of pdb)

import sys
import os.path
import socket
import json
from   os          import makedirs
from   time        import strftime, time, sleep

from pythonosc     import udp_client           # Open Sound Control UDP

from PySide.QtCore import *
from PySide.QtGui  import *

from UI import PrologUI
from ProvideIP import *
from common.functions import *  # Connect to Paca, rescaling, etc.

__appname__    = "Sound Advice"
__module__     = "Session Info Dialog Window"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018, Kevin Cole (2018.06.05)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"


class Prolog(QDialog, PrologUI.UI):
    """Researcher's subject data collection dialog"""

    def __init__(self, body, width=None, height=None, center=None,
                 parent=None):
        """Display introductory instructions dialog window"""

        super(Prolog, self).__init__(parent)

        self.setupUi(self)            # Ask for unit-wide settings

        self.resize(int(width * 0.60), int(height * 0.80))
        self.move(center - self.rect().center())

        self.div.setText(body)

        self.proceed.clicked.connect(self.launch)

    def launch(self):
        """Initialize conditions and launch main experiment UI"""

        self.close()


def main():
    import sys
    app = QApplication(sys.argv)

    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    center = app.desktop().rect().center()
    body = "Qwert!\n"

    prolog = Prolog(body, width=width, height=height, center=center)
    prolog.exec_()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
