#!/bin/bash
#
# install.sh
# Written by Kevin Cole <kevin.cole@novawebcoop.org> 2017.04.02
#
# This determines if we're in a Python virtual environment, and,
# if so, fetches and installs all the prerequisites.
#
# Note: This installer was designed with and for Mac OS X 10.12.4
# (Sierra).  py2app in particular, is used to "bundle" the python code
# into Mac apps and "brand" them with an icon I designed.
#

mkdir -p ~/.local/share/paca
mkdir -p ~/.config/sound-advice/presets/preset
mkdir -p ~/.config/sound-advice/ratings/rating
mkdir -p ~/.config/sound-advice/instructions/SNR/default
mkdir -p ~/.config/sound-advice/instructions/Localization/default
mkdir -p ~/.config/sound-advice/instructions/Optimization/default
mkdir -p ~/Code/dragana
chmod 755 ~/.config/sound-advice/instructions/SNR/default
chmod 755 ~/.config/sound-advice/instructions/Localization/default
chmod 755 ~/.config/sound-advice/instructions/Optimization/default
chmod 644 ~/.config/sound-advice/instructions/SNR/default/*
chmod 644 ~/.config/sound-advice/instructions/Localization/default/*
chmod 644 ~/.config/sound-advice/instructions/Optimization/default/*
rm -f ~/Code/dragana/Sound\ Advice
ln -s ~/.config/sound-advice ~/Code/dragana/Sound\ Advice
echo '{
 "unit": "SNR",
 "description": "Default SNR instructions",
 "presets": "default",
 "ratings": "default"
}' > ~/.config/sound-advice/instructions/SNR/default/00-index.json
echo '{
 "unit": "Localization",
 "description": "Default Localization instructions",
 "presets": "default",
 "ratings": "default"
}' > ~/.config/sound-advice/instructions/Localization/default/00-index.json
echo '{
 "unit": "Optimization",
 "description": "Default Optimization instructions",
 "presets": "default",
 "ratings": "default"
}' > ~/.config/sound-advice/instructions/Optimization/default/00-index.json
cp -v configurations/instructions/SNR.json                                   \
      ~/.config/sound-advice/instructions/SNR/default/template.json
cp -v configurations/instructions/Localization.json                          \
      ~/.config/sound-advice/instructions/Localization/default/template.json
cp -v configurations/instructions/Optimization.json                          \
      ~/.config/sound-advice/instructions/Optimization/default/template.json
chmod 444 ~/.config/sound-advice/instructions/SNR/default/*
chmod 444 ~/.config/sound-advice/instructions/Localization/default/*
chmod 444 ~/.config/sound-advice/instructions/Optimization/default/*
chmod 555 ~/.config/sound-advice/instructions/SNR/default
chmod 555 ~/.config/sound-advice/instructions/Localization/default
chmod 555 ~/.config/sound-advice/instructions/Optimization/default

pipenv update
source $(pipenv --venv)/bin/activate
  python $VIRTUAL_ENV/bin/pyside_postinstall.py -install
deactivate
rm -rf build dist
pipenv run ./applify.py py2app -A
