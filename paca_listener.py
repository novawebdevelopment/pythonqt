#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  PacaListener.py
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.10.23
#
#  A class that listens to and handles OSC messages from the
#  Paca(rana).
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

from   __future__ import print_function, absolute_import
import json

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages sent from Paca
from pythonosc import osc_server           # Listen for messages from Paca

from PySide.QtCore import *
from PySide.QtGui  import *

from common.functions import *  # Connect to Paca, rescaling, etc.
from common.control   import *  # VCS control widget class

handled = None


class PacaListener(QThread):
    """Listener for Paca responses"""

    talk    = Signal((str,))   # Smorgasbord of debugging messages
    fetched = Signal((bool,))  # All widgets fetched
    samples = Signal((int,))   # Number of samples reported by Paca

    def __init__(self, parent=None):
        """Initialize a QThread with default behavior"""
        print("DEBUG: Initializing PacaListener thread")
        super(PacaListener, self).__init__(parent)
        self.parent = parent
        self.controls = parent.controls
        self.osc      = parent.osc
        self.command = {}
        self.server = None
#       self.deleteMe = {}

    def handle_response_from(self, address, args):
        """Handles those rare events where Paca sends back an integer"""
        print("DEBUG: Entering response_from handler")
        global handled
        message = "Paca responding on port {0}".format(args)
        logging.debug(message)
        self.talk.emit(message)

    def handle_widget(self, address, args, stream):
        """Objectifies the JSON string returned by '/osc/widget #'."""
#       print("DEBUG: entering widget handler")
        global handled
        nary = listify(json.loads(stream))  # It's always a dictionary, we hope.
        naries[args] = nary  # len(naries) used to determine if all fetched
#       self.deleteMe[args] = nary
        if nary["creationClass"] == "VCSEventSourceComponent":
            uid = nary["concreteEventID"]
            label = nary["label"].lower()
            self.controls[label] = Control(nary)  # DANGER WILL ROBINSON !!! DEBUG
            widgetry[uid] = {}
            label = nary["label"].lower()
            widgetry[uid]["label"] = label
            eventery[label] = [uid, None]
            try:
                eventery[label][1] = osc_message_builder.OscMessageBuilder(address="/vcs")
                eventery[label][1].add_arg(uid)
                eventery[label][1].add_arg(0.0)  # Dummy argument
            except KeyError:
                vcsCommand = "/vcs/{0}/1".format(label)
                eventery[label][1] = osc_message_builder.OscMessageBuilder(address=vcsCommand)
            if "signal sample" in label:
                widgetry[uid]["samples"] = nary["tickMarksOrNil"]["rawLabelsString"]
                self.samples.emit(len(widgetry[uid]["samples"]))
            if "noise sample" in label:
                widgetry[uid]["samples"] = nary["tickMarksOrNil"]["rawLabelsString"]
                self.samples.emit(len(widgetry[uid]["samples"]))
#               print("DEBUG: {0}"
#                     .format(widgetry[uid]))
#               logging.debug("{0}"
#                             .format(widgetry[uid]))
        self.talk.emit("Processed a widget")

    def dump_paca(self, address, widgetId, value, rawval):
        """Dumps OSC data for debugging"""
        messages = []
        messages.append("Raw:    {0} {1} {2}".format(address, widgetId, rawval))
        messages.append("Cooked: {0} {1} {2}".format(address, widgetId, value))
        for widget in widgetry:
            messages.append("widgetry[{0}] = {1}"
                            .format(widget, widgetry[widget]))
        for message in messages:
            print(message)
            logging.error(message)
            self.talk.emit(message)

    def handle_value_change(self, address, args):
        """Handle messages that return values between 0.0 and 1.0"""
        print("DEBUG: entering value change handler")
        global handled
        blobs = [args[start:start + 8] for start in range(0, len(args), 8)]
        for count, blob in enumerate(blobs):
            widgetId =       struct.unpack("!i", blob[0:4])[0]
            rawval   =       struct.unpack("!f", blob[4:8])[0]
            value    = round(struct.unpack("!f", blob[4:8])[0], 3)
            loudspeaker = widgetry[widgetId]["label"][-1]
            if   "signal sample" in widgetry[widgetId]["label"].lower():
                try:
                    sample = int(value * (len(widgetry[widgetId]["samples"]) - 1))
                    message = "Signal {0} sample set to [{1}]" \
                              .format(loudspeaker,
                                      widgetry[widgetId]["samples"][sample])
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Number of signal samples: {0}. Computed sample index {1}" \
                              .format(len(widgetry[widgetId]["samples"]), sample)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "noise sample" in widgetry[widgetId]["label"].lower():
                try:
                    sample = int(value * (len(widgetry[widgetId]["samples"]) - 1))
                    message = "Noise {0} sample set to [{1}]" \
                              .format(loudspeaker,
                                      widgetry[widgetId]["samples"][sample])
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Number of noise samples: {0}. Computed sample index {1}" \
                              .format(len(widgetry[widgetId]["samples"]), sample)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "signal level"     in widgetry[widgetId]["label"].lower():
                try:
                    gain = descale(value, -12, 12)
                    message = "Signal {0} gain set to {1}" \
                              .format(loudspeaker, gain)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed Signal gain: {0}".format(gain)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "noise level"     in widgetry[widgetId]["label"].lower():
                try:
                    gain = descale(value, -12, 12)
                    message = "Noise {0} gain set to {1}" \
                              .format(loudspeaker, gain)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed Noise gain: {0}".format(gain)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "signal on"    in widgetry[widgetId]["label"].lower():
                try:
                    state = "ON" if value == 1.0 else "OFF"
                    message = "Signal {0} powered {1}" \
                              .format(loudspeaker, state)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed Signal state: {0}".format(state)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "noise on"    in widgetry[widgetId]["label"].lower():
                try:
                    state = "ON" if value == 1.0 else "OFF"
                    message = "Noise {0} powered {1}" \
                              .format(loudspeaker, state)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed Noise state: {0}".format(state)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "replay"     in widgetry[widgetId]["label"].lower():
                try:
                    state = "STARTED" if value == 1.0 else "STOPPED"
                    message = "Sample playback has {0} on speaker {1}" \
                              .format(state, count + 1)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed state: {0}".format(state)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
            elif "pause signal" in widgetry[widgetId]["label"].lower():
                try:
                    state = "PAUSED" if value == 1.0 else "RESUMED"
                    message = "Signal playback has {0} on speaker {1}" \
                              .format(state, count + 1)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed state: {0}".format(state)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
            elif "pause noise" in widgetry[widgetId]["label"].lower():
                try:
                    state = "PAUSED" if value == 1.0 else "RESUMED"
                    message = "Noise playback has {0} on speaker {1}" \
                              .format(state, count + 1)
                    logging.info(message)
                    self.talk.emit(message)
                except:
                    message = "Computed state: {0}".format(state)
                    print(message)
                    logging.error(message)
                    self.talk.emit(message)
                    self.dump_paca(address, widgetId, value, rawval)
#                   raise
#       handled = True

    def fetch_widgets(self, address, args):
        """Request the info on all widgets"""
        print("DEBUG: Entering PacaListener {0} fetch_widgets".format(args))
        global handled
        while len(naries) != args:  # Continue requesting until full
            for widget in range(args):
                # Request widget info JSON bundle
                oscWidget = "/osc/widget"
                msg = osc_message_builder.OscMessageBuilder(address=oscWidget)
                msg.add_arg(widget)
                msg = msg.build()
                self.osc.send(msg)
        empties = []
        for widget in widgetry:
            if not len(widgetry[widget]):  # Empty
                empties.append(widget)
#       deleteMe = open("deleteMe.txt", "wb")
#       deleteMe.write(json.dumps(self.deleteMe, sort_keys=True,
#                      indent=4, separators=(',', ': ')))
#       deleteMe.close()

        # Create /vcs messages with eight [eventID, argument] pairs
        #

        self.command["setSignalSample"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("signal sample", speaker)
            self.command["setSignalSample"].add_arg(eventery[label][0])  # ID
            self.command["setSignalSample"].add_arg(float(self.parent.signalSample[speaker - 1]))

        self.command["setNoiseSample"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("noise sample", speaker)
            self.command["setNoiseSample"].add_arg(eventery[label][0])  # ID
            self.command["setNoiseSample"].add_arg(float(self.parent.noiseSample[speaker - 1]))

        self.command["setSignalLevel"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("signal level", speaker)
            self.command["setSignalLevel"].add_arg(eventery[label][0])  # ID
            self.command["setSignalLevel"].add_arg(float(self.parent.signalLevel[speaker - 1]))

        self.command["setNoiseLevel"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("noise level", speaker)
            self.command["setNoiseLevel"].add_arg(eventery[label][0])  # ID
            self.command["setNoiseLevel"].add_arg(float(self.parent.noiseLevel[speaker - 1]))

        self.command["setSignalState"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("signal on", speaker)
            self.command["setSignalState"].add_arg(eventery[label][0])  # ID
            self.command["setSignalState"].add_arg(float(self.parent.signalState[speaker - 1]))

        self.command["setNoiseState"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("noise on", speaker)
            self.command["setNoiseState"].add_arg(eventery[label][0])  # ID
            self.command["setNoiseState"].add_arg(float(self.parent.noiseState[speaker - 1]))

        self.command["replay"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        self.command["replay"].add_arg(eventery["replay"][0])  # ID
        self.command["replay"].add_arg(float(self.parent.playing))

        self.command["pauseSignal"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("pause signal", speaker)
            self.command["pauseSignal"].add_arg(eventery[label][0])  # ID
            self.command["pauseSignal"].add_arg(float(self.parent.signalPaused[speaker - 1]))

        self.command["pauseNoise"] = osc_message_builder.OscMessageBuilder(address="/vcs")
        for speaker in range(1, 9):
            label = "{0} {1}".format("pause noise", speaker)
            self.command["pauseNoise"].add_arg(eventery[label][0])  # ID
            self.command["pauseNoise"].add_arg(float(self.parent.noisePaused[speaker - 1]))

#       for event in eventery:
#           print("DEBUG: eventery[{0}]: {1}".format(event, eventery[event]))
#           logging.debug("eventery[{0}]: {1}".format(event, eventery[event]))

        print("DEBUG: {0} VCS control widgets".format(len(self.controls)))
        logging.debug("All {0} widgets processed".format(args))

        handled = True
        self.fetched.emit(True)  # All widgets have been fetched & processed

##############################################################################

    def run(self):  # Must override the default run method of QThread
        """Start the Paca OSC listener thread"""
        print("DEBUG: Start running PacaListener thread in background")
        # Set up the server (listener)
        director = dispatcher.Dispatcher()
        print("DEBUG: An event dispatcher is instantiated...")
        director.map("/osc/response_from",          self.handle_response_from)
        director.map("/osc/widget",                 self.handle_widget)
        director.map("/osc/notify/vcs/SoundAdvice", self.fetch_widgets)
        director.map("/vcs",                        self.handle_value_change)
        self.server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8001), director)
#                   = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8001),   director)
        self.server.serve_forever()

    # Explore .wait() method and .stop() method of QThread
    # There is also a rude .terminate() method. Avoid.
