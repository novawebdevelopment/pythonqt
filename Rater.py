#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Rater.py
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.12.17
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

from __future__ import print_function
from six.moves  import input           # use raw_input when I say input
from os.path    import expanduser      # Cross-platform home directory finder

from PySide.QtCore import *
from PySide.QtGui  import *

import sys

from UI import RatingsUI

__appname__    = "Sound Advice"
__module__     = "Rater"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class RaterDialog(QDialog, RatingsUI.UI):
    def __init__(self, widget, parent=None):
        """Construct a Dialog window and fill with widgets"""
        super(RaterDialog, self).__init__(parent)  # Old way. Learn new way.
        self.parent = parent
        self.widget = widget
        self.ratings = parent.ratings
        self.setupUi(widget, self.ratings)
        self.ratingsMenu.rateButton.clicked.connect(self.accept)

    def accept(self):
        """Save the ratings to the CSV results file"""
        ratings = []
        for rating in self.ratingsPane.ratings:
            if rating.getValue() not in (None, ""):
                ratings.append(rating.getValue())

        if len(ratings) == len(self.ratingsPane.ratings):
            self.parent.log("Rated current levels", ratings)
            self.parent.rated += 1            # Number of times trial rated
            if self.parent.playing:           # If playing...
                self.parent.togglePlay(True)  # ...stop the sounds
            self.parent.controlMenu.deviceSetting.setEnabled(True)
            self.widget.accept()
        else:
            oops = QMessageBox()
            oops.setIcon(QMessageBox.Warning)
            oops.setWindowTitle("Ratings not saved")
            oops.setText("<strong>Ratings not saved</strong>")
            explain  = "<p>Your information has not been saved.</p>"
            explain += "<p>You must rate all three experiences.</p>"
            oops.setInformativeText(explain.replace(" ", "&nbsp;"))
            oops.exec_()


def main():
    """Instantiate a form, show it and start the app."""
    import os.path
    from common.functions import unjson
    ratingsPath = os.path.expanduser("~/.config/sound-advice/ratings/")
    configuration = unjson(ratingsPath + "default")
    ratings = configuration["ratings"]

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    app = QApplication(sys.argv)
#+  Widget = QWidget()
#+  ui = RaterDialog(Widget, ratings)
#+  Widget.show()
#+  for twiddle in range(10):         # Twiddle our thumbs while waiting...
#+      QApplication.processEvents()  # ...for resize, et al to finish
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
