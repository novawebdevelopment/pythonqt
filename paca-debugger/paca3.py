#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Paca OSC Simulator

This sends semi-realistic Paca OSC responses to OSC requests, so
that the main routines will no longer be dependent on an actual Paca
for testing.

It now  advertises itself as a Rendezvous / Bonjour / ZeroConf / Avahi
service!

See: Creating a program to be broadcasted by Avahi

  http://stackoverflow.com/questions/1534655/\
  creating-a-program-to-be-broadcasted-by-avahi/18104922
"""

#  See https://docs.python.org/3.3/tutorial/classes.html
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.05.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input    # use raw_input when I say input
from itertools  import tee, zip_longest

from   zeroconf import *         # Bonjour, Avahi, Zeroconf, etc.
import socket                    # Ditto (avahi "dependency")
import netifaces as ni           # We are the network interfaces that say "Ni!"
from   netifaces import AF_INET  # AF_INET6, AF_LINK, AF_PACKET, AF_BRIDGE

import sys
import re         # Regular Expression functions
import os.path    # File and directory manipulations

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca
import binascii   # Especially hexlify, for debugging
import threading  # Could it be this simple???
import signal     # Handle Ctrl-C interrupts

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages returned from Paca
from pythonosc import osc_server           # Listen for messages from Paca

__appname__    = "Sound Advice"
__module__     = "Paca OSC Simulator"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2016, Kevin Cole (2016.05.13)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

ESC = chr(27)

jsons = []
soundscapes = []
passer = {}


# See https://docs.python.org/3/library/itertools.html#itertools-recipes
#
def grouper(iterable, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * 2
    return zip_longest(*args, fillvalue=fillvalue)


class Paca(object):
    """Symbolic Sound Paca Emulator"""

    def __init__(self, widgets):
#       self.osc = udp_client.UDPClient("127.0.0.1", 8001)
        self.osc = udp_client.UDPClient("0.0.0.0", 8001)

        self.response_port = None
        self.notification  = False
        self.widgets = widgets

        # Set up the server (listener)
        self.director = dispatcher.Dispatcher()
        self.director.map("/osc/respond_to", self.handle_respond_to)
        self.director.map("/osc/notify/vcs/SoundAdvice", self.count_widgets)
        self.director.map("/osc/widget", self.handle_widget)
        self.director.map("/vcs", self.handle_vcs)
        self.director.set_default_handler(self.wtf)

        #self.server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8000),
        #                                               self.director)
        self.server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8000),
                                                     self.director)

        self.server_thread = threading.Thread(target=self.server.serve_forever)
        passer["server"] = self.server
        passer["thread"] = self.server_thread

    def get_by_id(self, event_id):
        """Retrieve a widget based on its event ID"""
        for widget in self.widgets:
            if "concreteEventID" in widget:
                if widget["concreteEventID"] == event_id:
                    return widget

    def handle_respond_to(self, addr, args):
        """Handles those rare events where Paca sends back an integer"""
        print("DEBUG: handle_response_to responding to {0}, {1}"
              .format(addr, args))
        osc_response_from = "/osc/response_from"
        msg = osc_message_builder.OscMessageBuilder(address=osc_response_from)
        msg.add_arg(34789)
        msg = msg.build()
        self.osc.send(msg)
        print("{0}[31;1m{1} {2}{0}[0m".format(ESC, addr, args))

    def handle_widget(self, addr, args):
        """Return JSON representation of widget"""
        print("DEBUG: handle_widget responding to {0}, {1}"
              .format(addr, args))

        try:
            widget = json.dumps(self.widgets[args])
        except IndexError:
            widget = ""

        print("{0}[35;1maddr: {1}{0}[0m".format(ESC, addr))
        print("{0}[35;1margs: {1}{0}[0m".format(ESC, args))
        print("{0}[35;1mnary: {1}{0}[0m".format(ESC, widget))

        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(args)
        msg.add_arg(widget)
        msg = msg.build()
        self.osc.send(msg)

    def count_widgets(self, addr, args):
        print("DEBUG: count_widgets responding to {0}, {1}"
              .format(addr, args))
        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(len(self.widgets))
        msg = msg.build()
        self.osc.send(msg)

    def handle_vcs(self, addr, *args):
        print("DEBUG: handle_vcs responding to {0}, {1}"
              .format(addr, args))
        fmt = "{0}[31;1m{1}"
        for i in range(len(args)):
            fmt += " {{{0}}}".format(i + 2)
        fmt += "{0}[0m"
        print(fmt.format(ESC, addr, *args))
#       msg = osc_message_builder.OscMessageBuilder(address=addr)
        for addr, arg in grouper(args):
            print("    {0}  {1}".format(addr, arg))
        for addr, arg in grouper(args):
            widget = self.get_by_id(addr)
            command = "/vcs/{0}/1".format(widget["label"])
            msg = osc_message_builder.OscMessageBuilder(address=command)
            msg.add_arg(arg)
            msg = msg.build()
            self.osc.send(msg)

#    def wtf(self, addr, *args):
#        """WTF???"""
#        print("DEBUG: wtf responding to {0}, {1}"
#              .format(addr, args))
#        print("{0}[35;1margs: {1} {2}{0}[0m".format(ESC, addr, *args))
#        print("{0}[35;1mlength: {1}{0}[0m".format(ESC, len(args)))
#        for arg in args:
#            print("{0}[35;1marg: {1}{0}[0m".format(ESC, arg))
#        command = "/wtf"
#        msg = osc_message_builder.OscMessageBuilder(address=command)
#        for arg in args:
#            msg.add_arg(arg)
#        msg.add_arg("{'wtf':'What the fuck???'}")
#        msg = msg.build()
#        self.osc.send(msg)

    def wtf(self, *args, **kwargs):
        """WTF???"""
        print("DEBUG: wtf responding")
        if args is not None:
            for x, arg in enumerate(args):
                print("arg {0}: {1}".format(x, arg))
        else:
            print("No regular args")
        if kwargs is not None:
            for key, value in kwargs.items():
                print("arg {0}: {1}".format(key, value))
        else:
            print("No keyword args")


def flush_network(signum, frame):
    signal.signal(signal.SIGINT, original_sigint)
    paca = passer["server"]
    tred = passer["thread"]
    paca.shutdown()
    zeroconf.unregister_all_services()
    signal.signal(signal.SIGINT, flush_network)
    sys.exit()


def main():
    """Compile, load and start the "Sound" in Paca"""

    if len(sys.argv) == 2:
        kyma = sys.argv[1]
    elif len(sys.argv) == 1:
        kyma = input("Paca JSON dump file [kyma.json]: ")
    else:
        print("Too many arguments provided")
        sys.exit(2)  # 2 = Unix command line syntax error
    if kyma == "":
        kyma = "kyma"

    wd = os.path.expanduser("~/.local/share/paca")
    kyma = os.path.splitext(kyma)[0]    # Eliminate the extension if provided
    full_path = "{0}/{1}.json".format(wd, kyma)

    dump = open(full_path, "r")
    widgets = json.load(dump)

    paca = Paca(widgets)

    paca.server_thread.start()


if __name__ == "__main__":
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, flush_network)

    addr = ""
    ip_capable = re.compile("(en[0-9]+|en.*|wl.*)")   # Interfaces of interest
    for interface in ni.interfaces():
        if ip_capable.match(interface):               # Can it have an IP addr?
            if AF_INET in ni.ifaddresses(interface):  # Does it?
                ips = ni.ifaddresses(interface)[AF_INET]
                for ip in ips:
                    if "addr" in ip:
                        addr = ip["addr"]

    props = {}
    info = ServiceInfo("_osc._udp.local.",
                       "beslime-833._osc._udp.local.",
                       address=socket.inet_aton(addr), port=8000,
                       weight=0, priority=0,
                       properties=props,
                       server="ncc-1701._osc._udp.local.")
    zeroconf = Zeroconf()
    zeroconf.register_service(info, ttl=36000)

    main()
