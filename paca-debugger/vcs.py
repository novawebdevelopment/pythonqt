#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  VCS
#  Written by Kevin Cole <kevin.cole@novawebdevelopment.org> 2017.08.21
#
#  This will build an SVG from a Paca-dumpped JSON file in preparation for
#  mimicing the Kyma Virtual Control Surface (VCS)
#

import os.path
import json


def unfuck(vcs):
    """Convert a list of key/value pairs to a dictionary, as God intended"""

    # STUPID, F'ING CrappyTalk!!!  WHY? Why suddenly put a LIST of key/value
    # pairs where a DICTIONARY belongs???  Now I've got to mung the whole
    # damned thing!

    for control in vcs:
        if isinstance(control, dict)                              and \
           "lookOrNil"        in control                          and \
           isinstance(control["lookOrNil"], dict)                 and \
           "database"         in control["lookOrNil"]             and \
           isinstance(control["lookOrNil"]["database"], dict)     and \
           "flatAssociations" in control["lookOrNil"]["database"] and \
           isinstance(control["lookOrNil"]["database"]["flatAssociations"], list):
            l =  control["lookOrNil"]["database"]["flatAssociations"]  # list
            d = {}                                                     # dict
            for k in range(0, len(l), 2):
                d[l[k]] = l[k + 1]
            control["lookOrNil"]["database"]["flatAssociations"] = d
    return vcs


def rgb(pallet):
    """Scale the colors and convert to an RGB hexadecimal string"""

    hex_rgb  = "#"
    hex_rgb += "{0:02x}".format(int(pallet["red"]   * 255))
    hex_rgb += "{0:02x}".format(int(pallet["green"] * 255))
    hex_rgb += "{0:02x}".format(int(pallet["blue"]  * 255))
    return hex_rgb


paca = os.path.expanduser("~/.local/share/paca")  # Virtual paca JSON files

svg_template = """\
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
                     "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="{0}" height="{1}"
     viewBox="0 0 {0} {1}"
     xmlns="http://www.w3.org/2000/svg" version="1.1">
  <title>{2}</title>
{3}
</svg>
"""

style_template  = "fill:{0}; fill-opacity:{1};"
style_template += "stroke:{2}; stroke-opacity:{3}; stroke-width:{4};"

rectangle_template  = """<rect x="{0}" y="{1}" """
rectangle_template += """      width="{2}" height="{3}" """
rectangle_template += """      style="{4}" />\n"""

label_template = """<text x="{0}" y="{1}">{2}</text>"""

display_types  = ["Fader", "Select from List", "Toggle"]  # More someday

title          = "Kyma Virtual Control Surface (VCS)"
fill_color     = "#e7e7e7"  # Light gray
fill_opacity   = 1.0        # Completely opaque (was 0.1)
stroke_color   = "#a7a7a7"  # Dark gray
stroke_opacity = 1.0        # Completely opaque (was 0.9)
stroke_width   = 2          # Not too thick

frame = style_template.format(fill_color,   fill_opacity,
                              stroke_color, stroke_opacity,
                              stroke_width)

body = ""
horizontal, vertical = 0, 0  # Full width, full height

source = input("                   Paca JSON dump file: ")
destin = input("Virtual Control Surface (VCS) SVG file: ")

source = os.path.splitext(source)[0]    # Eliminate the extension if provided
paca = "{0}/{1}.json".format(paca, source)

vcs = json.load(open(paca))
vcs = unfuck(vcs)

for control in vcs:
    labels = None
    if isinstance(control, dict):
        if "displayType" in control and control["displayType"] == "Toggle":
            colors = control["lookOrNil"]["database"]["flatAssociations"]

            off_color  = rgb(colors["vcsButtonDownColor"])
            on_color   = rgb(colors["vcsButtonUpColor"])

            fill_color = on_color

            style = style_template.format(fill_color,   fill_opacity,
                                          stroke_color, stroke_opacity,
                                          stroke_width)

            labels = control["tickMarksOrNil"]["rawLabelsString"]
        else:
            style = frame
            labels = None

        for property in control:
            if property == "layout":
                layout = control[property]
                x1     = layout["leftOffset"]
                y1     = layout["topOffset"]
                x2     = layout["rightOffset"]
                y2     = layout["bottomOffset"]
                width  = x2 - x1
                height = y2 - y1

                rectangle = rectangle_template.format(x1, y1,
                                                      width, height,
                                                      style)
                body += rectangle

                if labels:
                    label = label_template.format(x1 + 10,
                                                  y1 + 10,
                                                  labels[0])
                    body += label

                horizontal = max(horizontal, x2)
                vertical   = max(vertical,   y2)

src = svg_template.format(horizontal, vertical, title, body)

svg = open(destin, "w")
svg.write(src)
svg.close()
