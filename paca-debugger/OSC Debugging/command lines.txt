To start the sendOSC program, use Terminal and navigate to the OSC
Debugging folder and enter:

    berkeley-libs/sendOSC/sendOSC -h 192.168.16.59 8000

Replace 192.168.16.59 with the IP address for your Paca(rana) that you
find in DSP Status > Configure > OSC

sendOSC infers the types of the message arguments from what is typed
on the line: numbers without decimal points are sent as integers,
numbers with decimal points are sent as floats.


======================================


To start the dumpOSC program, use Terminal and navigate to the OSC
Debugging folder and enter:

    berkeley-libs/dumpOSC/dumpOSC 8000

This is telling dumpOSC to listen on port 8000.
