These two command line programs illustrate simple OSC communication
between your computer and the Paca(rana).

It is not recommended as a template for developing OSC
applications. It is just provided as an aid for debugging.

Kyma OSC implementation details can be found in the Appendix of the
Kyma 7 User Guide and at

    http://www.symbolicsound.com/Learn/OpenSoundControlImplementation

Deleted from the Linux version:

    Example OSC Sound.kym
