#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Paca OSC client

Interface for manually sending OSC messages to the Paca OSC simulator
interactively and getting OSC responses.

Based on the simple_server.py that came with python2-osc. Thus far, in
addition to the basic standard OSC messages that Paca knows, this
handles our specific messages to the Virtual Control Surface (VCS):

  /vcs/Replay/1 <int>           # <int> 0=Stop, 1=Replay
  /vcs/Powered.../1 <int>       # ... is 1 through 8, <int> 0=Off, 1=On
  /vcs/dBGain.../1 <float>      # ... is 1 through 8, <float> 0.0 to 1.0
  /vcs/Soundscape.../1 <float>  # ... is 1 through 8, <float> 0.0 to 1.0

 * Replay and Powered{...} take an integer argument as described above.
 * dBGain{...} scales values -12 dB to +12 dB into the range 0.0 to 1.0
 * Soundscape{...} scales the number of provided samples into 0.0 to 1.0

Replay returns eight values, because it controls all eight speakers.
"""

#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.03.01
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

# Allow common Python 3 idioms in Python 2
#
from __future__ import print_function, absolute_import
from six.moves  import input    # use raw_input when I say input
from zeroconf   import Zeroconf             # Auto-IP discovery

import sys
import os
import readline                 # Command line history
import atexit                   # Interpreter exit handlers
import re                       # Regular Expression functions
import socket
import os.path
from time    import strftime, time, sleep

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca
import binascii   # Especially hexlify, for debugging
import threading  # Could it be this simple???

import logging

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages returned from Paca
from pythonosc import osc_server           # Listen for messages from Paca

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

__appname__    = "Sound Advice"
__module__     = "Paca OSC Interactive Tester"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2016, Kevin Cole (2016.03.01)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

SECONDS = 0.05  # 0.01 is not enough sleep time for Paca, but 0.05 works.

ESC = chr(27)                     # ASCII ESC character
CLEAR = ESC + "[2J" + ESC + "[H"  # ANSI clear screen <ESC>[2J<ESC>[H

HELP = ""
helpfile = open("paca_test.hlp", "r")
for text in helpfile:
    HELP += text

# Set up logging
#
wd = os.path.expanduser("~/Data/Sound Advice")
if not os.path.exists(wd):         # If there's no directory...
    os.makedirs(wd, 0o755)         # ...make it so
osclog = "{0}/osc.log".format(wd)  # Keep the log file in it
logging.basicConfig(filename=osclog, level=logging.DEBUG)

# Set up persistent command line history
#
histfile = os.path.join(os.path.expanduser("~"), ".osc_history")
if not os.path.isfile(histfile):      # If there's no history file...
    empty = open(histfile, "a")       # ... create an ALMOST empty one...
    empty.write("_HiStOrY_V2_\n")     # ... with the special header line
    empty.close()
readline.read_history_file(histfile)  # Read history from previous sessions
readline.set_history_length(1000)     # Default length was -1 (infinite)

atexit.register(readline.write_history_file, histfile)  # Save history on exit

jsons = []
soundscapes = []
client = None


def listify(obj):
    """Recurse thru dictionary splitting multi-line string values into lists"""
    for field in obj:
        if isinstance(obj[field], dict):
            listify(obj[field])
        elif isinstance(obj[field], stringTypes):
            if obj[field][-1] == u"\r":       # If string ends with newline...
                obj[field] = obj[field][:-1]  # ...remove it
            obj[field] = obj[field].split(u"\r")  # Paca uses Mac newline
            if len(obj[field]) == 1:        # If "list" is only one element...
                obj[field] = obj[field][0]  # ...unlist it
    return obj


def handle_json(address, args, stream):
    """Objectifies the JSON string returned by '/osc/widget #'."""
    print("{0}[31;1m{1}, {2}{0}[0m".format(ESC, address, args))
    logging.debug("{0} {1}".format(address, args))
#   print(stream.replace("\r", "\n"))
    nary = listify(json.loads(stream))  # It's always a dictionary, we hope.
    print("{0}[31;1m{1}{0}[0m".format(ESC, nary))
    logging.debug(nary)
    jsons.append(nary)
    if ("label" in nary) and ("Soundscape" in nary["label"]):
        soundscape = {"soundscape": int(nary["label"][-1]),
                      "id":         nary["concreteEventID"],
                      "samples":    nary["tickMarksOrNil"]
                                        ["rawLabelsString"]}
        feedback = open("feedback.txt", "a")
        feedback.write("{0} ({1}): {2}\n"
                       .format(soundscape["soundscape"],
                               soundscape["id"],
                               soundscape["samples"]))
        feedback.close()
        soundscapes.append(soundscape)


def fetch_widgets(address, args):
    """Request the info on all widgets"""
    print("{0}[31;1m{1}, {2}{0}[0m".format(ESC, address, args))
    logging.debug("{0} {1}".format(address, args))
    for widget in range(args):
        oscWidget = "/osc/widget"
        msg = osc_message_builder.OscMessageBuilder(address=oscWidget)
        msg.add_arg(widget)
        msg = msg.build()
        client.send(msg)
        sleep(SECONDS)


def handle_int(address, args):
    """Handles those rare events where Paca sends back an integer"""
    print("{0}[31;1m{1}, {2}{0}[0m".format(ESC, address, args))
    logging.debug("{0} {1}".format(address, args))


def handle_float(address, args):
    """Handle messages that return values between 0.0 and 1.0"""
    hexdump = binascii.hexlify(args)
    print("{0}[31;1m{1}, {2}{0}[0m".format(ESC, address, hexdump))
    logging.debug("{0} {1}".format(address, hexdump))
    print("{0}[31;1m  (Length: {1}){0}[0m".format(ESC, len(args)))
    logging.debug("  (Length: {0})".format(len(args)))
    blobs = [args[start:start + 8] for start in range(0, len(args), 8)]
    pairs = []
    for blob in blobs:
        widgetId =       struct.unpack("!i", blob[0:4])[0]
        value    = round(struct.unpack("!f", blob[4:8])[0], 3)
        pairs.append((widgetId, value))
    print("{0}[31;1m{1}{0}[0m".format(ESC, pairs))
    logging.debug("{0}".format(pairs))


def handle_echo(address, args):
    print("{0}[31;1m{1}, {2}{0}[0m".format(ESC, address, args))

# See http://stackoverflow.com/questions/9590965/
# Convert an IP string to a number and vice-versa
#


def main():
    """The main enchilada"""

    global client
    print(CLEAR)
    print("{0} Version {1}\n{2} ({3})"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __license__))
    print("Type '/help' for a summary of commands or '/quit' to exit\n")

    # Set up the source Paca. (Was "touch".)
    if len(sys.argv) == 2:
        host = sys.argv[1]
    else:
        host = "beslime-833"
    host = "{0}._osc._udp.local.".format(host)

    data = None
    while data is None:
        try:
            zeroconf = Zeroconf()
            data = zeroconf.get_service_info("_osc._udp.local.", host)
        except:
            zeroconf.close()
            del zeroconf

    if data:
        ip   = data.address          # IP address of Paca
        ip   = socket.inet_ntoa(ip)  # Convert to a string
        port = 8000                  # Paca standard OSC port
        # zeroconf.close()
        # del zeroconf                 # Why would I need this???
        client = udp_client.UDPClient(ip, port)
    else:
        client = udp_client.UDPClient("127.0.0.1", 8000)

    # Set up the server (listener)
    #
    director = dispatcher.Dispatcher()
    director.map("/osc/response_from", handle_int)
    director.map("/osc/widget", handle_json)
    director.map("/osc/notify/vcs/SoundAdvice", fetch_widgets)
    director.map("/vcs", handle_float)  # Handles all our directives
    director.map("/vcs/Replay/1", handle_echo)
    for speaker in range(1, 9):
        director.map("/vcs/Soundscape{0}/1".format(speaker), handle_echo)
        director.map("/vcs/Powered{0}/1".format(speaker),    handle_echo)
        director.map("/vcs/dBGain{0}/1".format(speaker),     handle_echo)

    server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8001), director)
#   server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8001), director)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    oscRespondTo = "/osc/respond_to"
    msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
    msg.add_arg(8001)  # Send feedback to port 8001
    msg = msg.build()
    client.send(msg)

    oscNotify = "/osc/notify/vcs/SoundAdvice"
    msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
    msg.add_arg(1)  # Turn notifications ON (0 for OFF)
    msg = msg.build()
    client.send(msg)

    while True:
        command = input("> ")
        if command == "/quit":
            server.shutdown()
            sys.exit()
        elif command == "/help":
            print(CLEAR)
            print(HELP)
        else:
            try:
                adr, args = command.split(" ")
                msg = osc_message_builder.OscMessageBuilder(address=adr)
                for arg in args.split(","):
                    item = float(arg) if "." in arg else int(arg)
                    msg.add_arg(item)
                msg = msg.build()
                client.send(msg)
            except ValueError:
                print("Unknown command or Syntax error. Syntax is:\n")
                print("  /command[/modifier][/modifier]... arg[,arg,...,arg]\n")
                print("Type '/help' for a summary of commands or '/quit' to exit\n")


if __name__ == "__main__":
    main()
