#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Paca OSC client
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.03.01
#
#  Signal to Noise: Adjust signal level (from a single source), and
#  adjust background noise level (coming from multiple sources)
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input           # use raw_input when I say input
import readline                        # Yes! Command history!
import sys
import os
import re                       # Regular Expression functions
from os.path import expanduser  # Cross-platform home directory finder
from os.path import join

import json
import struct
import binascii  # Especially hexlify

from time import strftime, time

import socket
from zeroconf  import Zeroconf             # Auto-IP discovery
from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Open Sound Control messenger

__appname__    = "Sound Advice"
__module__     = "Paca OSC Client"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2016, Kevin Cole (2016.03.01)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# See http://stackoverflow.com/questions/9590965/
# Convert an IP string to a number and vice-versa
#

zeroconf = Zeroconf()
data = zeroconf.get_service_info("_osc._udp.local.",
                                 "beslime-833._osc._udp.local.")
ip   = data.address          # IP address of Paca
ip   = socket.inet_ntoa(ip)  # Convert to a string
port = 8000                  # Paca standard OSC port
zeroconf.close()
client = udp_client.UDPClient(ip, port)
print("DEBUG: IP address: {0}  Port: {1}".format(ip, port))

oscRespondTo = "/osc/respond_to"
msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
msg.add_arg(8001)  # Send feedback to port 8001
msg = msg.build()
client.send(msg)

oscNotify = "/osc/notify/vcs/SoundAdvice"
msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
msg.add_arg(1)  # Turn notifications ON (0 for OFF)
msg = msg.build()
client.send(msg)

while True:
    message = input("> ")
    if message == "/quit": break
    cmd, arg = message.split(" ")
    arg = float(arg) if "." in arg else int(arg)
    msg = osc_message_builder.OscMessageBuilder(address=cmd)
    msg.add_arg(arg)
    msg = msg.build()
    client.send(msg)
