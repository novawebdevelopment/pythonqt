#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Paca OSC Simulator

This sends semi-realistic Paca OSC responses to OSC requests, so
that the main routines will no longer be dependent on an actual Paca
for testing.
"""

#  See https://docs.python.org/3.3/tutorial/classes.html
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.05.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input    # use raw_input when I say input
from itertools  import tee, zip_longest

import sys
import re                       # Regular Expression functions

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca
import binascii   # Especially hexlify, for debugging
import threading  # Could it be this simple???

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages returned from Paca
from pythonosc import osc_server           # Listen for messages from Paca

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

__appname__    = "Sound Advice"
__module__     = "Paca OSC Simulator"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2016, Kevin Cole (2016.05.13)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"


ESC = chr(27)

jsons = []
soundscapes = []


# See https://docs.python.org/3/library/itertools.html#itertools-recipes
#
def grouper(iterable, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * 2
    return zip_longest(*args, fillvalue=fillvalue)


class Widget(object):
    """A Kyma Virtual Control Surface (VCS) control element"""
    event_id = 0x300000  # Event IDs offset

    def __init__(self, label, *samples):
        Widget.event_id += 1
        self.label = label
        self.event_id = Widget.event_id
        if samples:
            self.samples = samples[0]
        else:
            self.samples = None

    def get_label(self):
        """Gets label of widget"""
        return self.label

    def set_label(self, label):
        """Sets label of widget"""
        self.label = label

    def get_event_id(self):
        """Gets event ID of widget"""
        return self.event_id

    def set_event_id(self, event_id):
        """Sets event ID of widget"""
        self.event_id = event_id

    def get_samples(self):
        """Gets samples of widget"""
        return self.samples

    def set_samples(self, samples):
        """Sets samples of widget"""
        self.samples = samples


class Paca(object):
    """Symbolic Sound Paca Emulator"""

    def __init__(self):
        self.osc = udp_client.UDPClient("127.0.0.1", 8001)

        self.response_port = None
        self.notification  = False
        self.widgets = []

        # Set up the server (listener)
        self.director = dispatcher.Dispatcher()
        self.director.map("/osc/respond_to", self.handle_respond_to)
        self.director.map("/osc/notify/vcs/SoundAdvice", self.count_widgets)
        self.director.map("/osc/widget", self.handle_widget)
        self.director.map("/vcs", self.handle_vcs)
        self.director.set_default_handler(self.wtf)

        #self.server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8000),
        #                                               self.director)
        self.server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8000),
                                                     self.director)
        self.server_thread = threading.Thread(target=self.server.serve_forever)

    def add_widget(self, label, *samples):
        """Add a widget to the Paca VCS"""
        if samples:
            self.widgets.append(Widget(label, *samples))
        else:
            self.widgets.append(Widget(label))

    def get_by_id(self, event_id):
        """Retrieve a widget based on its event ID"""
        for widget in self.widgets:
            if widget.event_id == event_id:
                return widget

    def handle_respond_to(self, addr, args):
        """Handles those rare events where Paca sends back an integer"""
        print("DEBUG: handle_response_to responding to {0}, {1}"
              .format(addr, args))
        osc_response_from = "/osc/response_from"
        msg = osc_message_builder.OscMessageBuilder(address=osc_response_from)
        msg.add_arg(34789)
        msg = msg.build()
        self.osc.send(msg)
        print("{0}[31;1m{1} {2}{0}[0m".format(ESC, addr, args))

    def handle_widget(self, addr, args):
        """Return JSON representation of widget"""
        print("DEBUG: handle_widget responding to {0}, {1}"
              .format(addr, args))
        if args in range(len(self.widgets)):
            nary = {"concreteEventID": self.widgets[args].event_id,
                    "label":           self.widgets[args].label}
            if self.widgets[args].samples:
                label_string = ""
                for sample in self.widgets[args].samples:
                    label_string += "{0}\r".format(sample)
                nary["tickMarksOrNil"] = {"rawLabelsString": label_string}
        else:
            nary = {"ERROR":
                    "No such widget. Index must be between 0 and {0}"
                    .format(len(self.widgets) - 1)}
        print("{0}[35;1maddr: {1}{0}[0m".format(ESC, addr))
        print("{0}[35;1margs: {1}{0}[0m".format(ESC, args))
        print("{0}[35;1mnary: {1}{0}[0m".format(ESC, json.dumps(nary)))
        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(args)
        msg.add_arg(json.dumps(nary))
        msg = msg.build()
        self.osc.send(msg)

    def count_widgets(self, addr, args):
        print("DEBUG: count_widgets responding to {0}, {1}"
              .format(addr, args))
        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(len(self.widgets))
        msg = msg.build()
        self.osc.send(msg)

    def handle_vcs(self, addr, *args):
        print("DEBUG: handle_vcs responding to {0}, {1}"
              .format(addr, args))
        fmt = "{0}[31;1m{1}"
        for i in range(len(args)):
            fmt += " {{{0}}}".format(i + 2)
        fmt += "{0}[0m"
        print(fmt.format(ESC, addr, *args))
#       msg = osc_message_builder.OscMessageBuilder(address=addr)
        for addr, arg in grouper(args):
            print("    {0}  {1}".format(addr, arg))
        for addr, arg in grouper(args):
            widget = self.get_by_id(addr)
            command = "/vcs/{0}/1".format(widget.label)
            msg = osc_message_builder.OscMessageBuilder(address=command)
            msg.add_arg(arg)
            msg = msg.build()
            self.osc.send(msg)

#    def wtf(self, addr, *args):
#        """WTF???"""
#        print("DEBUG: wtf responding to {0}, {1}"
#              .format(addr, args))
#        print("{0}[35;1margs: {1} {2}{0}[0m".format(ESC, addr, *args))
#        print("{0}[35;1mlength: {1}{0}[0m".format(ESC, len(args)))
#        for arg in args:
#            print("{0}[35;1marg: {1}{0}[0m".format(ESC, arg))
#        command = "/wtf"
#        msg = osc_message_builder.OscMessageBuilder(address=command)
#        for arg in args:
#            msg.add_arg(arg)
#        msg.add_arg("{'wtf':'What the fuck???'}")
#        msg = msg.build()
#        self.osc.send(msg)

    def wtf(self, *args, **kwargs):
        """WTF???"""
        print("DEBUG: wtf responding")
        if args is not None:
            for x, arg in enumerate(args):
                print("arg {0}: {1}".format(x, arg))
        else:
            print("No regular args")
        if kwargs is not None:
            for key, value in kwargs.items():
                print("arg {0}: {1}".format(key, value))
        else:
            print("No keyword args")


def main():
    """Compile, load and start the "Sound" in Paca"""

    signals = ("Stage_Dancing_6",     "Video_Game_Tutorial",  "Pandoras_Box",
               "mantalkingaboutlife", "mantalkingaboutlife2", "Being_a_Man")
    noises  = ("restaurant1_s",       "restaurant2_s",        "restaurant3_s",
               "restaurant4_s",       "restaurant5_s",        "restaurant6_s")

    paca = Paca()

    paca.add_widget("Replay")

    for speaker in range(1, 9):
        if speaker == 1:
            paca.add_widget("Soundscape{0}".format(speaker), signals)
        else:
            paca.add_widget("Soundscape{0}".format(speaker), noises)

    for speaker in range(1, 9):
        paca.add_widget("Powered{0}".format(speaker))

    for speaker in range(1, 9):
        paca.add_widget("dBGain{0}".format(speaker))

    paca.server_thread.start()


#   oscRespondTo = "/osc/respond_to"
#   msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
#   msg.add_arg(####)  # Send feedback to port ####
#   msg = msg.build()
#   paca.osc.send(msg)

#   oscNotify = "/osc/notify/vcs/SoundAdvice"
#   msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
#   msg.add_arg(1)  # Turn notifications ON (0 for OFF)
#   msg = msg.build()
#   paca.osc.send(msg)

#   arg = float(arg) if "." in arg else int(arg)
#   msg = osc_message_builder.OscMessageBuilder(address=adr)
#   msg.add_arg(arg)
#   msg = msg.build()
#   paca.osc.send(msg)


if __name__ == "__main__":
    main()
