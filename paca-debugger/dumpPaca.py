#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Dump Paca
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.03.07
#
#  Saves Paca's guts to a JSON file for the emulator
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input           # use raw_input when I say input
import sys
import readline
import os
import re                       # Regular Expression functions
from os.path import expanduser  # Cross-platform home directory finder
from os.path import join
from time    import strftime, time, sleep

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca
import binascii   # Especially hexlify, for debugging
import threading  # Could it be this simple???

import socket
from zeroconf  import Zeroconf             # Auto-IP discovery
from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages returned from Paca
from pythonosc import osc_server           # Listen for messages from Paca

__appname__    = "Sound Advice"
__module__     = "Dump Paca"
__author__     = "Kevin Cole"
__copyright__  = "Copyright 2017, Kevin Cole (2017.03.07)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

global server
naries = {}
results = None

SECONDS = 0.05  # 0.01 is not enough sleep time for Paca, but 0.05 works.

# Set up the client (sender)
zeroconf = Zeroconf()
data = zeroconf.get_service_info("_osc._udp.local.",
                                 "beslime-833._osc._udp.local.")
ip   = data.address          # IP address of Paca
ip   = socket.inet_ntoa(ip)  # Convert to a string
port = 8000                  # Paca standard OSC port
zeroconf.close()
client = udp_client.UDPClient(ip, port)
print("DEBUG: IP address: {0}  Port: {1}".format(ip, port))


def listify(obj):
    """Recurse thru dictionary splitting multi-line string values into lists"""
    for field in obj:
        if isinstance(obj[field], dict):
            listify(obj[field])
        elif isinstance(obj[field], stringTypes):
            if obj[field][-1] == u"\r":       # If string ends with newline...
                obj[field] = obj[field][:-1]  # ...remove it
            obj[field] = obj[field].split(u"\r")  # Paca uses Mac newline
            if len(obj[field]) == 1:        # If "list" is only one element...
                obj[field] = obj[field][0]  # ...unlist it
    return obj


def handle_json(address, args, stream):
    """Objectifies the JSON string returned by '/osc/widget #'."""
    # print(address, args)
    nary = listify(json.loads(stream))  # It's always a dictionary, we hope.
    if args not in naries:
        naries[args] = nary
    print("\nargs = {0}, len(naries) = {1}".format(args, len(naries)))
    sleep(SECONDS)
    # print(args, naries)


def fetch_widgets(address, args):
    """Request the info on all widgets"""
    global results, server
    print(address, args)
    while len(naries) != args:
        input("\n{0} of {1} naries found.\n".format(len(naries), args))
        for widget in range(args):
            oscWidget = "/osc/widget"
            msg = osc_message_builder.OscMessageBuilder(address=oscWidget)
            msg.add_arg(widget)  # Send feedback to port 8000
            msg = msg.build()
            client.send(msg)
            sleep(SECONDS)
    kees = list(naries.keys())
    kees.sort()
    final = []
    for kee in kees:
        final.append(naries[kee])
    output = json.dumps(final, sort_keys=True,
                        indent=4, separators=(',', ': '))
    results.write(output)
    results.close()
    server.shutdown()
#   sys.exit()


def handle_int(address, args):
    """Handles those rare events where Paca sends back an integer"""
    # print(address, args)


def handle_float(address, args):
    """Handle messages that return values between 0.0 and 1.0"""
    hexdump = binascii.hexlify(args)
    # print(address, hexdump)
    # print("  (Length: {0})".format(len(args)))
    blobs = [args[start:start + 8] for start in range(0, len(args), 8)]
    pairs = []
    for blob in blobs:
        widgetId =       struct.unpack("!i", blob[0:4])[0]
        value    = round(struct.unpack("!f", blob[4:8])[0], 3)
        pairs.append((widgetId, value))
    # print(pairs)


# See http://stackoverflow.com/questions/9590965/
# Convert an IP string to a number and vice-versa
#

def main():
    global results, server

    save_as = input("Save as... [*.json]: ")
    wd = os.path.expanduser("~/.local/share/paca")
    if not os.path.exists(wd):
        os.makedirs(wd, 0o755)
    full_path = "{0}/{1}.json".format(wd, save_as)
    results = open(full_path, "w")

    # Set up the server (listener)
    director = dispatcher.Dispatcher()
    director.map("/osc/response_from", handle_int)
    director.map("/osc/widget", handle_json)
    director.map("/osc/notify/vcs/SoundAdvice", fetch_widgets)
    director.map("/vcs", handle_float)  # Handles all our directives

    server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8000), director)
#   server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8000), director)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    oscRespondTo = "/osc/respond_to"
    msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
    msg.add_arg(8000)  # Send feedback to port 8000
    msg = msg.build()
    client.send(msg)

    oscNotify = "/osc/notify/vcs/SoundAdvice"
    msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
    msg.add_arg(1)  # Turn notifications ON (0 for OFF)
    msg = msg.build()
    client.send(msg)


if __name__ == "__main__":
    main()
