#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Based on the simple_server.py that came with python2-osc. Thus far,
# in addition to the basic standard OSC messages that Paca knows, this
# handles our specific messages to the Virtual Control Surface (VCS):
#
#   /vcs/Replay/1         # Returns 8 pairs (because it controls 8 widgets)!
#   /vcs/Powered.../1     # ... is 1 through 8
#   /vcs/dBGain.../1      # ... is 1 through 8
#   /vcs/Soundscape.../1  # ... is 1 through 8
#

from __future__ import print_function, absolute_import

import sys

import json
import struct
import binascii  # Especially hexlify
import os
import logging

from pythonosc import dispatcher
from pythonosc import osc_server

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

wd = os.path.expanduser("~/Data/Sound Advice")
if not os.path.exists(wd):
    os.makedirs(wd, 0o755)
osclog = "{0}/osc.log".format(wd)
logging.basicConfig(filename=osclog, level=logging.DEBUG)


def listify(obj):
    """Recurse thru dictionary splitting multi-line string values into lists"""
    for field in obj:
        if isinstance(obj[field], dict):
            listify(obj[field])
        elif isinstance(obj[field], stringTypes):
            if obj[field][-1] == u"\r":       # If string ends with newline...
                obj[field] = obj[field][:-1]  # ...remove it
            obj[field] = obj[field].split(u"\r")  # Paca uses Mac newline
            if len(obj[field]) == 1:        # If "list" is only one element...
                obj[field] = obj[field][0]  # ...unlist it
    return obj


def handle_json(address, args, stream):
    """Objectifies the JSON string returned by '/osc/widget #'."""
    print(address, args)
    logging.debug("{0} {1}".format(address, args))
#   print(stream.replace("\r", "\n"))
    nary = listify(json.loads(stream))  # It's always a dictionary, we hope.
    print(nary)
    logging.debug(nary)


def handle_int(address, args):
    """Handles those rare events where Paca sends back an integer"""
    print(address, args)
    logging.debug("{0} {1}".format(address, args))


def handle_float(address, args):
    """Handle messages that return values between 0.0 and 1.0"""
    hexdump = binascii.hexlify(args)
    print(address, hexdump)
    logging.debug("{0} {1}".format(address, hexdump))
    print("  (Length: {0})".format(len(args)))
    logging.debug("  (Length: {0})".format(len(args)))
    blobs = [args[start:start + 8] for start in range(0, len(args), 8)]
    pairs = []
    for blob in blobs:
        widgetId =       struct.unpack("!i", blob[0:4])[0]
        value    = round(struct.unpack("!f", blob[4:8])[0], 3)
        pairs.append((widgetId, value))
    print(pairs)
    logging.debug("{0}".format(pairs))


def main():
    director = dispatcher.Dispatcher()
    director.map("/osc/response_from", handle_int)
    director.map("/osc/widget", handle_json)
    director.map("/osc/notify/vcs/SoundAdvice", handle_int)
    director.map("/vcs", handle_float)  # Handles all our directives

    server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8001),
                                              director)
    server.serve_forever()


if __name__ == "__main__":
    main()
