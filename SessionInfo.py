#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.06.19
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
# Solution to button focus issue provided by:
#
#     https://stackoverflow.com/a/45568125/447830
#

from   __future__ import print_function, absolute_import
from   six.moves  import input           # use raw_input when I say input

#import ipdb  # iPython debugger (relative of pdb)

import sys
import os.path
import socket
import json
from   os          import makedirs
from   time        import strftime, time, sleep

from pythonosc     import udp_client           # Open Sound Control UDP

from PySide.QtCore import *
from PySide.QtGui  import *

#from common import provideIP    # Manual connect Paca dialog
#from common.functions import *  # Connect to Paca, rescaling, etc.
#from common.control   import *  # VCS control widget class

from UI import SessionInfoUI
from ProvideIP import *
from common.functions import *  # Connect to Paca, rescaling, etc.

__appname__    = "Sound Advice"
__module__     = "Session Info Dialog Window"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017, Kevin Cole (2017.06.19)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Aaarg! Token placeholders!
results = None


class SessionInfo(QWidget, SessionInfoUI.UI):
    """Researcher's subject data collection dialog"""

    def __init__(self, unit, main_ui, width=None, height=None, center=None,
                 feet=17, inches=0, parent=None):
        """Display subject data collection dialog window"""

        super(SessionInfo, self).__init__(parent)

        self.main_ui = main_ui
        self.width   = width
        self.height  = height
        self.center  = center
        self.unit    = unit
        self.move(self.center - self.rect().center())

        wd = os.path.expanduser("~/Data/Sound Advice/{0}".format(self.unit))
        if not os.path.exists(wd):
            makedirs(wd, 0o755)
        now = strftime("%Y-%m-%d %H:%M")
        self.filename = "{0}/Results {1}.csv".format(wd, now)
        results = open(self.filename, "w")
        results.write(",,{0} Exploration ({1})\n".format(self.unit, now))
        results.write(",,Start time:,{0}\n".format(now))
        results.close()

        self.setupUi(self, feet, inches)  # Ask for unit-wide settings

        self.subjectId.selectAll()    # Selects the default text
        self.subjectId.setFocus()     # Focuses the cursor in the box

        self.presetsButton.clicked.connect(self.choosePresets)
        self.ratingsButton.clicked.connect(self.chooseRatings)
        self.instructionsButton.clicked.connect(self.chooseInstructions)
        self.saveButton.clicked.connect(self.launch)

    def choosePresets(self):
        """Choose presets JSON file"""

        wd = os.path.expanduser("~/.config/sound-advice/presets")
        self.presetsPath = QFileDialog.getExistingDirectory(caption="Choose presets...",
                                                            dir=wd)
        self.presetsButton.setText(self.presetsPath.split("/")[-1])

    def chooseRatings(self):
        """Choose ratings JSON file"""

        wd = os.path.expanduser("~/.config/sound-advice/ratings")
        self.ratingsPath = QFileDialog.getExistingDirectory(caption="Choose ratings...",
                                                            dir=wd)
        self.ratingsButton.setText(self.ratingsPath.split("/")[-1])

    def chooseInstructions(self):
        wd = os.path.expanduser("~/.config/sound-advice/instructions/{0}"
                                .format(self.unit))
        self.instructionsPath = QFileDialog.getExistingDirectory(caption="Choose instructions...",
                                                                 dir=wd)
        self.instructionsButton.setText(self.instructionsPath.split("/")[-1])

    def launch(self):
        """Initialize conditions and launch main experiment UI"""

        self.hide()

        subjectId      = self.subjectId.text()
        kymaSound      = self.kymaSound.text()
        programButtons = self.programButtons.value()

        configuration = unjson(self.presetsPath)       # Presets for each trial
        presets = configuration["presets"]
        configuration = unjson(self.ratingsPath)       # Ratings for entire session
        ratings = configuration["ratings"]
        configuration = unjson(self.instructionsPath)  # Instructions for each trial
        instructions = configuration["instructions"]

        results = open(self.filename, "a")
        results.write(",,Subject: {0}\n"
                      .format(subjectId))
        results.write(",,Kyma sound: {0}\n"
                      .format(kymaSound))
        results.write(",,Number of HA/CI programs: {0}\n"
                      .format(programButtons))
        results.write(",,Presets: {0}\n"
                      .format(self.presetsPath.split("/")[-1]))
        results.write(",,Ratings: {0}\n"
                      .format(self.ratingsPath.split("/")[-1]))
        results.write("\n")

        headings = ["Trial", "Time", "Details",
                    "Front Center (1)", "Front Left (2)",
                    "Center Left (3)",  "Rear Left (4)",
                    "Rear Center (5)",  "Rear Right (6)",
                    "Center Right (7)", "Front Right (8)",
                    "Signal", "Noise"]
        for rating in ratings:
            headings.append(rating["name"])  # Rating column heading
        for heading in headings:
            results.write("\"{0}\",".format(heading))
        results.write("\n")
        results.close()

        # See http://stackoverflow.com/questions/9590965/
        # Convert an IP string to a number and vice-versa
        #

        port = 8000  # Paca standard OSC port
        try:
            from zeroconf import Zeroconf  # Auto-IP discovery
            zeroconf = Zeroconf()

            if len(sys.argv) > 1:
                host = sys.argv[1]   # e.g. "touch" for the Pi touchscreen
            else:
                host = "beslime-833"  # Default for our Paca
            data = zeroconf.get_service_info("_osc._udp.local.",
                                             "{0}._osc._udp.local."
                                             .format(host))
            ip = data.address
            ip = socket.inet_ntoa(ip)  # Convert to a string
            print("DEBUG: IP address: {0}".format(ip))
#           logging.debug("IP address: {0}".format(ip))

            zeroconf.close()
        except:
            print("Exception {0}".format(sys.exc_info()[0]))
            getIP = ProvideIP()  # Manually get the IP address
            if getIP.exec_():
                ip = "{0}.{1}.{2}.{3}".format(getIP.octet_1.value(),
                                              getIP.octet_2.value(),
                                              getIP.octet_3.value(),
                                              getIP.octet_4.value())

        osc = udp_client.UDPClient(ip, port)

        meatRadius  = self.meatFeet.value() * 12
        meatRadius += self.meatInches.value()
        meatRadius /= 2
        initValues = {"subjectInfo":    subjectId,
                      "kymaSound":      kymaSound,
                      "programButtons": programButtons,
                      "meatRadius":     meatRadius,
                      "presets":        presets,
                      "ratings":        ratings,
                      "instructions":   instructions,
                      "configuration":  configuration,
                      "osc":            osc,
                      "width":          self.width,
                      "height":         self.height,
                      "center":         self.center,
                      "filename":       self.filename}

        vcs = self.main_ui(initValues)
