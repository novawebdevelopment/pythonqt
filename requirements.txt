# Written by Kevin Cole <kevin.cole@novawebcoop.org> 2017.01.13 (kjc)
# Last modified by Kevin Cole <kevin.cole@novawebcoop.org> 2018.05.24 (kjc)
#
# pip requirements file for Sound Advice in a Python2.7 virtual environment
#
# See: https://pip.readthedocs.io/en/1.1/requirements.html
# See "pip freeze" when modifying this code.
#
# KEEP PySide LOCKED at 1.2.2 until further notice!
# KEEP zeroconf LOCKED at 0.19.1! Later versions dropped support for Python 2
#
# 2017.09.25 KJC - PyInstaller's dependency on a non-builtin future
#                  module breaks EVERYTHING!
# 2018.05.24 KJC - Locked zeroconf to pre 0.20.0

six >= 1.10.0
PySide == 1.2.2
zeroconf == 0.19.1
git+https://github.com/kjcole/python2-osc.git#egg=python2_osc
py2app >= 0.10
#pyinstaller >= 3.3   # DO NOT ENABLE !!!

# I don't know if we'll need PyOpenGL yet...
# PyOpenGL >= 3.1.0
