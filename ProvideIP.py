#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtGui  import *
from common import provideIP    # Manual connect Paca dialog

class ProvideIP(QDialog, provideIP.Ui_provideIP):
    """Dialog for manual IP address entry"""

    def __init__(self, parent=None):
        """Provide a dialog window with four octets to fill"""
        super(ProvideIP, self).__init__(parent)  # Old way. Learn new way.
        self.setupUi(self)

        self.connectButton.clicked.connect(self.accept)
