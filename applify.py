#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
#  applify.py
#
#  Copyright 2015 Kevin Cole <kevin.cole@novawebcoop.org> 2015.12.02
#
#  This turns the Sound Advice files into Mac OS X apps.
#  Built from several setup.py files, plus the py2app instructions
#  at https://pythonhosted.org/py2app/ plus setup.cfg plus info
#  about Mac OS X applications Info.plist files.
#
#  Previously, this was done with 8 separate stanzas:
#
#     $ rm setup.py
#     $ py2applet --make-setup $1 --iconfile icons.icns --resources UI
#     $ python2.7 setup.py py2app -A
#
#  Now run with a single:
#
#    $ python2.7 applify.py py2app -A
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Build Mac OS X apps from Sound Advice Python sources"""

from plistlib   import Plist
from setuptools import setup

__appname__    = u"Sound Advice"
__module__     = u"Applify"
__author__     = u"Kevin Cole"
__copyright__  = u"Copyright \N{copyright sign} 2017, Kevin Cole (2017.01.25)"
__agency__     = u"NOVA Web Development, LLC"
__credits__    = [u"Kevin Cole"]  # Authors and bug reporters
__license__    = u"uGPL"
__version__    = u"1.0"
__maintainer__ = u"Kevin Cole"
__email__      = u"kevin.cole@novawebcoop.org"
__status__     = u"Development"  # "Prototype", "Development" or "Production"

COPYLEFT = u"Copyleft (c) 2017, Kevin Cole, GPL"


def main():
    """The main routine"""

    plist = Plist.fromFile('Info.plist')

    # Signal to Noise Ratio exercise with 8 independent signals and noises

    plist.update(dict(NSHighResolutionCapable=True,
                      CFBundleDisplayName="SNR",
                      CFBundleExecutable="SNR",
                      CFBundleName="SNR",
                      CFBundleIdentifier="edu.gallaudet.hsls.SNR",
                      NSHumanReadableCopyright=COPYLEFT,
                      CFBundleGetInfoString="Signal to Noise exercise",))

    setup(
        app=["SNR.py"],
        options=dict(py2app=dict(
            plist=plist,
            argv_emulation=False,
            iconfile="icon.icns",
            packages=["common",
                      "UI"],
            resources="images",)),
    )

    # Localization exercise with 8 independent signals and noises

    plist.update(dict(NSHighResolutionCapable=True,
                      CFBundleDisplayName="Localization",
                      CFBundleExecutable="Localization",
                      CFBundleName="Localization",
                      CFBundleIdentifier="edu.gallaudet.hsls.Localization",
                      NSHumanReadableCopyright=COPYLEFT,
                      CFBundleGetInfoString="Localization exercise",))

    setup(
        app=["Localization.py"],
        options=dict(py2app=dict(
            plist=plist,
            argv_emulation=False,
            iconfile="icon.icns",
            packages=["common",
                      "UI"],
            resources="images",)),
    )

    # Distance exercise with 8 independent signals and noises

    plist.update(dict(NSHighResolutionCapable=True,
                      CFBundleDisplayName="Distance",
                      CFBundleExecutable="Distance",
                      CFBundleName="Distance",
                      CFBundleIdentifier="edu.gallaudet.hsls.Distance",
                      NSHumanReadableCopyright=COPYLEFT,
                      CFBundleGetInfoString="Distance exercise",))

    setup(
        app=["Distance.py"],
        options=dict(py2app=dict(
            plist=plist,
            argv_emulation=False,
            iconfile="icon.icns",
            packages=["common",
                      "UI"],
            resources="images",)),
    )

    # Optimization exercise with 8 independent signals and noises

    plist.update(dict(NSHighResolutionCapable=True,
                      CFBundleDisplayName="Optimization",
                      CFBundleExecutable="Optimization",
                      CFBundleName="Optimization",
                      CFBundleIdentifier="edu.gallaudet.hsls.Optimization",
                      NSHumanReadableCopyright=COPYLEFT,
                      CFBundleGetInfoString="Optimization exercise",))

    setup(
        app=["Optimization.py"],
        options=dict(py2app=dict(
            plist=plist,
            argv_emulation=False,
            iconfile="icon.icns",
            packages=["common",
                      "UI"],
            resources="images",)),
    )

    # Edit instructions

    plist.update(dict(NSHighResolutionCapable=True,
                      CFBundleDisplayName="Instructions",
                      CFBundleExecutable="Instructions",
                      CFBundleName="Instructions",
                      CFBundleIdentifier="edu.gallaudet.hsls.Instructions",
                      NSHumanReadableCopyright=COPYLEFT,
                      CFBundleGetInfoString="Edit Instructions",))

    setup(
        app=["instructions.py"],
        options=dict(py2app=dict(
            plist=plist,
            argv_emulation=False,
            iconfile="icon.icns",
            packages=["common",
                      "UI"],
            resources="images",)),
    )


if __name__ == "__main__":
    main()
