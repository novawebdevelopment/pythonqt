#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  environment.py
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.04.21
#
#  Establish, as a set of classes, an environment that maintains both
#  the initial state of, and current state of everything -- trial,
#  samples, signal sources, levels, mutings, signal-to-noise ratios,
#  visual icons, locations in ... meat space and scale down to screen?
#  or screen space and scale up to meat space? ... of both sound
#  sources, and the listener. And, quite likely, additional physical
#  parameters.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#


from __future__ import print_function
from six.moves  import input           # use raw_input when I say input
from os.path    import expanduser      # Cross-platform home directory finder
import os
import sys

from math import sqrt, radians, degrees, sin, cos

__appname__    = "Sound Advice"
__module__     = "environment"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, NOVA Web Development, LLC"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


#   Signal {#}: Multisample           [List of signal samples]
#    - Index: !Signal{#}              [Current signal sample]
#    - Gate:  !Replay * !Powered{#}   [Powered: 0 = muted, 1 = unmuted]
#
#   Noise {#}: Multisample            [List of noise samples]
#    - Index: !Noise{#}               [Current noise sample]
#    - Gate:  !Replay * !Powered{#}   [Powered: 0 = muted, 1 = unmuted]
#
#   Source {#}: Crossfader            [Signal to Noise Ratio]
#    - Fade: !snr{#}                  [snr: 0 = all signal, 1 = all noise]
#    - Pan: 0                         [Force everything to LEFT speaker]
#    - Type: Linear                   [Note: Powered may be a better choice]
#
#   Speaker {#}: Level                [Between -12 dB and 12 dB in 3 dB steps]
#    - Left: (!dBGain{#} vmin: 12) dB smoothed
#    - Interpolation: Linear

class UnknownSpace(Exception):
    """Captain, we've left known space!"""
    def __init__(self, universe, traveler, location):
        x1 = 0 - (universe.x / 2)
        y1 = 0 - (universe.y / 2)
        x2 = 0 + (universe.x / 2)
        y2 = 0 + (universe.y / 2)
        message  = "{4} outside known universe\n"
        message += "  Universe coordinates: ({0}, {1}) to ({2}, {3})\n"
        message += "  {4} at ({5}, {6})"
        message = message.format(x1, y1, x2, y2,
                                 traveler,
                                 location[0], location[1])

        super(UnknownSpace, self).__init__(message)


class Source(object):
    """Sound source"""

    def __init__(self, environment,
                 sample=1, name="", image=None, X=0, Y=0,
                 preset=0, level=0, on=True, paused=False,
                 signal=False, weight=1):
        """Initialize sound source level, location, priority"""

        # pylint: disable=too-many-instance-attributes

        self.environment = environment
        self.sample      = sample  # index of sample to be played
        self.name        = name    # Sample name
        self.image       = image   # visual representation
        self.on          = on      # Unmuted / Powered
        self.paused      = paused  # Initially not paused
        self.preset      = preset  # p/reset level
        self.signal      = signal  # noise
        self.level       = level   # current level (dB)
        self.weight      = weight  # weighted importance
        if X > self.environment.x or Y > self.environment.y:
            raise UnknownSpace(self.environment, "Source", (X, Y))
        self.x           = X
        self.y           = Y
        self.position    = [X, Y]  # from center (origin)
        self.distance    = sqrt(X**2 + Y**2)  # from center (origin) inches
        self.direction   = None    # from center (origin) in degrees

    def setSample(self, sample=1):
        """Set sample to be played by source"""
        self.sample = sample

    def setName(self, name=""):
        self.name = name

    def setImage(self, image=None):
        """Set source's visual representation"""
        self.image = image

    def setOn(self, on=True):
        """Set source to muted or unmuted"""
        self.on = on

    def mute(self):
        self.setOn(False)
        self.muted = True

    def unmute(self):
        self.setOn(True)
        self.muted = False

    def setPause(self, paused=False):
        self.paused = paused

    def pause(self):
        setPause(True)

    def resume(self):
        setPause(False)

    def setLevel(self, level=0):
        """Set source output level"""
        self.level = level

    def setSignal(self, signal):
        """Designate source as either a signal or noise"""
        self.signal = signal

    def asSignal(self):
        self.setSignal(True)

    def asNoise(self):
        self.setSignal(False)

    def setPosition(self, X=0, Y=0):
        """Set source's position from center origin"""
        if X > self.environment.x or Y > self.environment.y:
            raise UnknownSpace(self.environment, "Source", (X, Y))
        self.position = [X, Y]
        self.x        = X
        self.y        = Y
        self.distance = sqrt(X**2 + Y**2)

    def setDistance(self, distance=0):
        """Set source's distance from avatar"""
        self.distance = distance

    def setDirection(self, direction=0):
        """Set source's direction from avatar"""
        self.direction = direction

    def setWeight(self, weight):
        """Set source's relative importance as a signal"""
        self.weight = weight


class Listener(object):
    """Person listening to sources"""

    def __init__(self, X, Y, environment):
        """Create a listener with a position and an image"""
        self.environment = environment
        if X > self.environment.x or Y > self.environment.y:
            raise UnknownSpace(self.environment, "Listener", (X, Y))
        self.position = [X, Y]
        self.x        = X
        self.y        = Y
        self.image    = None
        self.distance = []
        for source in range(7):
            self.distance.append(0)

    def setPosition(self, X, Y):
        """Set listener's position"""
        if X > self.environment.x or Y > self.environment.y:
            raise UnknownSpace(self.environment, "Listener", (X, Y))
        self.position = [X, Y]
        self.x = X
        self.y = Y

    def setDistance(self, source, distance):
        """Set listener's distance from a sound source"""
        self.distance[source] = distance

    def setImage(self, image):
        """Set listener's visual representation"""
        self.image = image


class Environment(object):
    """Audio environment of the listener"""

    def __init__(self, X, Y, sources=8):
        """Establish an environment with sound sources and a listener"""
        names = [["", ""],
                 ["", ""],
                 ["", ""]]

        self.background = None  # background image (or color)
        self.x = X
        self.y = Y
        self.dimensions = [X, Y]
        self.listener = Listener(0, 0, self)
        self.source = []
        self.radius = min(X, Y) / 2
        arc = 360.0 / sources  # Divvy the circle into source arcs
        for source in range(sources):
            x = cos(radians(source * arc)) * self.radius
            y = sin(radians(source * arc)) * self.radius
            self.source.append = Source(self,
                                        sample=1,
                                        name=names[source][0],
                                        image=None,
                                        X=x, Y=y,
                                        preset=0, level=0,
                                        on=True, paused=False,
                                        signal=False, weight=1)


def main():
    _ = os.system("clear")
    print("{0} v.{1}\n{2}\n{3} <{4}>\n({5})\n"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __author__,
                  __email__,
                  __license__))

    environment = Environment(104, 104, 8)
#   environment.listener.setPosition(105, 105)
    environment.source[7].setPosition(200, 200)
    return 0


if __name__ == "__main__":
    main()
