#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from .icons_rc     import *
from PySide.QtCore import *
from PySide.QtGui  import *


class SelectorUI(object):
    def setupUi(self, preferences):
        css  = "QFrame ["
        css += "    background-color: #d2d2d2;"
        css += "]"
        css += "QToolButton ["
        css += "    background-color:transparent;"
        css += "    border:none;"
        css += "]"
        css += "QToolButton:checked, QToolButton:pressed ["
        css += "    background-color:rgb(193, 210, 238);"
        css += "    border: 1px solid rgb(60, 127, 177);"
        css += "]"
        css += "QToolButton:hover ["
        css += "    background-color:rgb(224, 232, 246);"
        css += "]"
        css += "QToolButton:checked:hover ["
        css += "    background-color:rgb(193, 210, 238);"
        css += "]"
        css  = css.replace("[", "{")
        css  = css.replace("]", "}")

        preferences.setObjectName("preferences")
        preferences.setWindowTitle("Preferences")
        preferences.resize(1102, 748)

        self.frame = QFrame(preferences)
        self.frame.setGeometry(QRect(0, 0, 1101, 401))
        self.frame.setStyleSheet(css)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")

        font = QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)

        self.snr = QToolButton(self.frame)
        self.snr.setObjectName("SNR")
        self.snr.setFont(font)
        self.snr.setText("SNR")
        self.snr.setGeometry(QRect(40, 30, 300, 300))
        icon0 = QIcon()
        icon0.addPixmap(QPixmap(":/images/snr-pane-1.png"),
                        QIcon.Normal,
                        QIcon.Off)
        self.snr.setIcon(icon0)
        self.snr.setIconSize(QSize(256, 256))
        self.snr.setCheckable(True)
        self.snr.setAutoExclusive(True)
        self.snr.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.localization = QToolButton(self.frame)
        self.localization.setObjectName("Localization")
        self.localization.setFont(font)
        self.localization.setText("Localization")
        self.localization.setGeometry(QRect(410, 30, 300, 300))
        icon1 = QIcon()
        icon1.addPixmap(QPixmap(":/images/localization-pane-1.png"),
                        QIcon.Normal,
                        QIcon.Off)
        self.localization.setIcon(icon1)
        self.localization.setIconSize(QSize(256, 256))
        self.localization.setCheckable(True)
        self.localization.setAutoExclusive(True)
        self.localization.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.optimization = QToolButton(self.frame)
        self.optimization.setObjectName("Optimization")
        self.optimization.setFont(font)
        self.optimization.setText("Optimization")
        self.optimization.setGeometry(QRect(750, 30, 300, 300))
        icon2 = QIcon()
        icon2.addPixmap(QPixmap(":/images/optimization-pane-1.png"),
                        QIcon.Normal,
                        QIcon.Off)
        self.optimization.setIcon(icon2)
        self.optimization.setIconSize(QSize(256, 256))
        self.optimization.setCheckable(True)
        self.optimization.setAutoExclusive(True)
        self.optimization.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.plabel = QLabel(preferences)
        self.plabel.setGeometry(QRect(20, 430, 100, 23))
        self.plabel.setText("Presets:")
        self.presets = QComboBox(preferences)
        self.presets.setGeometry(QRect(130, 430, 831, 23))
        self.presets.setObjectName("presets")
        self.presets.addItem("Choose presets...")

        self.rlabel = QLabel(preferences)
        self.rlabel.setGeometry(QRect(20, 490, 100, 23))
        self.rlabel.setText("Ratings:")
        self.ratings = QComboBox(preferences)
        self.ratings.setGeometry(QRect(130, 490, 831, 23))
        self.ratings.setObjectName("ratings")
        self.ratings.addItem("Choose ratings...")

        self.ilabel = QLabel(preferences)
        self.ilabel.setGeometry(QRect(20, 550, 100, 23))
        self.ilabel.setText("Instructions:")
        self.instructions = QComboBox(preferences)
        self.instructions.setGeometry(QRect(130, 550, 831, 23))
        self.instructions.setObjectName("instructions")
        self.instructions.addItem("Choose instructions...")

#       self.dlabel = QLabel(preferences)
#       self.dlabel.setGeometry(QRect(20, 610, 100, 23))
#       self.dlabel.setText("Description:")
#       self.description = QLineEdit(preferences)
#       self.description.setGeometry(QRect(130, 610, 831, 23))
#       self.description.setObjectName("description")
#       self.description.setPlaceholderText("Enter a new, unique short description of the goals for these instructions")

        self.submitter = QDialogButtonBox(preferences)
        self.submitter.setGeometry(QRect(920, 700, 166, 23))
        self.submitter.setStandardButtons(QDialogButtonBox.Cancel |
                                          QDialogButtonBox.Ok)
        self.submitter.setObjectName("submitter")

        QMetaObject.connectSlotsByName(preferences)


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    preferences = QDialog()
    ui = SelectorUI()
    ui.setupUi(preferences)
    preferences.show()
    sys.exit(app.exec_())
