#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  RatingsUI.py
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.12.14
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#


from __future__ import print_function
from six.moves  import input           # use raw_input when I say input
from os.path    import expanduser      # Cross-platform home directory finder

from PySide.QtCore import *
from PySide.QtGui  import *

import sys

from .Panes.RatingsPane     import *
from .Panes.RatingsMenuPane import *

__appname__    = "Sound Advice"
__module__     = "Ratings Dialog Window"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"
__module__     = "RatingsUI"


class UI(QWidget):
    """Ratings Dialog"""
    def setupUi(self, Widget, session):
        Widget.setObjectName("RatingsUI")
        Widget.setWindowTitle("Rate your experience")

        self.ratingsPane = RatingsPane(session, self)
        self.ratingsMenu = RatingsMenuPane(self)

        self.layout = QVBoxLayout(Widget)
        self.layout.setObjectName("verticalLayout")
        self.layout.addWidget(self.ratingsPane)
        self.layout.addWidget(self.ratingsMenu)
        QMetaObject.connectSlotsByName(Widget)


def main():
    # Instantiate a form, show it and start the app.
    import os.path
    from common.functions import unjson
    ratingsPath = os.path.expanduser("~/.config/sound-advice/ratings/")
    configuration = unjson(ratingsPath + "default")
    ratings = configuration["ratings"]

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")
    app = QApplication(sys.argv)

    Widget = QWidget()
    ui = UI()
    ui.setupUi(Widget, ratings)
    Widget.show()
    for twiddle in range(10):         # Twiddle our thumbs while waiting...
        QApplication.processEvents()  # ...for resize, et al to finish
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
