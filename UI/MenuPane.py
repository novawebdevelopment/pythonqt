# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MenuPane.ui'
#
# Created: Wed Dec  6 14:02:23 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide.QtCore import *
from PySide.QtGui  import *

from .Panes.DeviceSettingPane import *
from .Panes.InstructionsPane  import *

class MenuPane(QGroupBox):
    def __init__(self, initValues, parent=None):
        """Implement [Start/Stop], [Pause/Resume] and [Next] buttons"""
        super(MenuPane, self).__init__(parent)
        self.parent = parent

        self.width          = initValues["width"]
        self.height         = initValues["height"]
        self.settingButtons = initValues["programButtons"]

#       self.setWindowFlags(Qt.FramelessWindowHint)

        self.setObjectName("menus")
        self.setFlat(True)

        self.setFixedSize(self.width - self.height - 42,
                          self.height - 42)

        sizePolicy = QSizePolicy(QSizePolicy.Minimum,
                                 QSizePolicy.Maximum)

        self.deviceSetting = DeviceSettingPane(self.settingButtons, self)
        self.instruction  = InstructionsPane(self)

        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding,
                                 QSizePolicy.MinimumExpanding)

        font = QFont()
        font.setPointSize(20)
        font.setWeight(50)
        font.setBold(False)

        self.mainMenu = QGroupBox(self)
        self.mainMenu.setObjectName("mainMenu")
        self.mainMenu.setSizePolicy(sizePolicy)
        self.mainMenu.setTitle("")
        self.mainMenu.setAlignment(Qt.AlignCenter)

        sizePolicy = QSizePolicy(QSizePolicy.Maximum,
                                 QSizePolicy.Fixed)

        self.startButton = QPushButton(self.mainMenu)
        self.startButton.setObjectName("startButton")
        self.startButton.setSizePolicy(sizePolicy)
        self.startButton.setFont(font)
        self.startButton.setText("Start")

        self.pauseButton = QPushButton(self.mainMenu)
        self.pauseButton.setObjectName("pauseButton")
        self.pauseButton.setSizePolicy(sizePolicy)
        self.pauseButton.setFont(font)
        self.pauseButton.setText("Pause")

        self.rateButton = QPushButton(self.mainMenu)
        self.rateButton.setObjectName("rateButton")
        self.rateButton.setSizePolicy(sizePolicy)
        self.rateButton.setFont(font)
        self.rateButton.setText("Rate")

        self.nextButton = QPushButton(self.mainMenu)
        self.nextButton.setObjectName("nextButton")
        self.nextButton.setSizePolicy(sizePolicy)
        self.nextButton.setFont(font)
        self.nextButton.setText("Next")

        self.gridLayout = QGridLayout(self.mainMenu)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.addWidget(self.startButton, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.pauseButton, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.rateButton,  2, 0, 1, 1)
        self.gridLayout.addWidget(self.nextButton,  3, 0, 1, 1)

        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout.addWidget(self.deviceSetting)
        self.verticalLayout.addWidget(self.instruction)
        self.verticalLayout.addWidget(self.mainMenu)

        QMetaObject.connectSlotsByName(self)

    def setEnabled(self, state):
        """Enable / disable audio sample control buttons"""

        self.startButton.setEnabled(state)
        self.pauseButton.setEnabled(state)
        self.nextButton.setEnabled(state)
        self.rateButton.setEnabled(state)


def main():
    import sys
    app = QApplication(sys.argv)
    if len(sys.argv) == 3:
        width  = int(sys.argv[1])
        height = int(sys.argv[2])
    elif len(sys.argv) == 2:
        width  = int(sys.argv[1])
        height = int(sys.argv[1])
    else:
        screen_resolution = app.desktop().screenGeometry()
        width, height = screen_resolution.width(), screen_resolution.height()
    initValues = {"height": height,
                  "width": width,
                  "programButtons": 6}
    controlMenu = MenuPane(initValues)
    controlMenu.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
