# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'prolog.ui'
#
# Created: Mon Jun  4 21:13:28 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Prolog"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018, Kevin Cole (2018.06.04)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"


class UI(object):
    def setupUi(self, Prolog):
        Prolog.setObjectName("Prolog")
        Prolog.setWindowTitle("Introduction")

        self.header = QGroupBox(Prolog)
        self.header.setObjectName("header")
        self.header.setStyleSheet("QGroupBox {border: none;}")

        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)

        self.welcome = QLabel(self.header)
        self.welcome.setObjectName("welcome")
        self.welcome.setFont(font)
        self.welcome.setText("Welcome")

        spacer_1 = QSpacerItem(40, 20,
                              QSizePolicy.Expanding, QSizePolicy.Minimum)
        spacer_2 = QSpacerItem(40, 20,
                              QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.headerLayout = QHBoxLayout(self.header)
        self.headerLayout.setObjectName("headerLayout")
        self.headerLayout.addItem(spacer_1)
        self.headerLayout.addWidget(self.welcome)
        self.headerLayout.addItem(spacer_2)

        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())

        font.setPointSize(14)

        self.body = QGroupBox(Prolog)
        self.body.setObjectName("body")
        self.body.setSizePolicy(sizePolicy)

        self.div = QLabel(self.body)
        self.div.setObjectName("div")
        self.div.setFont(font)
        self.div.setAlignment(Qt.AlignTop | Qt.AlignHCenter)
        self.div.setWordWrap(True)
        self.div.setText("")
        self.div.setStyleSheet("QLabel {background-color: rgba(0, 0, 0, 0%);}")

        self.bodyLayout = QHBoxLayout(self.body)
        self.bodyLayout.setObjectName("bodyLayout")
        self.bodyLayout.addWidget(self.div)

        self.footer = QGroupBox(Prolog)
        self.footer.setObjectName("footer")
        self.footer.setStyleSheet("QGroupBox {border: none;}")

        self.proceed = QPushButton(self.footer)
        sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
        sizePolicy.setHeightForWidth(self.proceed.sizePolicy().hasHeightForWidth())
        self.proceed.setSizePolicy(sizePolicy)
        self.proceed.setObjectName("proceed")
        self.proceed.setText("Continue")

        spacer_3 = QSpacerItem(40, 20,
                              QSizePolicy.Expanding, QSizePolicy.Minimum)
        spacer_4 = QSpacerItem(40, 20,
                              QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.footerLayout = QHBoxLayout(self.footer)
        self.footerLayout.setObjectName("footerLayout")
        self.footerLayout.addItem(spacer_3)
        self.footerLayout.addWidget(self.proceed)
        self.footerLayout.addItem(spacer_4)

        self.prologLayout = QVBoxLayout(Prolog)
        self.prologLayout.setObjectName("prologLayout")
        self.prologLayout.addWidget(self.header)
        self.prologLayout.addWidget(self.body)
        self.prologLayout.addWidget(self.footer)


        QMetaObject.connectSlotsByName(Prolog)


def main():
    import sys
    app = QApplication(sys.argv)

    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    center = app.desktop().rect().center()

    Widget = QWidget()
    prolog = UI()
    prolog.setupUi(Widget)
    Widget.resize(int(width * 0.60), int(height * 0.80))
    Widget.move(center - Widget.rect().center())
    Widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
