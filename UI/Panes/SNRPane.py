#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  MainPanel.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Signal to Noise Ratio Control Pane: A speech level slider and a
#  noise level dial.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "SNR Control Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class ControlPane(QGroupBox):
    """Signal volume slider and Noise volume dial"""

    def __init__(self, parent=None):
        """Implement a Signal volume slider and a Noise volume dial"""

        css = ".frame {border: 1px solid #007f00;}"

        super(ControlPane, self).__init__(parent)
        self.parent = parent

        self.setObjectName("controlPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(10)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setTitle("")
        self.setProperty("class", "frame")  # for CSS
        self.setStyleSheet(css)

        self.signalGroup = QGroupBox(self)
        self.signalGroup.setObjectName("signalGroup")
        self.signalGroup.setTitle("")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.signalGroup.sizePolicy()
                                     .hasHeightForWidth())
        self.signalGroup.setSizePolicy(sizePolicy)

        self.signalLabel = QLabel(self.signalGroup)
        self.signalLabel.setObjectName("signalLabel")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.signalLabel.sizePolicy()
                                     .hasHeightForWidth())
        self.signalLabel.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.signalLabel.setFont(font)
        self.signalLabel.setAlignment(Qt.AlignHCenter |
                                      Qt.AlignTop)
        self.signalLabel.setText("Signal")

        self.signalLouder = QLabel(self.signalGroup)
        self.signalLouder.setObjectName("signalLouder")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.signalLouder.setFont(font)
        self.signalLouder.setAlignment(Qt.AlignRight |
                                       Qt.AlignTop |
                                       Qt.AlignTrailing)
        self.signalLouder.setText("Louder")

        self.signalLevel = QSlider(self.signalGroup)
        self.signalLevel.setObjectName("signalLevel")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(2)
        sizePolicy.setHeightForWidth(self.signalLevel.sizePolicy()
                                     .hasHeightForWidth())
        self.signalLevel.setSizePolicy(sizePolicy)
        self.signalLevel.setMinimum(-4)
        self.signalLevel.setMaximum(4)
        self.signalLevel.setPageStep(1)
        self.signalLevel.setOrientation(Qt.Vertical)
        self.signalLevel.setTickPosition(QSlider.TicksAbove)

        self.signalSofter = QLabel(self.signalGroup)
        self.signalSofter.setObjectName("signalSofter")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.signalSofter.setFont(font)
        self.signalSofter.setAlignment(Qt.AlignBottom |
                                       Qt.AlignRight |
                                       Qt.AlignTrailing)
        self.signalSofter.setText("Softer")

        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding,
                                 QSizePolicy.Minimum)
        spacerItem1 = QSpacerItem(40, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)

        self.signalGrid = QGridLayout(self.signalGroup)
        self.signalGrid.setObjectName("signalGrid")
        self.signalGrid.addWidget(self.signalLabel,  0, 0, 1, 4)
        self.signalGrid.addItem(spacerItem,          1, 0, 1, 1)
        self.signalGrid.addWidget(self.signalLouder, 1, 1, 1, 1)
        self.signalGrid.addWidget(self.signalLevel,  1, 2, 2, 1)
        self.signalGrid.addItem(spacerItem1,         1, 3, 1, 1)
        self.signalGrid.addWidget(self.signalSofter, 2, 1, 1, 1)

        self.noiseGroup = QGroupBox(self)
        self.noiseGroup.setObjectName("noiseGroup")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.noiseGroup.sizePolicy()
                                     .hasHeightForWidth())
        self.noiseGroup.setSizePolicy(sizePolicy)
        self.noiseGroup.setTitle("")

        self.noiseLabel = QLabel(self.noiseGroup)
        self.noiseLabel.setObjectName("noiseLabel")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.noiseLabel.sizePolicy()
                                     .hasHeightForWidth())
        self.noiseLabel.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.noiseLabel.setFont(font)
        self.noiseLabel.setAlignment(Qt.AlignHCenter |
                                     Qt.AlignTop)
        self.noiseLabel.setText("Noise")

        self.noiseSofter = QLabel(self.noiseGroup)
        self.noiseSofter.setObjectName("noiseSofter")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.noiseSofter.setFont(font)
        self.noiseSofter.setAlignment(Qt.AlignBottom |
                                      Qt.AlignRight |
                                      Qt.AlignTrailing)
        self.noiseSofter.setText("Softer")

        self.noiseLevel = QDial(self.noiseGroup)
        self.noiseLevel.setObjectName("noiseLevel")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(9)
        sizePolicy.setHeightForWidth(self.noiseLevel.sizePolicy()
                                     .hasHeightForWidth())
        self.noiseLevel.setSizePolicy(sizePolicy)
        self.noiseLevel.setMinimum(-4)
        self.noiseLevel.setMaximum(4)
        self.noiseLevel.setPageStep(1)
        self.noiseLevel.setNotchesVisible(True)

        self.noiseLouder = QLabel(self.noiseGroup)
        self.noiseLouder.setObjectName("noiseLouder")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.noiseLouder.setFont(font)
        self.noiseLouder.setAlignment(Qt.AlignBottom |
                                      Qt.AlignLeading |
                                      Qt.AlignLeft)
        self.noiseLouder.setText("Louder")

        self.noiseGrid = QGridLayout(self.noiseGroup)
        self.noiseGrid.setObjectName("noiseGrid")
        self.noiseGrid.addWidget(self.noiseLabel,  0, 0, 1, 3)
        self.noiseGrid.addWidget(self.noiseSofter, 1, 0, 1, 1)
        self.noiseGrid.addWidget(self.noiseLevel,  1, 0, 1, 3)
        self.noiseGrid.addWidget(self.noiseLouder, 1, 2, 1, 1)

        self.verticalLayout_5 = QVBoxLayout(self)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout_5.addWidget(self.signalGroup)
        self.verticalLayout_5.addWidget(self.noiseGroup)

    def setEnabled(self, state):
        """Enable / disable Signal slider and Noise dial"""

        self.signalLevel.setEnabled(state)
        self.noiseLevel.setEnabled(state)

    def keyPressEvent(self, event):
        super(ControlPane, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            QApplication.closeAllWindows()


def main():
    import sys
    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()

    controlPane = ControlPane()
    controlPane.setWindowFlags(Qt.FramelessWindowHint)
    controlPane.setFixedSize(height - 42, height - 42)
    controlPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
