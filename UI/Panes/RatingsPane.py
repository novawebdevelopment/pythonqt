#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  RatingsPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Ratings Pane: Side panel with three ratings on a scale of 1 to 5.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

from .Assets.Rating import *

__appname__    = "Sound Advice"
__module__     = "Ratings Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole",
                  "Marco Sirabella"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class RatingsPane(QGroupBox):
    """Provide feedback regarding the experience with the current unit"""

    def __init__(self, session, parent=None):
        """Implement the display of the rating questions"""

        super(RatingsPane, self).__init__(parent)
        self.setSizePolicy(QSizePolicy.Preferred,
                           QSizePolicy.Maximum)
        self.parent  = parent
        self.session = session
        self.ratings = []

        self.setObjectName("ratingsPane")
        self.setTitle("")

        for rating in self.session:
            self.ratings.append(Rating(rating, self))

        self.ratingsLayout = QVBoxLayout(self)
        self.ratingsLayout.setObjectName("ratingsLayout")
        for rating in self.ratings:
            self.ratingsLayout.addWidget(rating)

    def setEnabled(self, state):
        """Enable / disable ratings"""

        for rating in self.ratings:
            rating.setEnabled(state)

    def uncheckAll(self):
        """Reset all ratings to null values"""

        for rating in self.ratings:
            rating.uncheckAll()


def main():
    import sys
    import os
    from common.functions import unjson
    app = QApplication(sys.argv)
    ratingsPath = os.path.expanduser("~/.config/sound-advice/ratings/dwarves")
    configuration = unjson(ratingsPath)  # Ratings for entire session
    ratings = configuration["ratings"]
    ratingsPane = RatingsPane(ratings)
    ratingsPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
