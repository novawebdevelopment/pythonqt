#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  MainPanel.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Localization Main Panel: Eight Louder / Softer button pairs,
#  one pair per speaker, surrounding an avatar.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *
from PySide.QtSvg  import *

from math import sin, cos, radians

__appname__    = "Sound Advice"
__module__     = "Localization Main Panel"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class ControlPane(QGroupBox):
    """Eight independent selection and volume control buttins for audio sources"""

    WINGSPAN = 72.0  # finger-tip to finger-tip, initially 6 feet (72 inches)

    def __init__(self, initValues, parent=None):
        """Implement eight sets of [Select], [Louder] and [Softer] buttons in a ring"""

        css = ".frame {border: 1px solid #007f00;}"

        super(ControlPane, self).__init__(parent)
        self.parent     = parent
#       self.width      = initValues["width"]
#       self.height     = initValues["height"]
#       self.diameter   = self.height - 44
#       self.radius     = self.diameter / 2
        self.meatRadius = initValues["meatRadius"]

        self.setObjectName("controlPane")
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding,
                                 QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(10)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setTitle("")
        self.setProperty("class", "frame")  # for CSS
        self.setStyleSheet(css)

        self.speakers = []

        self.speakerSize = 55

        gradient  = "x1: 0, y1: 0, x2: 0, y2: 1,"
#       gradient += "stop: 0    #88d,"
#       gradient += "stop: 0.1  #99e,"
#       gradient += "stop: 0.49 #77c,"
#       gradient += "stop: 0.5  #66b,"
#       gradient += "stop: 1    #77c"
        gradient += "stop: 0    #dd8,"
        gradient += "stop: 0.1  #ee9,"
        gradient += "stop: 0.49 #cc7,"
        gradient += "stop: 0.5  #bb6,"
        gradient += "stop: 1    #cc7"

        self.style  = "color: {0};"
        self.style += "background-color: QLinearGradient({0});".format(gradient)
        self.style += "border-width: 1px;"
        self.style += "border-color: #339;"
        self.style += "border-style: solid;"
        self.style += "border-radius: 10;"
        self.style += "font-size: 24px;"
        self.style += "min-width:20px;"
        self.style += "min-height:20px;"
        self.style += "max-width:20px;"
        self.style += "max-height:20px;"
        self.style += "padding:5px 3px 5px 3px;"

        self.speaker1 = QGroupBox(self)
        self.speaker1Select = QPushButton(self.speaker1)
        self.speaker1Select.setObjectName("s1Select")
#       self.speaker1Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker1Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker1Select.setCheckable(True)
#       self.speaker1Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker1Select.setText("1")
        self.speaker1Louder = QPushButton(self.speaker1)
        self.speaker1Louder.setObjectName("s1Louder")
        self.speaker1Louder.setText("+")
        self.speaker1Softer = QPushButton(self.speaker1)
        self.speaker1Softer.setObjectName("s1Softer")
        self.speaker1Softer.setText("-")
        self.speakers.append({"louder": self.speaker1Louder,
                              "softer": self.speaker1Softer,
                              "select": self.speaker1Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker1Louder)
        layout.addWidget(self.speaker1Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker1Softer)
        self.speaker1.setLayout(layout)

        self.speaker2 = QGroupBox(self)
        self.speaker2Select = QPushButton(self.speaker2)
        self.speaker2Select.setObjectName("s2Select")
#       self.speaker2Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker2Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker2Select.setCheckable(True)
#       self.speaker2Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker2Select.setText("2")
        self.speaker2Louder = QPushButton(self.speaker2)
        self.speaker2Louder.setObjectName("s2Louder")
        self.speaker2Louder.setText("+")
        self.speaker2Softer = QPushButton(self.speaker2)
        self.speaker2Softer.setObjectName("s2Softer")
        self.speaker2Softer.setText("-")
        self.speakers.append({"louder": self.speaker2Louder,
                              "softer": self.speaker2Softer,
                              "select": self.speaker2Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker2Louder)
        layout.addWidget(self.speaker2Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker2Softer)
        self.speaker2.setLayout(layout)

        self.speaker3 = QGroupBox(self)
        self.speaker3Select = QPushButton(self.speaker3)
        self.speaker3Select.setObjectName("s3Select")
#       self.speaker3Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker3Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker3Select.setCheckable(True)
#       self.speaker3Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker3Select.setText("3")
        self.speaker3Louder = QPushButton(self.speaker3)
        self.speaker3Louder.setObjectName("s3Louder")
        self.speaker3Louder.setText("+")
        self.speaker3Softer = QPushButton(self.speaker3)
        self.speaker3Softer.setObjectName("s3Softer")
        self.speaker3Softer.setText("-")
        self.speakers.append({"louder": self.speaker3Louder,
                              "softer": self.speaker3Softer,
                              "select": self.speaker3Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker3Louder)
        layout.addWidget(self.speaker3Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker3Softer)
        self.speaker3.setLayout(layout)

        self.speaker4 = QGroupBox(self)
        self.speaker4Select = QPushButton(self.speaker4)
        self.speaker4Select.setObjectName("s4Select")
#       self.speaker4Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker4Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker4Select.setCheckable(True)
#       self.speaker4Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker4Select.setText("4")
        self.speaker4Louder = QPushButton(self.speaker4)
        self.speaker4Louder.setObjectName("s4Louder")
        self.speaker4Louder.setText("+")
        self.speaker4Softer = QPushButton(self.speaker4)
        self.speaker4Softer.setObjectName("s4Softer")
        self.speaker4Softer.setText("-")
        self.speakers.append({"louder": self.speaker4Louder,
                              "softer": self.speaker4Softer,
                              "select": self.speaker4Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker4Louder)
        layout.addWidget(self.speaker4Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker4Softer)
        self.speaker4.setLayout(layout)

        self.speaker5 = QGroupBox(self)
        self.speaker5Select = QPushButton(self.speaker5)
        self.speaker5Select.setObjectName("s5Select")
#       self.speaker5Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker5Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker5Select.setCheckable(True)
#       self.speaker5Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker5Select.setText("5")
        self.speaker5Louder = QPushButton(self.speaker5)
        self.speaker5Louder.setObjectName("s5Louder")
        self.speaker5Louder.setText("+")
        self.speaker5Softer = QPushButton(self.speaker5)
        self.speaker5Softer.setObjectName("s5Softer")
        self.speaker5Softer.setText("-")
        self.speakers.append({"louder": self.speaker5Louder,
                              "softer": self.speaker5Softer,
                              "select": self.speaker5Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker5Louder)
        layout.addWidget(self.speaker5Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker5Softer)
        self.speaker5.setLayout(layout)

        self.speaker6 = QGroupBox(self)
        self.speaker6Select = QPushButton(self.speaker6)
        self.speaker6Select.setObjectName("s6Select")
#       self.speaker6Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker6Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker6Select.setCheckable(True)
#       self.speaker6Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker6Select.setText("6")
        self.speaker6Louder = QPushButton(self.speaker6)
        self.speaker6Louder.setObjectName("s6Louder")
        self.speaker6Louder.setText("+")
        self.speaker6Softer = QPushButton(self.speaker6)
        self.speaker6Softer.setObjectName("s6Softer")
        self.speaker6Softer.setText("-")
        self.speakers.append({"louder": self.speaker6Louder,
                              "softer": self.speaker6Softer,
                              "select": self.speaker6Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker6Louder)
        layout.addWidget(self.speaker6Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker6Softer)
        self.speaker6.setLayout(layout)

        self.speaker7 = QGroupBox(self)
        self.speaker7Select = QPushButton(self.speaker7)
        self.speaker7Select.setObjectName("s7Select")
#       self.speaker7Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker7Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker7Select.setCheckable(True)
#       self.speaker7Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker7Select.setText("7")
        self.speaker7Louder = QPushButton(self.speaker7)
        self.speaker7Louder.setObjectName("s7Louder")
        self.speaker7Louder.setText("+")
        self.speaker7Softer = QPushButton(self.speaker7)
        self.speaker7Softer.setObjectName("s7Softer")
        self.speaker7Softer.setText("-")
        self.speakers.append({"louder": self.speaker7Louder,
                              "softer": self.speaker7Softer,
                              "select": self.speaker7Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker7Louder)
        layout.addWidget(self.speaker7Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker7Softer)
        self.speaker7.setLayout(layout)

        self.speaker8 = QGroupBox(self)
        self.speaker8Select = QPushButton(self.speaker8)
        self.speaker8Select.setObjectName("s8Select")
#       self.speaker8Select.setGeometry(QRect(20, 10, 28, 27))
        self.speaker8Select.setStyleSheet(self.style.format("#af0000"))
        self.speaker8Select.setCheckable(True)
#       self.speaker8Select.setText(unichr(0x25cf))  # Unicode filled circle
        self.speaker8Select.setText("8")
        self.speaker8Louder = QPushButton(self.speaker8)
        self.speaker8Louder.setObjectName("s8Louder")
        self.speaker8Louder.setText("+")
        self.speaker8Softer = QPushButton(self.speaker8)
        self.speaker8Softer.setObjectName("s8Softer")
        self.speaker8Softer.setText("-")
        self.speakers.append({"louder": self.speaker8Louder,
                              "softer": self.speaker8Softer,
                              "select": self.speaker8Select})

        layout = QVBoxLayout()
        layout.addWidget(self.speaker8Louder)
        layout.addWidget(self.speaker8Select, 0, Qt.AlignHCenter)
        layout.addWidget(self.speaker8Softer)
        self.speaker8.setLayout(layout)

        self.signal = QGroupBox(self)
        self.signal.setSizePolicy(sizePolicy)
        self.signal.setTitle("")
        self.signalPause = QPushButton(self.signal)
        self.signalPause.setObjectName("signalPause")
        font = QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.signalPause.setFont(font)
        self.signalPause.setText("Pause\nSignal")
        self.signalPause.setSizePolicy(sizePolicy)

        layout = QVBoxLayout()
        layout.addWidget(self.signalPause)
        self.signal.setLayout(layout)

        self.noise = QGroupBox(self)
        self.noise.setSizePolicy(sizePolicy)
        self.noise.setTitle("")
        self.noisePause = QPushButton(self.noise)
        self.noisePause.setObjectName("noisePause")
        font = QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.noisePause.setFont(font)
        self.noisePause.setText("Pause\nNoise")
        self.noisePause.setSizePolicy(sizePolicy)

        layout = QVBoxLayout()
        layout.addWidget(self.noisePause)
        self.noise.setLayout(layout)

        self.avatar = QSvgRenderer("images/avatar.svg")

    def paintEvent(self, event):
        """Draw / position the audio source buttons"""

        super(ControlPane, self).paintEvent(event)  # Draw the normal grey box
############################################################################
        bounds = self.geometry()
        self.X1, self.Y1, self.width, self.height = bounds.getRect()
        self.X1, self.Y1, self.X2,    self.Y2     = bounds.getCoords()
        self.centerX  = int((self.width  / 2.0))
        self.centerY  = int((self.height / 2.0))
        self.diameter = int(min(self.width, self.height)) - 50
        self.diameter -= self.speakerSize
        self.radius   = int(self.diameter / 2.0)
        self.left     = (self.centerX - self.radius) + 1
        self.top      = (self.centerY - self.radius) + 1
        self.right    = (self.centerX + self.radius)
        self.bottom   = (self.centerY + self.radius)
        self.compass  = []
        for degree in range(-90, -450, -45):
            x = self.centerX + (cos(radians(degree)) * self.radius)
            y = self.centerY + (sin(radians(degree)) * self.radius)
            self.compass.append((x, y))
        self.speaker1.setGeometry(self.compass[0][0] - 42,
                                  self.compass[0][1] - 50,
                                  85, 105)
        self.speaker2.setGeometry(self.compass[1][0] - 42,
                                  self.compass[1][1] - 50,
                                  85, 105)
        self.speaker3.setGeometry(self.compass[2][0] - 42,
                                  self.compass[2][1] - 50,
                                  85, 105)
        self.speaker4.setGeometry(self.compass[3][0] - 42,
                                  self.compass[3][1] - 50,
                                  85, 105)
        self.speaker5.setGeometry(self.compass[4][0] - 42,
                                  self.compass[4][1] - 50,
                                  85, 105)
        self.speaker6.setGeometry(self.compass[5][0] - 42,
                                  self.compass[5][1] - 50,
                                  85, 105)
        self.speaker7.setGeometry(self.compass[6][0] - 42,
                                  self.compass[6][1] - 50,
                                  85, 105)
        self.speaker8.setGeometry(self.compass[7][0] - 42,
                                  self.compass[7][1] - 50,
                                  85, 105)

        wide = max(self.signal.width(),  self.noise.width())
        high = max(self.signal.height(), self.noise.height())
        signalX = 10
        signalY = self.height - (high + 10)
        noiseX  = self.width  - (wide + 10)
        noiseY  = self.height - (high + 10)
        self.signal.setGeometry(signalX, signalY, wide, high)
        self.noise.setGeometry(noiseX,   noiseY,  wide, high)

        rg = QRadialGradient(self.centerX, self.centerY,
                             self.radius,
                             self.centerX, self.centerY)
#       rg.setColorAt(0, QColor.fromRgbF(1, 1, 1, 1.0))  # White, opaque
#       rg.setColorAt(1, QColor.fromRgbF(0, 0, 0, 0.5))  # Black, translucent
        rg.setColorAt(0, QColor(255, 255, 255,   0))  # White, transparent
        rg.setColorAt(1, QColor(191, 255, 191, 255))  # Green, opaque
############################################################################
        qp = QPainter()
        qp.begin(self)
#       qp.setBrush(QColor(Qt.green))
        qp.setBrush(QBrush(rg))
        qp.setPen(QPen(QColor(Qt.red), 1))
        self.drawRing(event, qp)
        qp.end()

    def drawRing(self, event, qp):
        """Draw the ring and the avatar"""

        square = QRect(self.left, self.top, self.diameter, self.diameter)
        qp.drawEllipse(square)
#       self.avatar.render(qp, QRectF(self.centerX - 198,
#                                     self.centerY - 32,
#                                     397, 63))
        self.scalingFactor = int(self.radius / self.meatRadius)  # pixels/inch
        width =  ControlPane.WINGSPAN        * self.scalingFactor
        depth = (ControlPane.WINGSPAN / 6.0) * self.scalingFactor
        self.avatar.render(qp, QRectF(self.centerX - (width / 2),
                                      self.centerY - (depth / 2),
                                      width, depth))
#       for rose in self.compass:
#           qp.drawEllipse(rose[0] - (self.speakerSize / 2),
#                          rose[1] - (self.speakerSize / 2),
#                          self.speakerSize,
#                          self.speakerSize)

    def setSpeakerEnabled(self, speaker, state):
        """Enable / disable an audio source"""

        self.speakers[speaker - 1]["select"].setEnabled(state)
        self.speakers[speaker - 1]["louder"].setEnabled(state)
        self.speakers[speaker - 1]["softer"].setEnabled(state)
        if state:
            self.speakers[speaker - 1]["select"].setStyleSheet(self.style.format("#00af00"))

    def setSpeakerSelectable(self, speaker, state):
        """Enable / disable selectability of an audio source"""

        self.speakers[speaker - 1]["select"].setEnabled(state)
        if state:
            self.speakers[speaker - 1]["select"].setStyleSheet(self.style.format("#af0000"))

    def setEnabled(self, state):
        """Enable / disable pane including master pause / resume buttons"""

        self.signalPause.setEnabled(state)
        self.noisePause.setEnabled(state)

        for speaker in range(1, 9):
            self.setSpeakerEnabled(speaker, state)

#       self.speaker1Louder.setEnabled(state)
#       self.speaker1Softer.setEnabled(state)
#       self.speaker2Louder.setEnabled(state)
#       self.speaker2Softer.setEnabled(state)
#       self.speaker3Louder.setEnabled(state)
#       self.speaker3Softer.setEnabled(state)
#       self.speaker4Louder.setEnabled(state)
#       self.speaker4Softer.setEnabled(state)
#       self.speaker5Louder.setEnabled(state)
#       self.speaker5Softer.setEnabled(state)
#       self.speaker6Louder.setEnabled(state)
#       self.speaker6Softer.setEnabled(state)
#       self.speaker7Louder.setEnabled(state)
#       self.speaker7Softer.setEnabled(state)
#       self.speaker8Louder.setEnabled(state)
#       self.speaker8Softer.setEnabled(state)

    def keyPressEvent(self, event):
        super(ControlPane, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            QApplication.closeAllWindows()


def main():
    import sys
    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()

    controlPane = ControlPane()
    controlPane.setWindowFlags(Qt.FramelessWindowHint)
    controlPane.setFixedSize(height - 42, height - 42)
    controlPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
