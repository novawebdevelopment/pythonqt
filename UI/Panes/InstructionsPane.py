#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  InstructionsPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.08.05
#
#  Device Setting Pane: Ever-changing instructions.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Instructions Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class InstructionsPane(QGroupBox):
    """Instructions for the user"""

    def __init__(self, parent=None):
        """Implement a pane for displaying instructions to the user"""

        super(InstructionsPane, self).__init__(parent)

        self.setObjectName("instructionsPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.setFont(font)
        self.label = QLabel(self)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignTop|Qt.AlignLeft)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.label.setText("Instructions:")
        self.settingLayout = QHBoxLayout(self)
        self.settingLayout.setObjectName("settingLayout")
        self.settingLayout.addWidget(self.label)

        self.setAlignment(Qt.AlignLeft)


def main():
    import sys
    app = QApplication(sys.argv)
    instructionsPane = InstructionsPane()
    instructionsPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
