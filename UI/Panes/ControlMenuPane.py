#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  ControlMenuPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Control Menu Pane: [Play] and [Next] buttons for controlling the flow.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Control Menu Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class ControlMenuPane(QGroupBox):
    """Audio sample control menu"""

    def __init__(self, parent=None):
        """Implement [Start/Stop], [Pause/Resume] and [Next] buttons"""

        super(ControlMenuPane, self).__init__(parent)
        self.parent = parent

        self.setObjectName("controlMenuPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setTitle("")

        self.startButton = QPushButton(self)
        self.startButton.setObjectName("startButton")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.startButton.setFont(font)
        self.startButton.setText("&Start")

        self.pauseButton = QPushButton(self)
        self.pauseButton.setObjectName("pauseButton")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.pauseButton.setFont(font)
        self.pauseButton.setText("&Pause")

        self.nextButton = QPushButton(self)
        self.nextButton.setObjectName("nextButton")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.nextButton.setFont(font)
        self.nextButton.setText("&Next")

        spacerItem = QSpacerItem(95, 20, QSizePolicy.Expanding,
                                 QSizePolicy.Minimum)
        spacerItem1 = QSpacerItem(96, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)
        spacerItem2 = QSpacerItem(95, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)
        spacerItem3 = QSpacerItem(95, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)

        self.controlMenuLayout = QHBoxLayout(self)
        self.controlMenuLayout.setObjectName("controlMenuLayout")
        self.controlMenuLayout.addItem(spacerItem)
        self.controlMenuLayout.addWidget(self.startButton)
        self.controlMenuLayout.addItem(spacerItem1)
        self.controlMenuLayout.addWidget(self.pauseButton)
        self.controlMenuLayout.addItem(spacerItem2)
        self.controlMenuLayout.addWidget(self.nextButton)
        self.controlMenuLayout.addItem(spacerItem3)

    def setEnabled(self, state):
        """Enable / disable audio sample control buttons"""

        self.startButton.setEnabled(state)
        self.pauseButton.setEnabled(state)
        self.nextButton.setEnabled(state)


def main():
    import sys
    app = QApplication(sys.argv)
    controlMenuPane = ControlMenuPane()
    controlMenuPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
