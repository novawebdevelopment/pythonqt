#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  DeviceSettingPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Device Setting Pane: User interface for assistive listening device
#  program selection setting.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Device Setting Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class DeviceSettingPane(QGroupBox):
    """Assistive Listening Device (ALD) program setting menu"""

    def __init__(self, settingButtons, parent=None):
        """Implement between one and six buttons for indicating ALD setting"""

        super(DeviceSettingPane, self).__init__(parent)
        self.parent = parent

        if settingButtons not in range(1, 7):
            raise Exception("Number of device program settings ({0}) not in range 1-6"
                            .format(settingButtons))

        self.settingButtons = settingButtons
        self.setObjectName("deviceSettingPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Maximum)
        self.setSizePolicy(sizePolicy)

        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.setFont(font)

        sizePolicy = QSizePolicy(QSizePolicy.Minimum,
                                 QSizePolicy.Preferred)

        self.label = QLabel(self)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setText("Program:")
        self.label.setSizePolicy(sizePolicy)
        labelSpacer = QSpacerItem(40, 20,
                                  QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)

        self.device = QButtonGroup()
        self.device.setObjectName("device")
        self.device.setExclusive(True)

        font = QFont()
        font.setPointSize(20)
        font.setWeight(50)
        font.setBold(False)

        sizePolicy = QSizePolicy(QSizePolicy.Minimum,
                                 QSizePolicy.Fixed)

        self.ready = []
        spacer  = []
        for setting in range(self.settingButtons):
            self.ready.append(QPushButton(self))
            self.ready[setting].setObjectName("ready{0}"
                                              .format(setting + 1))
            self.ready[setting].setFont(font)
            self.ready[setting].setSizePolicy(sizePolicy)
            self.ready[setting].setText("&{0}".format(setting + 1))
            self.ready[setting].setCheckable(True)
            width = self.ready[setting].fontMetrics().boundingRect("0").width() + 40
            self.ready[setting].setMaximumWidth(width)
            self.device.addButton(self.ready[setting])
            self.device.setId(self.ready[setting], setting + 1)
            spacer.append(QSpacerItem(40, 20,
                                      QSizePolicy.Expanding,
                                      QSizePolicy.Minimum))

        self.settingLayout = QHBoxLayout(self)
        self.settingLayout.setObjectName("settingLayout")
        self.settingLayout.addWidget(self.label)
        self.settingLayout.addItem(labelSpacer)
        for setting in range(self.settingButtons):
            self.settingLayout.addWidget(self.ready[setting])
            self.settingLayout.addItem(spacer[setting])

        self.setAlignment(Qt.AlignLeft)

    def setEnabled(self, state):
        """Enable / disable ALD program setting buttons"""

        for setting in range(self.settingButtons):
            self.ready[setting].setEnabled(state)

    def uncheckAll(self):
        """Uncheck all ALD program setting buttons"""

        self.device.setExclusive(False)
        for setting in range(self.settingButtons):
            self.ready[setting].setChecked(False)
            self.ready[setting].setStyleSheet("background: #e0e0e0;")
        self.device.setExclusive(True)


def main():
    import sys
    programButtons = int(raw_input("Number of programs: "))
    app = QApplication(sys.argv)
    deviceSettingPane = DeviceSettingPane(programButtons)
    deviceSettingPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
