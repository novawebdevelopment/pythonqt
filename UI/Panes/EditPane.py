#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  EditPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2018.02.05
#
#  Edit Instructions Pane: Ever-changing instructions.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Form implementation generated from reading ui file 'editMe.ui'
#
# Created: Mon Feb  5 15:32:52 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.2


from __future__ import print_function

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Instructions Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class EditPane(QGroupBox):
    def __init__(self, parent=None):
        super(EditPane, self).__init__(parent)

        self.setObjectName("editPane")

        boxCSS  = "QGroupBox:title {"
        boxCSS += "  color:        #7f0000;"
        boxCSS += "  padding-left: 20px;"
        boxCSS += "}"

        boxCSS += "QPushButton {"
        boxCSS += "  color:            #7f0000;"
        boxCSS += "  background-color: #ffffcf;"
        boxCSS += "}"

        textCSS  = "background: rgba(0, 0, 0, 0%);"
        textCSS += "border: 1px solid #ff0000;"

        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)

        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.resize(640, 480)

        self.text = QPlainTextEdit(self)
        self.text.setObjectName("text")
        self.text.setStyleSheet(textCSS)
        self.text.setFont(font)
#       self.text.setWordWrap(True)
        self.text.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.text.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.hint = QComboBox(self)
        self.hint.setObjectName("hint")
#       self.hint.setProperty("class", "hint")  # for CSS
#       self.hint.setStyleSheet("color: #7f0000;")

        self.label = QLabel(self)
        self.label.setObjectName("label")
        self.label.setText("Step:")

        self.step = QComboBox(self)
        self.step.setObjectName("step")
#       self.step.setProperty("class", "step")  # for CSS
#       self.step.setStyleSheet("color: #7f0000;")

        sizePolicy = QSizePolicy(QSizePolicy.Maximum,
                                 QSizePolicy.Preferred)

        self.prev = QPushButton(u"\u2190 Previous")
        self.prev.setObjectName("previous")
        self.prev.setSizePolicy(sizePolicy)

        self.save = QPushButton("Save")
        self.save.setObjectName("save")
        self.save.setSizePolicy(sizePolicy)

        self.next = QPushButton(u"Next \u2192")
        self.next.setObjectName("next")
        self.next.setSizePolicy(sizePolicy)

        self.exit = QPushButton("Exit")
        self.exit.setObjectName("exit")
        self.exit.setSizePolicy(sizePolicy)

        spacers = []
        for i in range(6):
            spacers.append(QSpacerItem(40, 20,
                                       QSizePolicy.Expanding,
                                       QSizePolicy.Minimum))

        self.gridLayout = QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.addWidget(self.hint,  0, 0, 1, 8)
        self.gridLayout.addWidget(self.text,  1, 0, 1, 8)
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.gridLayout.addWidget(self.step,  2, 1, 1, 1)
        self.gridLayout.addItem(spacers[0],   2, 2, 1, 1)
        self.gridLayout.addItem(spacers[1],   2, 3, 1, 1)
        self.gridLayout.addWidget(self.save,  2, 4, 1, 1)
        self.gridLayout.addItem(spacers[2],   2, 5, 1, 1)
        self.gridLayout.addWidget(self.exit,  2, 6, 1, 1)
        self.gridLayout.addItem(spacers[3],   2, 7, 1, 1)

        if __name__ == "__main__":
            self.prev.clicked.connect(self.debug)
            self.next.clicked.connect(self.debug)

        self.setStyleSheet(boxCSS)

        QMetaObject.connectSlotsByName(self)

    def debug(self):
        import re
        step = self.text.toPlainText()
        step = step.replace("\t", " ")             # Convert all [TAB] to " "
        while step[-1] == "\n":                    # Remove all trailing [LF]
            step = step[:-1]
        step = re.sub("\s*(\n)\s*", "\\1", step)   # Remove spaces around [LF]
        step = re.sub("(\s)\s+", "\\1", step)      # Remove multiple whitespace
        print(repr(step))
        instruction = {"step1": step.encode("ascii")}
        print(instruction)
        print("tab\ttab")


def main():
    import sys
    app = QApplication(sys.argv)
    dialog = QDialog()
    editPane = EditPane()
    layout = QVBoxLayout()
    layout.addWidget(editPane)
    dialog.setLayout(layout)
    dialog.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
#       pixmap = pixmap.scaled(QSize(w, h), Qt.KeepAspectRatio
