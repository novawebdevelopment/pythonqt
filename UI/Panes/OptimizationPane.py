#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  MainPanel.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Optimization Main Panel: Eight loudspeaker icons, surrounding an avatar.
#
#  Written with lots of help. See StackOverflow questions:
#
#   * PySide: Have an SVG image follow mouse without drag and drop
#   * Qt: Using QTransform object for rotate differs from setRotation in qgr
#   * Qt: OpenGl rotation with mouse drag negative delta#13997438
#   * PySide QGraphicsView size
#
#  Respectively:
#
#   * http://stackoverflow.com/questions/38749882
#   * http://stackoverflow.com/questions/7577371
#   * http://stackoverflow.com/questions/13997250
#   * http://stackoverflow.com/a/31907853/447830
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

import sys
from   time   import sleep
from   random import random
from   math   import sin, cos, radians, degrees, pi, log10, sqrt
import cmath

from PySide.QtCore import *
from PySide.QtGui  import *
from PySide.QtSvg  import *

from .Assets.Avatar import *
from .Assets.Source import *

__appname__    = "Sound Advice"
__module__     = "Optimization Main Panel"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

# Colors

WHITE_OPAQUE      = QColor.fromRgbF(1, 1, 1, 1.0)  # Currently unused
BLACK_TRANSLUCENT = QColor.fromRgbF(0, 0, 0, 0.5)  # Currently unused
GRAY_OPAQUE       = QColor(226, 226, 226, 255)     # Currently unused

WHITE_TRANSPARENT = QColor(255, 255, 255,   0)     # Radial gradient (unused)
GREEN_OPAQUE      = QColor(191, 255, 191, 255)     # Radial gradient (unused)

breadcrumbs = False  # Change to True to enable breadcrumb trail        # DEBUG


class ControlPane(QGraphicsView):
    """Virtual 2D "room" with avatar and sound sources"""

    dBs = Signal(list)  # Emit a signal full of dB changes

    def __init__(self, initValues, parent=None):
        """Implement a virtual 2D space of a given scale and populate with sounds and avatar"""

        self.parent     = parent
        self.width      = initValues["width"]
        self.height     = initValues["height"]
        self.diameter   = self.height - 44
        self.radius     = self.diameter / 2
        self.meatRadius = initValues["meatRadius"]

        self.spots       = []        # Avatar breadcrumb trail          # DEBUG
        self.distance    = [-1] * 8  # Distance of avatar from each speaker
        self.attenuation = [0]  * 8  # Attenuation of each speaker at avatar
        self.orientation = 0         # Avatar facing forward

        pixelsPerInch    = self.diameter / (self.meatRadius * 2.0)
        self.sourceSize  =  8.0 * pixelsPerInch  # OVERRIDDEN !!!
#       self.avatarWidth = 24.0 * pixelsPerInch  # For the custom icons 12.0
#       self.avatarDepth = 12.0 * pixelsPerInch  # For the custom icons  6.0
        self.avatarWidth = 17.0 * pixelsPerInch  # Dragana's sample size = 1
        self.avatarDepth =  8.5 * pixelsPerInch  # Dragana's sample size = 1
        self.sourceSize  = [8.0 * pixelsPerInch] * 8                  # DEBUG

#       self.compass     = [(0, 0)] * 8
        self.compass     = []        # The "compass rose" for speakers
        for degree in range(-90, -450, -45):
            x = self.radius + (cos(radians(degree)) * (self.radius - 10))
            y = self.radius + (sin(radians(degree)) * (self.radius - 10))
            self.compass.append((x, y))

        super(ControlPane, self).__init__(parent)
        self.setObjectName("controlPane")
        self.setFixedSize(self.diameter + 2, self.diameter + 2)
        self.setStyleSheet("border: 1px solid #007f00;"
                           "background-color: #efefaf;")  # Yellow w/ green

        self.cursor = QCursor()
        self.setCursor(self.cursor)

        self.scene = QGraphicsScene(0, 0,
                                    self.diameter, self.diameter,
                                    self)
        self.setScene(self.scene)

        # Place source icons inside and tangent to "ring".
        self.sources = []
        sourceScale  = []
        for source in range(8):
            image = "images/blue-loudspeaker-{0}.svg".format(source + 1)
            self.sources.append(Source(image=image))
            self.scene.addItem(self.sources[source])
            sourceScale.append(self.sourceSize[source] / self.sources[source].w)
            self.sources[source].setScale(sourceScale[source])
            new_x = self.compass[source][0] - self.sources[source].compass[source][0]
            new_y = self.compass[source][1] - self.sources[source].compass[source][1]
            self.sources[source].setPos(new_x, new_y)
#       self.sources    = [Source(image="images/empty.svg")] * 8
#       self.sources    = [Source(image="images/QuestionMark.svg")] * 8
#       self.sources = []
#       for i in range(8):
#           image = "images/empty.svg"
#           self.sources.append(Source(image=image))
#       self.sources[0] = Source(image="images/CandleLightDate.svg")
#       self.sources[3] = Source(image="images/Phone.svg")
#       self.sources[5] = Source(image="images/KitchenUtensils.svg")
#       self.sources[6] = Source(image="images/CoupleOutToEat.svg")
#       for source in range(8):
#           self.scene.addItem(self.sources[source])

        self.avatar = Avatar()
        avatarScale = self.avatarWidth / self.avatar.w
        self.avatar.setScale(avatarScale)
        self.avatar.setPos(self.radius - self.avatar.cx,
                           self.radius - self.avatar.cy - 10)
        self.avatar.oldX  = self.avatar.x()
        self.avatar.oldY  = self.avatar.y()
        self.avatar.zeroX = self.avatar.x()
        self.avatar.zeroY = self.avatar.x()
        self.scene.addItem(self.avatar)

    def setEnabled(self, enabled):
        """Gray out the avatar when disabled"""
        super(ControlPane, self).setEnabled(enabled)
        if enabled:
            self.avatar.setGraphicsEffect(None)
        else:
            grayed = QGraphicsColorizeEffect(self.avatar)
            grayed.setColor(QColor(127, 127, 127))
            self.avatar.setGraphicsEffect(grayed)

    def deltaX(self, degrees):
        return int((cos(radians(degrees)) * self.avatarWidth))

    def deltaY(self, degrees):
        return int((sin(radians(degrees)) * self.avatarWidth))

    def keyPressEvent(self, event):
        """Rotate avatar and set sound levels when avatar moves"""

        turn = {55: 225,       56: 270,       57: 315,        # 7   8   9
                52: 180,                      54:   0,        # 4  (5)  6
                49: 135,       50:  90,       51:  45,        # 1   2   3
                16777232: 225, 16777235: 270, 16777238: 315,  # 7   8   9
                16777234: 180,                16777236:   0,  # 4  (5)  6
                16777233: 135, 16777237:  90, 16777239:  45}  # 1   2   3

        if event.isAutoRepeat():
            return
        press = event.key()
        print(press)

        if press == Qt.Key_Escape:          # "ESCape" hatch emergency exit
            QApplication.closeAllWindows()

#       direction = press & 0xDF            # Bitwise mask to upper case
        direction = press
        if direction in turn:
            newX = self.avatar.x() + self.deltaX(turn[direction])
            newY = self.avatar.y() + self.deltaY(turn[direction])
            if newX in range(self.diameter) and \
               newY in range(self.diameter):
                self.avatar.setPos(QPoint(newX, newY))
            self.orientation = turn[direction] + 90
        elif direction in (53, 16777227):
# ============================================================================
#           dx = self.avatar.zeroX - self.avatar.x()  # delta X
#           dy = self.avatar.zeroY - self.avatar.y()  # delta Y
#           iNumber   = dx - dy * 1j
#           angle     = cmath.phase(iNumber) + 1.5 * pi
#           self.orientation  = (360 - degrees(angle)) % 360
#           transform = QTransform()
#           transform.translate(self.avatar.cx,  self.avatar.cy)
#           transform.rotate(random() * 360)
#           transform.translate(-self.avatar.cx, -self.avatar.cy)
#           self.avatar.setTransform(transform)
#           print("Orientation = {0}".format(self.orientation))
#           self.avatar.update()
#           sleep(5)
#           while (self.avatar.x() != self.avatar.zeroX) or \
#                 (self.avatar.x() != self.avatar.zeroX):
#               nextX = self.avatar.x() - round(cos(angle))
#               nextY = self.avatar.y() - round(sin(angle))
#               print(self.avatar.zeroX - nextX,
#                     self.avatar.zeroY - nextY)
#               self.avatar.setPos(QPoint(nextX, nextY))
#               self.update()
#               sleep(0.25)
# ============================================================================
            self.avatar.setPos(QPoint(self.avatar.zeroX,
                                      self.avatar.zeroY))
            self.orientation = 0

        transform = QTransform()
        transform.translate(self.avatar.cx, self.avatar.cy)
        transform.rotate(self.orientation)
        transform.translate(-self.avatar.cx, -self.avatar.cy)
        self.avatar.setTransform(transform)

        self.levelChange([self.avatar.x() + self.avatar.cx,
                          self.avatar.y() + self.avatar.cy])

        self.avatar.oldX = self.avatar.x()
        self.avatar.oldY = self.avatar.y()
        super(ControlPane, self).keyPressEvent(event)

    def reset(self):
        """Recenter avatar, reset dB levels, remove breadcrumb spots"""

        self.avatar.setPos(QPoint(self.avatar.zeroX,
                                  self.avatar.zeroY))
        self.orientation  = 0  # in degrees
        transform = QTransform()
        transform.translate(self.avatar.cx,  self.avatar.cy)
        transform.rotate(self.orientation)
        transform.translate(-self.avatar.cx, -self.avatar.cy)
        self.avatar.setTransform(transform)
        self.avatar.oldX = self.avatar.x()
        self.avatar.oldY = self.avatar.y()

        if breadcrumbs:                                                 # DEBUG
            for i, spot in enumerate(self.spots):                       # DEBUG
                self.scene.removeItem(spot)                             # DEBUG
            self.spots = []                                             # DEBUG

        self.levelChange([self.avatar.oldX + self.avatar.cx,
                          self.avatar.oldY + self.avatar.cy])

    def levelChange(self, coords):
        """Calculate dB values based on distances. Add breadcrumbs."""

        moved = False
        nearby = False                                                  # DEBUG
        for speaker, rose in enumerate(self.compass):
            x = coords[0] - rose[0]
            y = coords[1] - rose[1]
            distance = sqrt(x**2 + y**2)
            if abs(self.distance[speaker] - distance) > 9:
                moved = True
                self.distance[speaker] = distance
                ratio = float(self.radius) / float(distance)
                self.attenuation[speaker] = min(20 * log10(ratio), 12.0)
            if distance < 20.0:                                         # DEBUG
                nearby = True                                           # DEBUG

        if breadcrumbs:                                                 # DEBUG
            if nearby:                                                  # DEBUG
                oil = Qt.cyan                                           # DEBUG
            else:                                                       # DEBUG
                oil = Qt.red                                            # DEBUG
#                                                                       # DEBUG
            self.spots.append(QGraphicsEllipseItem(coords[0] - 2,       # DEBUG
                                                   coords[1] - 2,       # DEBUG
                                                   4, 4))               # DEBUG
            self.spots[-1].setPen(QPen(QColor(oil), 1))                 # DEBUG
            self.spots[-1].setBrush(QBrush(oil))                        # DEBUG
            self.scene.addItem(self.spots[-1])                          # DEBUG
            if len(self.spots) > 25:                                    # DEBUG
                for spot in self.spots[0:-25]:                          # DEBUG
                    self.scene.removeItem(spot)                         # DEBUG
                for i, spot in enumerate(self.spots[0:-25]):            # DEBUG
                    del self.spots[i]                                   # DEBUG

        if moved:
            self.dBs.emit(self.attenuation)


def main():
    import sys
    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    initValues = {"width":      width,
                  "height":     height,
                  "meatRadius":   102}
    controlPane = ControlPane(initValues)
    controlPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
