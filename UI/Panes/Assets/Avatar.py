#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Written by Kevin Cole <kevin.cole@novawebcoop.org> 2016.11.05
#
# With lots of help. See StackOverflow questions:
#
#   PySide: Have an SVG image follow mouse without drag and drop
#   Qt: Using QTransform object for rotate differs from setRotation in qgr
#   Qt: OpenGl rotation with mouse drag negative delta#13997438
#   PySide QGraphicsView size
#   PyQt: GraphicsView - Simple Animation
#
# respectively:
#
#   http://stackoverflow.com/questions/38749882
#   http://stackoverflow.com/questions/7577371
#   http://stackoverflow.com/questions/13997250
#   http://stackoverflow.com/a/31907853/447830
#   https://wiki.python.org/moin/PyQt/GraphicsView_-_SimpleAnimation
#

from PySide.QtCore import *
from PySide.QtGui  import *
from PySide.QtSvg  import *


class Avatar(QGraphicsSvgItem):
    """Virtual 2D representation of a client"""

    moved = Signal((list,))  # Emit a signal with current location

    def __init__(self, image="images/armless.svg", parent=None):
        """Draw the avatar and store position and size info"""

        super(Avatar, self).__init__(image, parent)
        self.parent   = parent
        self.svgSize  = self.renderer().defaultSize()
        self.w        = self.svgSize.width()   # Default avatar width
        self.h        = self.svgSize.height()  # Default avatar height
        self.cx       = int(self.w / 2)        # Center of avatar
        self.cy       = int(self.h / 2)        # Center of avatar
        self.nowX     = self.x()
        self.nowY     = self.y()
        self.oldX     = self.x()
        self.oldY     = self.y()
        self.zeroX    = self.x()
        self.zeroY    = self.y()
        self.mobile   = 0     # Initially immobile

        self.setFlags(QGraphicsItem.ItemIsMovable)
        self.setAcceptsHoverEvents(True)

    def resizeEvent(self, event):
        """Recalculate values based on new size"""

        super(Avatar, self).resizeEvent(event)
        self.svgSize  = self.size()            # New avatar size
        self.w        = self.svgSize.width()   # New avatar width
        self.h        = self.svgSize.height()  # New avatar height
        self.cx       = int(self.w / 2)        # Center of avatar
        self.cy       = int(self.h / 2)        # Center of avatar

    def mousePressEvent(self, event):
        """Toggle avatar mobility on mouse presses"""

        super(Avatar, self).mousePressEvent(event)
        self.mobile = (self.mobile + 1) % 2  # Toggle mobility
