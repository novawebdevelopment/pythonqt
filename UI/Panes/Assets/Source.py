#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Written by Kevin Cole <kevin.cole@novawebcoop.org> 2016.11.05
#
# With lots of help. See StackOverflow questions:
#
#   PySide: Have an SVG image follow mouse without drag and drop
#   Qt: Using QTransform object for rotate differs from setRotation in qgr
#   Qt: OpenGl rotation with mouse drag negative delta#13997438
#   PySide QGraphicsView size
#   PyQt: GraphicsView - Simple Animation
#
# respectively:
#
#   http://stackoverflow.com/questions/38749882
#   http://stackoverflow.com/questions/7577371
#   http://stackoverflow.com/questions/13997250
#   http://stackoverflow.com/a/31907853/447830
#   https://wiki.python.org/moin/PyQt/GraphicsView_-_SimpleAnimation
#

from PySide.QtCore import *
from PySide.QtGui  import *
from PySide.QtSvg  import *

from math          import *


class Source(QGraphicsSvgItem):
    """Virtual 2D representation of a sound source"""

    def __init__(self, environment=None,
                 sample=1, name="", image=None, X=0, Y=0,
                 preset=0, level=0, on=True, paused=False,
                 signal=False, weight=1, parent=None):
        """Initialize sound source level, location, priority"""

        # pylint: disable=too-many-instance-attributes

        super(Source, self).__init__(image, parent)
        self.parent   = parent

        self.dB       = 65                     # Loudness at source
        self.sample   = sample                 # index of sample to be played
        self.name     = name                   # Sample name
        self.image    = image                  # visual representation
        self.on       = on                     # Unmuted / Powered
        self.paused   = paused                 # Initially not paused
        self.preset   = preset                 # p/reset level
        self.signal   = signal                 # noise
        self.level    = level                  # current level (dB)
        self.weight   = weight                 # weighted importance

        self.environment = environment
        self.svgSize  = self.renderer().defaultSize()
        self.x        = self.x()               # X within the scene
        self.y        = self.y()               # Y within the scene
        self.w        = self.svgSize.width()   # Default source width
        self.h        = self.svgSize.height()  # Default source height
        self.cx       = int(self.w / 2)        # Center of source
        self.cy       = int(self.h / 2)        # Center of source
        self.radius   = max(self.cx, self.cy)  # (with luck, cx == cy)
        self.compass  = []                     # Source "anchor points"
        for degree in range(-90, -450, -45):
            x = self.cx + (cos(radians(degree)) * self.radius)
            y = self.cx + (sin(radians(degree)) * self.radius)
            self.compass.append((x, y))

#       if X > self.environment.x or Y > self.environment.y:
#           raise UnknownSpace(self.environment, "Source", (X, Y))
#       self.x           = X
#       self.y           = Y
#       self.position    = [X, Y]  # from center (origin)
#       self.distance    = sqrt(X**2 + Y**2)  # from center (origin) inches
#       self.direction   = None    # from center (origin) in degrees

    def resizeEvent(self, event):
        """Recalculate values based on new size"""

        super(Source, self).resizeEvent(event)
#       self.x        = self.x()               # X within the scene
#       self.y        = self.y()               # Y within the scene
        self.svgSize  = self.size()            # New source size
        self.w        = self.svgSize.width()   # New source width
        self.h        = self.svgSize.height()  # New source height
        self.cx       = int(self.w / 2)        # New center of source
        self.cy       = int(self.h / 2)        # New center of source
        self.radius   = max(self.cx, self.cy)  # (with luck, cx == cy)
        self.compass  = []                     # New source "anchor points"
        for degree in range(-90, -450, -45):
            x = self.cx + (cos(radians(degree)) * self.radius)
            y = self.cx + (sin(radians(degree)) * self.radius)
            self.compass.append((x, y))

    def setSample(self, sample=1):
        """Set sample to be played by source"""

        self.sample = sample

    def setName(self, name=""):
        self.name = name

    def setImage(self, image=None):
        """Set source's visual representation"""

        self.image = image

    def setOn(self, on=True):
        """Set source to muted or unmuted"""

        self.on = on

    def mute(self):
        self.setOn(False)
        self.muted = True

    def unmute(self):
        self.setOn(True)
        self.muted = False

    def setPause(self, paused=False):
        self.paused = paused

    def pause(self):
        self.setPause(True)

    def resume(self):
        self.setPause(False)

    def setLevel(self, level=0):
        """Set source output level"""

        self.level = level

    def setSignal(self, signal):
        """Designate source as either a signal or noise"""

        self.signal = signal

    def asSignal(self):
        self.setSignal(True)

    def asNoise(self):
        self.setSignal(False)

    def setPosition(self, X=0, Y=0):
        """Set source's position from center origin"""

#       if X > self.environment.x or Y > self.environment.y:
#           raise UnknownSpace(self.environment, "Source", (X, Y))
        self.position = [X, Y]
        self.x        = X
        self.y        = Y
        self.distance = sqrt(X**2 + Y**2)

    def setDistance(self, distance=0):
        """Set source's distance from avatar"""

        self.distance = distance

    def setDirection(self, direction=0):
        """Set source's direction from avatar"""

        self.direction = direction

    def setWeight(self, weight):
        """Set source's relative importance as a signal"""

        self.weight = weight
