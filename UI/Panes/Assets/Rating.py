#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  rating.py
#
#  A Rating class
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.07.10
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

from __future__ import print_function, absolute_import
from six.moves  import input           # use raw_input when I say input
from os.path    import expanduser      # Cross-platform home directory finder
import os
import sys

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Rating"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole",
                  "Marco Sirabella"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

heading = QFont()
heading.setPointSize(20)
heading.setWeight(75)
heading.setBold(True)

normal = QFont()
normal.setPointSize(20)
normal.setWeight(50)
normal.setBold(False)


class Option:
    """Rating option"""

    def __init__(self, option, parent=None):
        """Expects a list [recode value, prompt]"""

        self.parent   = parent
        self.prompt   = option[1]  # Text seen by user
        self.recode   = option[0]  # Recoded value stored in results file
        if   self.parent.genre == "radio":
            self.button = QRadioButton()
            self.button.clicked.connect(self.setValue)
        elif self.parent.genre == "check":
            self.button = QCheckBox()
            self.button.stateChanged.connect(self.setValue)
#       self.button.setObjectName()
        self.button.setFont(normal)
        self.button.setText(self.prompt)

    def setValue(self):
        """When checked, option value equals recode otherwise None"""

        if self.button.isChecked():
            if   self.parent.genre == "radio":
                self.parent.value = self.recode
            elif self.parent.genre == "check" and \
                 self.recode not in self.parent.value:
                self.parent.value.append(self.recode)
        else:
            if self.parent.genre == "check":
                self.parent.value[:] = (x for x in self.parent.value
                                        if x != self.recode)


class Rating(QGroupBox):
    """Rating question and response"""

    def __init__(self, entry, parent=None):
        super(Rating, self).__init__(parent)
        self.setSizePolicy(QSizePolicy.Minimum,
                           QSizePolicy.Maximum)
        self.genre   = entry["subtype"]       # "radio", "check", "free"
        self.name    = entry["name"]          # "CSV column heading"
        self.query   = entry["question"]      # "Your experience was..."
        self.rated   = False                  # Nothing selected
        if self.genre in ("radio", "check"):
            self.options = []
            for option in entry["options"]:
                self.options.append(Option(option, self))

        if   self.genre == "radio":
#           self.setStyleSheet("background-color: #ffeeee;")
            self.value = None
        elif self.genre == "check":
#           self.setStyleSheet("background-color: #eeffee;")
            self.value = []
        elif self.genre == "free":
#           self.setStyleSheet("background-color: #eeeeff;")
            self.value = ""

        self.setObjectName(self.name + "Statement")

        self.layout = QVBoxLayout(self)
        self.layout.setObjectName(self.name + "Layout")

        self.label = QLabel(self.query)
        self.label.setWordWrap(True)
        self.label.setFont(heading)
        self.label.setSizePolicy(QSizePolicy.Minimum,
                                 QSizePolicy.Minimum)
        self.layout.addWidget(self.label)

        if   self.genre == "radio":
            self.buttons = QButtonGroup()
            self.buttons.setObjectName(self.name)
            for option in self.options:
                self.buttons.addButton(option.button)
                self.buttons.setId(option.button, option.recode)
                self.layout.addWidget(option.button)

        elif self.genre == "check":
            for option in self.options:
                self.layout.addWidget(option.button)

        elif self.genre == "free":
            self.fillIn = QPlainTextEdit()
            self.fillIn.setObjectName(self.name + "Statement")
            self.fillIn.setFont(normal)
            self.fillIn.setWordWrapMode(QTextOption.WordWrap)

            self.layout.addWidget(self.fillIn)

    def getValue(self):
        if self.genre in ("radio", "check"):
            if self.genre == "check":
                self.value.sort()
            return self.value
        elif self.genre == "free":
            return self.fillIn.toPlainText()

    def setEnabled(self, state):
        """Enable / disable rating"""

        if   self.genre in ("radio", "check"):
            for option in self.options:
                option.button.setEnabled(state)
        elif self.genre == "free":
            self.fillIn.setEnabled(state)

    def uncheckAll(self):
        """Reset rating to null values"""

        if   self.genre == "radio":
            self.value = None
            self.buttons.setExclusive(False)
            for option in self.options:
                option.button.setChecked(False)
            self.buttons.setExclusive(True)

        elif self.genre == "check":
            self.value = []
            for option in self.options:
                option.button.setChecked(False)

        elif self.genre == "free":
            self.value = ""
            self.fillIn.clear()

        self.rated = False


def main():
    """
    >>> app = QApplication(sys.argv)
    >>> whateva = {"subtype": "radio"}
    >>> whateva["question"] = "At the sound of the tone, the time will be..."
    >>> whateva["name"] = "time"
    >>> whateva["options"] = [[1, "13:00"], [2, "14:00"], [3, "15:00"]]
    >>> x = Rating(whateva)
    >>> x  # doctest: +ELLIPSIS
    <__main__.Rating object at ...>
    >>> x.genre
    'radio'
    >>> x.name
    'time'
    >>> x.query
    'At the sound of the tone, the time will be...'
    >>> x.rated
    False
    >>> x.options[0].prompt
    '13:00'
    >>> x.options[0].recode
    1
    >>> QCoreApplication.exit()
    """

    import doctest

    _ = os.system("clear")
    print("{0} v.{1}\n{2} ({3})\n{4}, {5} <{6}>\n"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __license__,
                  __author__,
                  __agency__,
                  __email__))

    doctest.testmod()

    try:
        app = QApplication(sys.argv)
    except:
        app = QApplication.instance()

    whateva = {"subtype": "radio"}
    whateva = {"subtype": "check"}
    whateva["question"] = "At the sound of the tone, the time will be..."
    whateva["name"] = "time"
    whateva["options"] = [[1, "13:00"], [2, "14:00"], [3, "15:00"]]
    rating = Rating(whateva)
    rating.show()
    sys.exit(app.exec_())

    return 0


if __name__ == "__main__":
    main()
