#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  RatingsMenuPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Ratings Menu Pane: [Save] button.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Ratings Menu Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class RatingsMenuPane(QGroupBox):
    """Ratings menu"""

    def __init__(self, parent=None):
        """Implement the [Save] button"""

        super(RatingsMenuPane, self).__init__(parent)
        self.parent = parent

        self.setObjectName("ratingsMenuPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred,
                                 QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setTitle("")

        self.rateButton = QPushButton(self)
        self.rateButton.setObjectName("rateButton")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.rateButton.setFont(font)
        self.rateButton.setText("&Save")

        spacerItem3 = QSpacerItem(65, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)
        spacerItem4 = QSpacerItem(65, 20, QSizePolicy.Expanding,
                                  QSizePolicy.Minimum)

        self.ratingsLayout = QHBoxLayout(self)
        self.ratingsLayout.setObjectName("ratingsLayout")
        self.ratingsLayout.addItem(spacerItem3)
        self.ratingsLayout.addWidget(self.rateButton)
        self.ratingsLayout.addItem(spacerItem4)

    def setEnabled(self, state):
        """Enable / disable the [Save] button"""

        self.rateButton.setEnabled(state)


def main():
    import sys
    app = QApplication(sys.argv)
    ratingsMenuPane = RatingsMenuPane()
    ratingsMenuPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
