#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  MainPanel.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Distance Main Panel: Eight Louder / Softer button pairs,
#  one pair per speaker, surrounding an avatar.
#
#  Written with lots of help. See StackOverflow questions:
#
#   * PySide: Have an SVG image follow mouse without drag and drop
#   * Qt: Using QTransform object for rotate differs from setRotation in qgr
#   * Qt: OpenGl rotation with mouse drag negative delta#13997438
#   * PySide QGraphicsView size
#
#  Respectively:
#
#   * http://stackoverflow.com/questions/38749882
#   * http://stackoverflow.com/questions/7577371
#   * http://stackoverflow.com/questions/13997250
#   * http://stackoverflow.com/a/31907853/447830
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

import sys
from   math import sin, cos, radians, degrees, pi, log10, sqrt
import cmath


from PySide.QtCore import *
from PySide.QtGui  import *
from PySide.QtSvg  import *

from .Assets.Avatar import *
from .Assets.Source import *

__appname__    = "Sound Advice"
__module__     = "Distance Main Panel"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

# Colors

WHITE_OPAQUE      = QColor.fromRgbF(1, 1, 1, 1.0)  # Currently unused
BLACK_TRANSLUCENT = QColor.fromRgbF(0, 0, 0, 0.5)  # Currently unused
GRAY_OPAQUE       = QColor(226, 226, 226, 255)     # Currently unused

WHITE_TRANSPARENT = QColor(255, 255, 255,   0)     # Radial gradient (unused)
GREEN_OPAQUE      = QColor(191, 255, 191, 255)     # Radial gradient (unused)

breadcrumbs = False  # Change to True to enable breadcrumb trail        # DEBUG


class ControlPane(QGraphicsView):
    """Virtual 2D "room" with avatar and sound sources"""

    dBs = Signal(list)  # Emit a signal full of dB changes

    def __init__(self, meatRadius=52, parent=None):
        """Implement a virtual 2D space of a given scale and populate with sounds and avatar"""

        super(ControlPane, self).__init__(parent)
        self.parent = parent
        self.meatRadius = meatRadius

        self.setObjectName("controlPane")

        self.cursor = QCursor()
        self.setCursor(self.cursor)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setFrameShape(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        sizePolicy = QSizePolicy(QSizePolicy.Expanding,
                                 QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(1)
        self.setSizePolicy(sizePolicy)
        self.setStyleSheet("border: 1px solid #007f00;"
                           "background-color: #ffff7f;")  # Yellow w/ green

        self.scene = QGraphicsScene(self)

        self.spots       = []        # Avatar breadcrumb trail          # DEBUG
        self.distance    = [-1] * 8  # Distance of avatar from each speaker
        self.attenuation = [0]  * 8  # Attenuation of each speaker at avatar

        self.avatar = Avatar()
        self.scene.addItem(self.avatar)

        self.sources = []
        for source in range(8):
            image = "images/blue-loudspeaker-{0}.svg".format(source + 1)
            self.sources.append(Source(image=image))
            self.scene.addItem(self.sources[-1])

    def resizeEvent(self, event):
        """Reposition everything when resizing"""

        super(ControlPane, self).resizeEvent(event)
        bounds = self.geometry()

        self.X1, self.Y1, self.w,  self.h  = bounds.getRect()
        self.X1, self.Y1, self.X2, self.Y2 = bounds.getCoords()

        self.cx  = bounds.center().x()
        self.cy  = bounds.center().y()

        self.diameter = int(min(self.w, self.h)) - 30
        self.radius   = int(self.diameter / 2.0)

        self.left     = (self.cx - self.radius) + 1
        self.top      = (self.cy - self.radius) + 1
        self.right    = (self.cx + self.radius)
        self.bottom   = (self.cy + self.radius)

        self.scene.setSceneRect(self.X1, self.Y1,
                                self.w, self.h)

        self.square = QRect(self.left,     self.top,       # Currently unused
                            self.diameter, self.diameter)  # Currently unused

        self.rg = QRadialGradient(self.cx, self.cy,        # Currently unused
                                  self.radius,             # Currently unused
                                  self.cx, self.cy)        # Currently unused
        self.rg.setColorAt(0, WHITE_TRANSPARENT)           # Currently unused
        self.rg.setColorAt(1, GREEN_OPAQUE)                # Currently unused

        # WARNING! WARNING! DANGER, WILL ROBINSON! (BEGIN)

#       self.inner  = QGraphicsEllipseItem(0, 0, self.avatar.w, self.avatar.w)
#       self.scene.addItem(self.inner)

        scalingFactor = self.diameter / (self.meatRadius * 2.0)
        self.sourceSize  =  8.0 * scalingFactor
        self.avatarWidth = 24.0 * scalingFactor
        self.avatarDepth = 12.0 * scalingFactor

        sourceScale = []
        for rose in range(8):
            sourceScale.append(self.sourceSize / self.sources[rose].w)
            self.sources[rose].setScale(sourceScale[rose])

        self.compass = []        # The "compass rose" for speakers
        for degree in range(-90, -450, -45):
            x = self.cx + (cos(radians(degree)) * self.radius)
            y = self.cy + (sin(radians(degree)) * self.radius)
            self.compass.append((x, y))

        self.avatar.cx = int(self.avatarWidth / 2.0)
        self.avatar.cy = int(self.avatarDepth / 2.0)

        avatarScale = self.avatarWidth / self.avatar.w
        self.avatar.setScale(avatarScale)
        self.avatar.setPos(self.cx - self.avatar.cx,
                           self.cy - self.avatar.cy - 10)

        # Place source icon circles inside and tangent to ring.
        for rose in range(8):
            new_x = self.compass[rose][0] - self.sources[rose].compass[rose][0]
            new_y = self.compass[rose][1] - self.sources[rose].compass[rose][1]
            self.sources[rose].setPos(new_x, new_y)

#       self.inner.setScale(avatarScale)
#       self.inner.setPos(self.cx  - self.avatar.cx,
#                         self.cy  - self.avatar.cy - 20)

        # WARNING! WARNING! DANGER, WILL ROBINSON! (END)

        self.setScene(self.scene)
#       self.setBackgroundBrush(QBrush(Qt.yellow))
        brush = QBrush(QColor(255, 255, 127))
        brush.setStyle(Qt.SolidPattern)
        self.setBackgroundBrush(brush)
        self.update()

    def drawBackground(self, painter, rectf):
        """Draws red ring, green gradient, grey speaker boxes"""

        super(ControlPane, self).drawBackground(painter, rectf)
        return                                   # DISABLE drawBackground !!!

        painter.setBrush(QBrush(self.rg))        # Green radial gradient fill
        painter.setPen(QPen(QColor(Qt.red), 1))  # Red stroke
        painter.drawEllipse(self.square)         # Draw red w/green fill ring

        painter.setPen(QPen(QColor(Qt.red), 1))
        for rose in self.compass:
            painter.drawRect(rose[0] - (self.sourceSize / 2),
                             rose[1] - (self.sourceSize / 2),
                             self.sourceSize,
                             self.sourceSize)
            painter.fillRect(rose[0] + 1 - (self.sourceSize / 2),
                             rose[1] + 1 - (self.sourceSize / 2),
                             self.sourceSize - 1,
                             self.sourceSize - 1,
                             QColor(Qt.gray))

    def mouseMoveEvent(self, event):
        """Rotate avatar and set sound levels when avatar moves"""

#       super(ControlPane, self).mouseMoveEvent(event)
        print()
        print("mapToScene(event.pos) = {0}"
              .format(self.mapToScene(event.pos()).toTuple()))
        print("event.pos             = {0}"
              .format(event.pos().toTuple()))
        if self.avatar.mobile:
            QApplication.setOverrideCursor(Qt.BlankCursor)  # Hide while mobile
            print("Cursor position = {0}".format(self.cursor.pos().toTuple()))
#           self.cursor.setPos(500, 500)                                # DEBUG
            self.avatar.setPos(self.mapToScene(event.pos()) -
                               QPoint(self.avatar.cx, self.avatar.cy))
            self.levelChange([self.avatar.oldX + self.avatar.cx,
                              self.avatar.oldY + self.avatar.cy])

            # Rotate only after 10 pixels of movement to reduce wobble
            if ((abs(self.avatar.x() - self.avatar.oldX) > 10)  or
                (abs(self.avatar.y() - self.avatar.oldY) > 10)):
                dx = self.avatar.x() - self.avatar.oldX  # delta X
                dy = self.avatar.y() - self.avatar.oldY  # delta Y
                iNumber   = dx - dy * 1j
                angle     = cmath.phase(iNumber) + 1.5 * pi
                rotation  = (360 - degrees(angle)) % 360
                transform = QTransform()
                transform.translate(self.avatar.cx,  self.avatar.cy)
                transform.rotate(rotation)
                transform.translate(-self.avatar.cx, -self.avatar.cy)
                self.avatar.setTransform(transform)
                self.avatar.oldX = self.avatar.x()
                self.avatar.oldY = self.avatar.y()
        else:
#           QApplication.restoreOverrideCursor()  # Should work. Doesn't.
            QApplication.setOverrideCursor(Qt.ArrowCursor)  # Show while locked
            print("Cursor position = {0}".format(self.cursor.pos().toTuple()))

    def reset(self):
        """Recenter avatar, reset dB levels, remove breadcrumb spots"""

        self.avatar.setPos(QPoint(self.cx, self.cy) -
                           QPoint(self.avatar.cx, self.avatar.cy))
        rotation  = 0  # in degrees
        transform = QTransform()
        transform.translate(self.avatar.cx,  self.avatar.cy)
        transform.rotate(rotation)
        transform.translate(-self.avatar.cx, -self.avatar.cy)
        self.avatar.setTransform(transform)
        self.avatar.oldX = self.avatar.x()
        self.avatar.oldY = self.avatar.y()

        if breadcrumbs:                                                 # DEBUG
            for i, spot in enumerate(self.spots):                       # DEBUG
                self.scene.removeItem(spot)                             # DEBUG
            self.spots = []                                             # DEBUG

        self.levelChange([self.avatar.oldX + self.avatar.cx,
                          self.avatar.oldY + self.avatar.cy])

    def levelChange(self, coords):
        """Calculate dB values based on distances. Add breadcrumbs."""

        moved = False
        nearby = False                                                  # DEBUG
        for speaker, rose in enumerate(self.compass):
            x = coords[0] - rose[0]
            y = coords[1] - rose[1]
            distance = sqrt(x**2 + y**2)
            if abs(self.distance[speaker] - distance) > 9:
                moved = True
                self.distance[speaker] = distance
                ratio = float(self.radius) / float(distance)
                self.attenuation[speaker] = min(20 * log10(ratio), 12.0)
            if distance < 20.0:                                         # DEBUG
                nearby = True                                           # DEBUG

        if breadcrumbs:                                                 # DEBUG
            if nearby:                                                  # DEBUG
                oil = Qt.cyan                                           # DEBUG
            else:                                                       # DEBUG
                oil = Qt.red                                            # DEBUG
#                                                                       # DEBUG
            self.spots.append(QGraphicsEllipseItem(coords[0] - 2,       # DEBUG
                                                   coords[1] - 2,       # DEBUG
                                                   4, 4))               # DEBUG
            self.spots[-1].setPen(QPen(QColor(oil), 1))                 # DEBUG
            self.spots[-1].setBrush(QBrush(oil))                        # DEBUG
            self.scene.addItem(self.spots[-1])                          # DEBUG
            if len(self.spots) > 25:                                    # DEBUG
                for spot in self.spots[0:-25]:                          # DEBUG
                    self.scene.removeItem(spot)                         # DEBUG
                for i, spot in enumerate(self.spots[0:-25]):            # DEBUG
                    del self.spots[i]                                   # DEBUG

        if moved:
            self.dBs.emit(self.attenuation)

    def keyPressEvent(self, event):
        """Provide 'escape' hatch emergency exit"""

        super(ControlPane, self).keyPressEvent(event)  # Old way. Learn new way.

        key = event.key()
        if key == Qt.Key_Escape:
            QApplication.closeAllWindows()


def main():
    import sys
    app = QApplication(sys.argv)
    controlPane = ControlPane()
    controlPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
