#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'EditInfo.ui'
#
# Created: Sun Feb  7 21:48:18 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# Solution to button focus issue provided by:
#
#     https://stackoverflow.com/a/45568125/447830
#

from __future__ import print_function, absolute_import

import os.path

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Edit Info Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017, Kevin Cole (2017.06.19)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Button colors

GREY = "background: #e0e0e0;"  # Neutral (default state)


class FocusButton(QPushButton):

    def __init__(self, *args, **kwargs):
        super(FocusButton, self).__init__(*args, **kwargs)

    def focusInEvent(self, event):
        self.setDefault(True)
        super(FocusButton, self).focusInEvent(event)

    def focusOutEvent(self, event):
        self.setDefault(False)
        super(FocusButton, self).focusOutEvent(event)


class UI(QDialog):
    """Dialog to collect current edit information"""

    def __init__(self, parent=None):
        """Implement dialog with subject info, Kyma sound, number of ALD settings, presets, and room size"""
        super(UI, self).__init__(parent)

        self.setObjectName("EditInfo")
        self.setWindowTitle("Synchronize files")
        self.resize(313, 159)

        self.presetsLabel = QLabel(self)
        self.presetsLabel.setObjectName("presetsLabel")
        self.presetsLabel.setText("Presets:")

        self.presetsButton = FocusButton(self)
        self.presetsButton.setObjectName("presetsButton")
        self.presetsButton.setText("Choose presets folder")

        self.ratingsLabel = QLabel(self)
        self.ratingsLabel.setObjectName("ratingsLabel")
        self.ratingsLabel.setText("Ratings:")

        self.ratingsButton = FocusButton(self)
        self.ratingsButton.setObjectName("ratingsButton")
        self.ratingsButton.setText("Choose ratings folder")

        self.instructionsLabel = QLabel(self)
        self.instructionsLabel.setObjectName("instructionsLabel")
        self.instructionsLabel.setText("Instructions:")

        self.instructionsButton = FocusButton(self)
        self.instructionsButton.setObjectName("instructionsButton")
        self.instructionsButton.setText("Choose instructions file")

        self.saveButton = FocusButton(self)
        self.saveButton.setObjectName("saveButton")
        self.saveButton.setText("Begin edit")

        spacerItem = QSpacerItem(40, 20,
                                 QSizePolicy.Expanding,
                                 QSizePolicy.Minimum)

        self.gridLayout = QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.addWidget(self.presetsLabel,        0, 0, 1, 1)
        self.gridLayout.addWidget(self.presetsButton,       0, 1, 1, 2)
        self.gridLayout.addWidget(self.ratingsLabel,        1, 0, 1, 1)
        self.gridLayout.addWidget(self.ratingsButton,       1, 1, 1, 2)
        self.gridLayout.addWidget(self.instructionsLabel,   2, 0, 1, 1)
        self.gridLayout.addWidget(self.instructionsButton,  2, 1, 1, 2)
        self.gridLayout.addItem(spacerItem,                 3, 0, 1, 2)
        self.gridLayout.addWidget(self.saveButton,          3, 2, 1, 1)

        QMetaObject.connectSlotsByName(self)

        if __name__ == "__main__":
            self.presetsButton.clicked.connect(self.choosePresets)
            self.ratingsButton.clicked.connect(self.chooseRatings)
            self.instructionsButton.clicked.connect(self.chooseInstructions)
            self.saveButton.clicked.connect(self.accept)

    if __name__ == "__main__":
        def choosePresets(self):
            wd = os.path.expanduser("~/.config/sound-advice/presets")
            self.presetsPath = QFileDialog.getExistingDirectory(caption="Choose presets...",
                                                                dir=wd)
            self.presetsButton.setText(self.presetsPath.split("/")[-1])

        def chooseRatings(self):
            wd = os.path.expanduser("~/.config/sound-advice/ratings")
            self.ratingsPath = QFileDialog.getExistingDirectory(caption="Choose ratings...",
                                                                dir=wd)
            self.ratingsButton.setText(self.ratingsPath.split("/")[-1])

        def chooseInstructions(self):
            wd = os.path.expanduser("~/.config/sound-advice/instructions")
            self.instructionsPath = QFileDialog.getOpenFileName(caption="Choose instructions...",
                                                                dir=wd,
                                                                filter="JSON (*.json)")[0]
            self.instructionsFile = os.path.basename(self.instructionsPath)
            self.instructionsButton.setText(self.instructionsFile.split(".")[0])

        def accept(self):
            print("Presets:      {0}".format(self.presetsPath))
            print("Ratings:      {0}".format(self.ratingsPath))
            print("Instructions: {0}".format(self.instructionsPath))


def main():
    import sys
    app = QApplication(sys.argv)
    editInfo = UI()
    editInfo.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
