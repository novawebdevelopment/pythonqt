#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  UI.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  UI: Pulls together the pieces to make the Optimization User
#  Interface.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

from .Panes.OptimizationPane import *
from .MenuPane         import *

__appname__    = "Sound Advice"
__module__     = "Optimization Virtual Control Surface (VCS)"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class UI(QWidget):
    """Optimization unit complete UI panes"""

    def setupUi(self, Widget, initValues):
        """Implement main pane, ratings pane, audio sample menu, rating menu, ALD settings"""

        self.settingButtons = initValues["programButtons"]
        self.meatRadius     = initValues["meatRadius"]

        Widget.setObjectName("Widget")
        Widget.setWindowTitle("Optimization Exploration")

        Widget.setWindowFlags(Qt.FramelessWindowHint)
        Widget.showFullScreen()

        self.gridLayout = QGridLayout(Widget)
        self.gridLayout.setObjectName("gridLayout")

        self.controlPanel  = ControlPane(initValues, self)
        self.controlMenu   = MenuPane(initValues, self)

        self.gridLayout.addWidget(self.controlPanel, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.controlMenu,  0, 1, 1, 1)

        QMetaObject.connectSlotsByName(Widget)


def main():
    import sys
    import os.path
    import json

    from common.functions import unjson

    # Use a default set of presets and ratings questions for testing
    #
    presetsPath = os.path.expanduser("~/.config/sound-advice/presets/")
    configuration = unjson(presetsPath + "default")
    presets = configuration["presets"]

    ratingsPath = os.path.expanduser("~/.config/sound-advice/ratings/")
    configuration = unjson(ratingsPath + "default")
    ratings = configuration["ratings"]

    instructionsPath = os.path.expanduser("~/.config/sound-advice/instructions/Optimization/")
    instructionsFile = instructionsPath + "default.json"
    fd = open(instructionsFile, "r")
    instructions = json.load(fd)

    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()

    initValues = {"width": width,
                  "height": height,
                  "subjectInfo": "KJC",
                  "kymaSound":   "Testing",
                  "programButtons": 6,
                  "meatRadius": 17 * 12,
                  "presets": presets,
                  "ratings": ratings,
                  "instructions": instructions,
                  "filename": "/dev/null"}

    Widget = QWidget()
    ui = UI()
    ui.setupUi(Widget, initValues)
    Widget.show()

    for twiddle in range(10):         # Twiddle our thumbs while waiting...
        QApplication.processEvents()  # ...for resize, et al to finish

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
