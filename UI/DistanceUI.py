#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  UI.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  UI: Pulls together the pieces to make the Distance User
#  Interface.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

from .Panes.DeviceSettingPane import *
from .Panes.DistancePane      import *
from .Panes.RatingsPane       import *
from .Panes.ControlMenuPane   import *
from .Panes.RatingsMenuPane   import *

__appname__    = "Sound Advice"
__module__     = "Distance Virtual Control Surface (VCS)"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class UI(QWidget):
    """Distance unit complete UI panes"""

    def setupUi(self, Widget, ratings, settingButtons=6, meatRadius=52):
        """Implement main pane, ratings pane, audio sample menu, rating menu, ALD settings"""

        Widget.setObjectName("Widget")
        Widget.setWindowTitle("Distance Exploration")

        Widget.setWindowFlags(Qt.FramelessWindowHint)
        Widget.showFullScreen()

        self.gridLayout = QGridLayout(Widget)
        self.gridLayout.setObjectName("gridLayout")

        self.deviceSetting = DeviceSettingPane(settingButtons, self)
        self.controlPanel  = ControlPane(meatRadius, self)
        self.ratingsPanel  = RatingsPane(ratings, self)
        self.controlMenu   = ControlMenuPane(self)
        self.ratingsMenu   = RatingsMenuPane(self)

        self.gridLayout.addWidget(self.deviceSetting, 0, 0, 1, 2)
        self.gridLayout.addWidget(self.controlPanel,  1, 0, 1, 1)
        self.gridLayout.addWidget(self.ratingsPanel,  1, 1, 1, 1)
        self.gridLayout.addWidget(self.controlMenu,   2, 0, 1, 1)
        self.gridLayout.addWidget(self.ratingsMenu,   2, 1, 1, 1)

        QMetaObject.connectSlotsByName(Widget)


def main():
    import sys
    import os.path
    from common.functions import unjson

    # Use a default set of ratings questions for testing
    #
    ratingsPath = os.path.expanduser("~/.config/sound-advice/ratings/")
    configuration = unjson(ratingsPath + "default")
    ratings = configuration["ratings"]

    app = QApplication(sys.argv)
    Widget = QWidget()
    ui = UI()
    ui.setupUi(Widget, ratings)
    Widget.show()

    for twiddle in range(10):         # Twiddle our thumbs while waiting...
        QApplication.processEvents()  # ...for resize, et al to finish

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
