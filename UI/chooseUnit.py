#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chooseUnit.ui'
#
# Created: Sun Jan 28 13:31:37 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

# Mac aspect ratio = (3360 * 2100) = (16 * 10) = (8 * 5)
# No... In SPITE of that being the size of the image it created,
# Display preferences 4 out of 5 (between "Default" and "More Space")
# claims to be (1680 * 1050) = (16 * 10) = (8 * 5)

from PySide.QtCore import *
from PySide.QtGui  import *


class ChooseUnit(QDialog):
    def __init__(self, parent=None):
        super(ChooseUnit, self).__init__(parent)

        self.setObjectName("chooseUnit")
        self.setWindowTitle("Choose unit")
        self.showMaximized()
#       self.resize(675, 950)

        font = QFont()
        font.setFamily("Sans Serif")
        font.setPointSize(16)

        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding,
                                 QSizePolicy.Maximum)

        self.selectUnit = QLabel(self)
        self.selectUnit.setObjectName("selectUnit")
        self.selectUnit.setText("Select exercise unit")
        self.selectUnit.setFont(font)
        self.selectUnit.setAlignment(Qt.AlignCenter)
        self.selectUnit.setSizePolicy(sizePolicy)

        sizePolicy = QSizePolicy(QSizePolicy.Maximum,
                                 QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)

        self.snrUnit = QRadioButton(self)
        self.snrUnit.setObjectName("SNR")
        self.snrUnit.setText("S&NR")
        self.snrUnit.setFont(font)
        self.snrUnit.setSizePolicy(sizePolicy)

        self.localizationUnit = QRadioButton(self)
        self.localizationUnit.setObjectName("Localization")
        self.localizationUnit.setText("&Localization")
        self.localizationUnit.setFont(font)
        self.localizationUnit.setSizePolicy(sizePolicy)

        self.optimizationUnit = QRadioButton(self)
        self.optimizationUnit.setObjectName("Optimization")
        self.optimizationUnit.setText("Op&timization")
        self.optimizationUnit.setFont(font)
        self.optimizationUnit.setSizePolicy(sizePolicy)

        self.units = QButtonGroup(self)
        self.units.setObjectName("units")
        self.units.addButton(self.snrUnit)
        self.units.addButton(self.localizationUnit)
        self.units.addButton(self.optimizationUnit)

        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding,
                                 QSizePolicy.MinimumExpanding)

        self.snrUnitShot = QLabel(self)
        self.snrUnitShot.setObjectName("snrUnitShot")
        self.snrUnitShot.setMaximumWidth(464)
        self.snrUnitShot.setMaximumHeight(290)
        self.snrUnitShot.setSizePolicy(sizePolicy)
        self.snrUnitShot.setFrameShape(QFrame.StyledPanel)
        screenshot = QPixmap("images/SNR.png")
        w = min(screenshot.width(),  self.snrUnitShot.maximumWidth())
        h = min(screenshot.height(), self.snrUnitShot.maximumHeight())
        screenshot = screenshot.scaled(w, h,
                                       Qt.KeepAspectRatio,
                                       Qt.SmoothTransformation)
        self.snrUnitShot.setPixmap(screenshot)
#       self.snrUnitShot.setScaledContents(True)

        self.localizationUnitShot = QLabel(self)
        self.localizationUnitShot.setObjectName("localizationUnitShot")
        self.localizationUnitShot.setMaximumWidth(464)
        self.localizationUnitShot.setMaximumHeight(290)
        self.localizationUnitShot.setSizePolicy(sizePolicy)
        self.localizationUnitShot.setFrameShape(QFrame.StyledPanel)
        screenshot = QPixmap("images/Localization.png")
        w = min(screenshot.width(),  self.localizationUnitShot.maximumWidth())
        h = min(screenshot.height(), self.localizationUnitShot.maximumHeight())
        screenshot = screenshot.scaled(w, h,
                                       Qt.KeepAspectRatio,
                                       Qt.SmoothTransformation)
        self.localizationUnitShot.setPixmap(screenshot)
#       self.localizationUnitShot.setScaledContents(True)
        self.optimizationUnitShot = QLabel(self)
        self.optimizationUnitShot.setObjectName("optimizationUnitShot")
        self.optimizationUnitShot.setMaximumWidth(464)
        self.optimizationUnitShot.setMaximumHeight(290)
        self.optimizationUnitShot.setSizePolicy(sizePolicy)
        self.optimizationUnitShot.setFrameShape(QFrame.StyledPanel)
        screenshot = QPixmap("images/Optimization.png")
        w = min(screenshot.width(),  self.optimizationUnitShot.maximumWidth())
        h = min(screenshot.height(), self.optimizationUnitShot.maximumHeight())
        screenshot = screenshot.scaled(w, h,
                                       Qt.KeepAspectRatio,
                                       Qt.SmoothTransformation)
        self.optimizationUnitShot.setPixmap(screenshot)
#       self.optimizationUnitShot.setScaledContents(True)

        self.snrUnitShot.setBuddy(self.snrUnit)
        self.localizationUnitShot.setBuddy(self.localizationUnit)
        self.optimizationUnitShot.setBuddy(self.optimizationUnit)

        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")

        self.gridLayout = QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.addWidget(self.selectUnit,           0, 0, 1, 2)
        self.gridLayout.addWidget(self.snrUnit,              1, 0, 1, 1)
        self.gridLayout.addWidget(self.snrUnitShot,          1, 1, 1, 1)
        self.gridLayout.addWidget(self.localizationUnit,     2, 0, 1, 1)
        self.gridLayout.addWidget(self.localizationUnitShot, 2, 1, 1, 1)
        self.gridLayout.addWidget(self.optimizationUnit,     3, 0, 1, 1)
        self.gridLayout.addWidget(self.optimizationUnitShot, 3, 1, 1, 1)
        self.gridLayout.addWidget(self.buttonBox,            4, 1, 1, 1)

        QObject.connect(self.buttonBox, SIGNAL("accepted()"), self.accept)
        QObject.connect(self.buttonBox, SIGNAL("rejected()"), self.reject)
        QMetaObject.connectSlotsByName(self)


def main():
    import sys
    app = QApplication(sys.argv)

    chooseunit = ChooseUnit()
    chooseunit.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
#       pixmap = pixmap.scaled(QSize(w, h), Qt.KeepAspectRatio
# Form implementation generated from reading ui file 'buddy.ui'
