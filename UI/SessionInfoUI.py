#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SessionInfo.ui'
#
# Created: Sun Feb  7 21:48:18 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# Solution to button focus issue provided by:
#
#     https://stackoverflow.com/a/45568125/447830
#

from __future__ import print_function, absolute_import

import os.path

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Session Info Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017, Kevin Cole (2017.06.19)"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"

# Button colors

GREY = "background: #e0e0e0;"  # Neutral (default state)


class FocusButton(QPushButton):

    def __init__(self, *args, **kwargs):
        super(FocusButton, self).__init__(*args, **kwargs)

    def focusInEvent(self, event):
        self.setDefault(True)
        super(FocusButton, self).focusInEvent(event)

    def focusOutEvent(self, event):
        self.setDefault(False)
        super(FocusButton, self).focusOutEvent(event)


class UI(object):
    """Dialog to collect current session information"""

    def setupUi(self, SessionInfo, feet=17, inches=0):
        """Implement dialog with subject info, Kyma sound, number of ALD settings, presets, and room size"""

        SessionInfo.setObjectName("SessionInfo")
        SessionInfo.setWindowTitle("Subject Info")
        SessionInfo.resize(313, 159)

        self.subjectLabel = QLabel(SessionInfo)
        self.subjectLabel.setObjectName("subjectLabel")
        self.subjectLabel.setText("Subject information:")

        self.subjectId = QLineEdit(SessionInfo)
        self.subjectId.setObjectName("subjectId")

        self.kymaLabel = QLabel(SessionInfo)
        self.kymaLabel.setObjectName("kymaLabel")
        self.kymaLabel.setText("Kyma sound:")

        self.kymaSound = QLineEdit(SessionInfo)
        self.kymaSound.setObjectName("kymaSound")

        self.programsLabel = QLabel(SessionInfo)
        self.programsLabel.setObjectName("programsLabel")
        self.programsLabel.setText("Number of device programs:")

        self.programButtons = QSpinBox(SessionInfo)
        self.programButtons.setObjectName("programButtons")
        self.programButtons.setMinimum(1)
        self.programButtons.setMaximum(6)

        self.presetsLabel = QLabel(SessionInfo)
        self.presetsLabel.setObjectName("presetsLabel")
        self.presetsLabel.setText("Presets:")

        self.presetsButton = FocusButton(SessionInfo)
        self.presetsButton.setObjectName("presetsButton")
        self.presetsButton.setText("Choose presets folder")

        self.ratingsLabel = QLabel(SessionInfo)
        self.ratingsLabel.setObjectName("ratingsLabel")
        self.ratingsLabel.setText("Ratings:")

        self.ratingsButton = FocusButton(SessionInfo)
        self.ratingsButton.setObjectName("ratingsButton")
        self.ratingsButton.setText("Choose ratings folder")

        self.instructionsLabel = QLabel(SessionInfo)
        self.instructionsLabel.setObjectName("instructionsLabel")
        self.instructionsLabel.setText("Instructions:")

        self.instructionsButton = FocusButton(SessionInfo)
        self.instructionsButton.setObjectName("instructionsButton")
        self.instructionsButton.setText("Choose instructions file")

        self.meatLabel = QLabel(SessionInfo)
        self.meatLabel.setObjectName("meatLabel")
        self.meatLabel.setText("Room diameter [Default 17ft. 0in.]:")

        self.meatFeet = QSpinBox(SessionInfo)
        self.meatFeet.setObjectName("meatFeet")
        self.meatFeet.setSuffix(" ft.")
        self.meatFeet.setMinimum(1)
        self.meatFeet.setMaximum(300)
        self.meatFeet.setValue(feet)        # Default 17'0" (204")

        self.meatInches = QSpinBox(SessionInfo)
        self.meatInches.setObjectName("meatInches")
        self.meatInches.setSuffix(" in.")
        self.meatInches.setMinimum(0)
        self.meatInches.setMaximum(11)
        self.meatInches.setValue(inches)    # Default 17'0" (204")

        self.saveButton = FocusButton(SessionInfo)
        self.saveButton.setObjectName("saveButton")
        self.saveButton.setText("Begin session")

        spacerItem = QSpacerItem(40, 20,
                                 QSizePolicy.Expanding,
                                 QSizePolicy.Minimum)

        self.gridLayout = QGridLayout(SessionInfo)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.addWidget(self.subjectLabel,        0, 0, 1, 1)
        self.gridLayout.addWidget(self.subjectId,           0, 1, 1, 2)
        self.gridLayout.addWidget(self.kymaLabel,           1, 0, 1, 1)
        self.gridLayout.addWidget(self.kymaSound,           1, 1, 1, 2)
        self.gridLayout.addWidget(self.programsLabel,       2, 0, 1, 2)
        self.gridLayout.addWidget(self.programButtons,      2, 2, 1, 1)
        self.gridLayout.addWidget(self.presetsLabel,        3, 0, 1, 1)
        self.gridLayout.addWidget(self.presetsButton,       3, 1, 1, 2)
        self.gridLayout.addWidget(self.ratingsLabel,        4, 0, 1, 1)
        self.gridLayout.addWidget(self.ratingsButton,       4, 1, 1, 2)
        self.gridLayout.addWidget(self.instructionsLabel,   5, 0, 1, 1)
        self.gridLayout.addWidget(self.instructionsButton,  5, 1, 1, 2)
        self.gridLayout.addWidget(self.meatLabel,           6, 0, 1, 1)
        self.gridLayout.addWidget(self.meatFeet,            6, 1, 1, 1)
        self.gridLayout.addWidget(self.meatInches,          6, 2, 1, 1)
        self.gridLayout.addItem(spacerItem,                 7, 0, 1, 2)
        self.gridLayout.addWidget(self.saveButton,          7, 2, 1, 1)

        QMetaObject.connectSlotsByName(SessionInfo)

        if __name__ == "__main__":
            self.presetsButton.clicked.connect(self.choosePresets)
            self.ratingsButton.clicked.connect(self.chooseRatings)
            self.instructionsButton.clicked.connect(self.chooseInstructions)
            self.saveButton.clicked.connect(self.accept)

    if __name__ == "__main__":
        def choosePresets(self):
            wd = os.path.expanduser("~/.config/sound-advice/presets")
            self.presetsPath = QFileDialog.getExistingDirectory(caption="Choose presets...",
                                                                dir=wd)
            self.presetsButton.setText(self.presetsPath.split("/")[-1])

        def chooseRatings(self):
            wd = os.path.expanduser("~/.config/sound-advice/ratings")
            self.ratingsPath = QFileDialog.getExistingDirectory(caption="Choose ratings...",
                                                                dir=wd)
            self.ratingsButton.setText(self.ratingsPath.split("/")[-1])

        def chooseInstructions(self):
            wd = os.path.expanduser("~/.config/sound-advice/instructions")
            self.instructionsPath = QFileDialog.getOpenFileName(caption="Choose instructions...",
                                                                dir=wd,
                                                                filter="JSON (*.json)")[0]
            self.instructionsFile = os.path.basename(self.instructionsPath)
            self.instructionsButton.setText(self.instructionsFile.split(".")[0])

        def accept(self):
            print("Subject ID:   {0}".format(self.subjectId.text()))
            print("Kyma sound:   {0}".format(self.kymaSound.text()))
            print("Programs:     {0}".format(self.programButtons.value()))
            print("Presets:      {0}".format(self.presetsPath))
            print("Ratings:      {0}".format(self.ratingsPath))
            print("Instructions: {0}".format(self.instructionsPath))
            print("Room size:    {0}' {1}\"".format(self.meatFeet.value(),
                                                    self.meatInches.value()))


def main():
    import sys
    app = QApplication(sys.argv)
    Widget = QWidget()
    subjectInfo = UI()
    subjectInfo.setupUi(Widget)
    Widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
