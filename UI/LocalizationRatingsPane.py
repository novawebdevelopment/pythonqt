#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  RatingsPane.py
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.02.08
#
#  Ratings Pane: Side panel with three ratings on a scale of 1 to 5.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import print_function, absolute_import

from PySide.QtCore import *
from PySide.QtGui  import *

__appname__    = "Sound Advice"
__module__     = "Localization Ratings Pane"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2016, Kevin Cole"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class RatingsPane(QGroupBox):
    """Rating pane for Localization unit"""

    def __init__(self, parent=None):
        """Implement [Certainty], [Topic] and [Effort] ratings"""

        super(RatingsPane, self).__init__(parent)

        self.setObjectName("ratingsPane")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(10)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setTitle("")

        self.certaintyRating = QGroupBox(self)
        self.certaintyRating.setObjectName("certaintyRating")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.certaintyRating.setFont(font)
        self.certaintyRating.setTitle("I chose the source location with...")

        self.certaintyButtons = QButtonGroup()
        self.certaintyButtons.setObjectName("certainty")

        self.certaintyLayout = QVBoxLayout(self.certaintyRating)
        self.certaintyLayout.setObjectName("certaintyLayout")

        choices = ["&Complete certainty", "",
                   "Some certainty",     "",
                   "No certainty"]
        self.certainty = []
        for button in range(5):
            self.certainty.append(QRadioButton(self.certaintyRating))
            self.certainty[button].setObjectName("certainty{0}".format(button))
            font = QFont()
            font.setPointSize(20)
            font.setWeight(50)
            font.setBold(False)
            self.certainty[button].setFont(font)
            self.certainty[button].setText(choices[button])
            self.certaintyButtons.addButton(self.certainty[button])
            self.certaintyButtons.setId(self.certainty[button], button + 1)
            self.certaintyLayout.addWidget(self.certainty[button])

        self.topicRating = QGroupBox(self)
        self.topicRating.setObjectName("topicRating")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.topicRating.setFont(font)
        self.topicRating.setTitle("The topic of the sound is...")
        self.topicDescription = QPlainTextEdit()
        self.topicDescription.setObjectName("topicDescription")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding,
                                 QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.topicDescription.sizePolicy()
                                     .hasHeightForWidth())
        self.topicDescription.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(20)
        font.setWeight(50)
        font.setBold(False)
        self.topicDescription.setFont(font)
        self.topicDescription.setWordWrapMode(QTextOption.WordWrap)

        self.topicLayout = QVBoxLayout(self.topicRating)
        self.topicLayout.setObjectName("topicLayout")
        self.topicLayout.addWidget(self.topicDescription)

        self.effortRating = QGroupBox(self)
        self.effortRating.setObjectName("effortRating")
        font = QFont()
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.effortRating.setFont(font)
        self.effortRating.setTitle("Listening is...")

        self.effortButtons = QButtonGroup()
        self.effortButtons.setObjectName("effort")

        self.effortLayout = QVBoxLayout(self.effortRating)
        self.effortLayout.setObjectName("effortLayout")

        choices = ["Very difficu&lt", "", "So-so", "", "Very easy"]
        self.effort = []
        for button in range(5):
            self.effort.append(QRadioButton(self.effortRating))
            self.effort[button].setObjectName("effort{0}".format(button))
            font = QFont()
            font.setPointSize(20)
            font.setWeight(50)
            font.setBold(False)
            self.effort[button].setFont(font)
            self.effort[button].setText(choices[button])
            self.effortButtons.addButton(self.effort[button])
            self.effortButtons.setId(self.effort[button], button + 1)
            self.effortLayout.addWidget(self.effort[button])

        self.ratingsLayout = QVBoxLayout(self)
        self.ratingsLayout.setObjectName("ratingsLayout")
        self.ratingsLayout.addWidget(self.certaintyRating)
        self.ratingsLayout.addWidget(self.topicRating)
        self.ratingsLayout.addWidget(self.effortRating)

    def setEnabled(self, state):
        """Enable / disable ratings"""

        for button in range(5):
            self.certainty[button].setEnabled(state)
            self.effort[button].setEnabled(state)
        self.topicDescription.setEnabled(state)

    def uncheckAll(self):
        """Reset all ratings to null values"""

        self.certaintyButtons.setExclusive(False)
        self.effortButtons.setExclusive(False)

        for button in range(5):
            self.certainty[button].setChecked(False)
            self.effort[button].setChecked(False)

        self.topicDescription.clear()

        self.certaintyButtons.setExclusive(True)
        self.effortButtons.setExclusive(True)


def main():
    import sys
    app = QApplication(sys.argv)
    ratingsPane = RatingsPane()
    ratingsPane.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
