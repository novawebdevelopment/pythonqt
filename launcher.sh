# 2015.10.13 KJC
# This is the string (all as one line) that appeared in a generated
# Terminal window on Mac OS X, when asked to launch a Python program
# with the Python Launcher
#

cd '/Users/kyma/Code/kevin/OSC/' && \
'/usr/local/bin/pythonw'  '/Users/kyma/Code/kevin/OSC/DemoChangingLevels.py' && \
echo Exit status: $? && exit 1
