# README #

Welcome to **Sound Advice**.

### Background ###

People who have experienced a hearing loss as adults often have
difficulty learning to use assistive listening devices such as hearing
aids and cochlear implants. Users of the equipment often have
unrealistic expectations regarding the quality of the audio signal
provided, and become frustrated with the rehabilitation exercises.

**Sound Advice** is an attempt to _gamify_ the experience, in the
hopes that it will allow users to stick with the exercises long enough
to derive some benefit from the devices, rather than abandoning them.
Technologies involved:

* A [Symbolic Sound](http://kyma.symbolicsound.com/) **Paca** Digital
  Signal Processor (DSP) together with a
  [Behringer](http://www.music-group.com/brand/behringer/home)
  Firepower FCA610 USB/Firewire audio interface and Kyma X control
  software also from [Symbolic Sound](http://kyma.symbolicsound.com/)
* [Python 2](https://www.python.org/) with [Qt
  (PySide)](https://wiki.qt.io/PySide), and [Open Sound Control
  (OSC)](https://github.com/kjcole/python2-osc) protocol modules
* HTML5 and Javascript for the web-based version

### How do I get set up? ###

For the simple web-based version, not much to do at this point, really.

* Clone the repository.
* If you have a web server (Apache, Nginx, or other) running, move the
  repository to a web-accessible directory, and point your web browser
  at it with a URL like http://localhost/... Otherwise, simply point
  your web browser to a URL like file:///... (Your web browser should
  have a "File -> Open File..." or similar in a pull-down menu, that
  you can use to navigate to the repository directory.)
* Dependencies: A browser that understands HTML5, Javascript, and Ogg
  Vorbis audio files. Most modern web browsers qualify.

### But my friends call me brew.sh

Although perhaps not strictly necessary, [Homebrew](https://brew.sh/)
is like Cygwin for Mac OS X. It is the successor to Fink, DarwinPorts
and MacPorts.  To install:

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


### Building for Mac OS X 10.12.4 (Sierra)

I am not certain which of these worked:

    $ brew prune
    $ brew doctor
    $ brew install cartr/qt4/qt
    $ brew linkapps qt

__Or,__

* [register with Apple](https://developer.apple.com/programs/enroll/)
  as an [Apple Developer](https://developer.apple.com/programs/)

* Once registered, login and download [Xcode](https://developer.apple.com/download/)

* download an old Qt4 SOURCE from  http://download.qt.io/archive/qt/4.8/4.8.6/
  Specifically [qt-everywhere-opensource-src-4.8.6.tar.gz](https://download.qt.io/archive/qt/4.8/4.8.6/qt-everywhere-opensource-src-4.8.6.tar.gz)

* apply the [el-capitan patch](https://github.com/Homebrew/formula-patches/blob/master/qt/el-capitan.patch)

* build from [the instructions](http://doc.qt.io/qt-4.8/install-mac.html):


It appears to no longer like WebKit. It also complains about phonon
and QuickTime. So, turn off WebKit and multimedia. There are a lot of
other unnecessary bits as well. So:

    $ make confclean
    $ ./configure -no-cups -no-dbus -no-exceptions \
                  -no-qt3support -no-gstreamer \
                  -no-multimedia -no-phonon -no-phonon-backend \
                  -no-audio-backend -no-webkit -no-javascript-jit \
                  -no-script -no-scripttools \
                  -make libs -make tools \
                  -nomake examples -nomake demos -nomake docs \
                  -nomake translation \
            | tee buildme.log
    $ make
    $ sudo make -j1 install

Once Qt 4 is installed, set up a virtual environment with Python's
virtualenv and activate it. Then, in this repository's main directory:

    $ ./install.sh

This SHOULD be everything needed in order to run the units.

### Who do I talk to? ###

* Kevin Cole

### Miscellaneous ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
